<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * InvoicePlane
 *
 * @author		InvoicePlane Developers & Contributors
 * @copyright	Copyright (c) 2012 - 2017 InvoicePlane.com
 * @license		https://invoiceplane.com/license.txt
 * @link		https://invoiceplane.com
 */

/**
 * Class Reports
 */
class Reports extends Admin_Controller
{
    /**
     * Reports constructor.
     */
    public function __construct()
    {
        parent::__construct();

    }

    public function sales_orders_detail()
    {
        $this->load->helper('url');
        if ($this->input->post('btn_submit')) {
            
            $month = $this->input->post('month');
            $year = $this->input->post('year');
            redirect(env('REPORTING_URL')."sales_orders_detail.php?month=$month&year=$year",'refresh');
        }

        $this->layout->buffer('content', 'reports/sales_orders_detail_index')->render();
    }

    public function ar_sales_orders()
    {
        $this->load->helper('url');
        if ($this->input->post('btn_submit')) {
            
             
            $date = date_create($this->input->post('date'));
            $cutoff = date_format($date, 'Y-m-d');
            redirect(env('REPORTING_URL')."ar_sales_orders.php?cutoff=$cutoff",'refresh');
        }

        $this->layout->buffer('content', 'reports/ar_sales_orders_index')->render();
    }

}
