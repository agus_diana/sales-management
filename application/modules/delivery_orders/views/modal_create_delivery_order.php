<script>
    $(function () {
        // Display the create delivery_order modal
        $('#create-delivery_order').modal('show');

        $('.simple-select').select2();

        <?php $this->layout->load_view('clients/script_select2_client_id.js'); ?>

        // Toggle on/off permissive search on clients names
        $('span#toggle_permissive_search_clients').click(function () {
            if ($('input#input_permissive_search_clients').val() == ('1')) {
                $.get("<?php echo site_url('clients/ajax/save_preference_permissive_search_clients'); ?>", {
                    permissive_search_clients: '0'
                });
                $('input#input_permissive_search_clients').val('0');
                $('span#toggle_permissive_search_clients i').removeClass('fa-toggle-on');
                $('span#toggle_permissive_search_clients i').addClass('fa-toggle-off');
            } else {
                $.get("<?php echo site_url('clients/ajax/save_preference_permissive_search_clients'); ?>", {
                    permissive_search_clients: '1'
                });
                $('input#input_permissive_search_clients').val('1');
                $('span#toggle_permissive_search_clients i').removeClass('fa-toggle-off');
                $('span#toggle_permissive_search_clients i').addClass('fa-toggle-on');
            }
        });

        // Creates the delivery_order
        $('#delivery_order_create_confirm').click(function () {
            console.log('clicked');
            // Posts the data to validate and create the delivery_order;
            // will create the new client if necessary

            $.post("<?php echo site_url('sales_orders/ajax/sales_order_to_delivery_order'); ?>", {
            //$.post("<?php echo site_url('delivery_orders/ajax/create'); ?>", {
                    sales_order_id: $('#create_delivery_order_client_id').val(),
                    //client_id: $('#create_delivery_order_client_id').val(),
                    delivery_order_date_created: $('#delivery_order_date_created').val(),
                    delivery_order_time_created: '<?php echo date('H:i:s') ?>',
                    delivery_order_password: $('#delivery_order_password').val(),
                    user_id: '<?php echo $this->session->userdata('user_id'); ?>',
                    invoice_group_id: $('#invoice_group_id').val()
                },
                function (data) {
                    <?php echo(IP_DEBUG ? 'console.log(data);' : ''); ?>
                    var response = JSON.parse(data);
                    if (response.success === 1) {
                        // The validation was successful and delivery_order was created
                        window.location = "<?php echo site_url('delivery_orders/view'); ?>/" + response.delivery_order_id;
                    }
                    else {
                        // The validation was not successful
                        $('.control-group').removeClass('has-error');
                        for (var key in response.validation_errors) {
                            $('#' + key).parent().parent().addClass('has-error');
                        }
                    }
                });
        });
    });
</script>
 
<div id="create-delivery_order" class="modal modal-lg" role="dialog" aria-labelledby="modal_create_delivery_order" aria-hidden="true">
    <form class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="panel-title"><?php _trans('create_deliver_order'); ?></h4>
        </div>
        <div class="modal-body">

            <input class="hidden" id="input_permissive_search_clients"
                   value="<?php echo get_setting('enable_permissive_search_clients'); ?>">

            <div class="form-group has-feedback">
                <label for="create_delivery_order_client_id"><?php _trans('client'); ?></label>
                <div class="input-group">
   
                    
                    <select name="create_delivery_order_client_id" id="create_delivery_order_client_id" class="form-control simple-select">
                        <?php foreach ($open_so as $sales_order) { ?>
                            <option value="<?php echo $sales_order->sales_order_id; ?>"
                                <?php check_select($this->mdl_sales_orders->form_value('sales_order_id'), $sales_order->sales_order_id); ?>>
                                <?php echo $sales_order->sales_order_number . ' - ' . format_client($sales_order) . ' - ' . format_amount($sales_order->sales_order_qty_balance); ?>
                            </option>
                        <?php } ?>
                    </select>
                    
                    <!--
                    <select name="client_id" id="create_delivery_order_client_id" class="client-id-select form-control"
                            autofocus="autofocus">
                        <?php if (!empty($client)) : ?>
                            <option value="<?php echo $client->client_id; ?>"><?php _htmlsc(format_client($client)); ?></option>
                        <?php endif; ?>
                    </select>
                    -->

                    <span id="toggle_permissive_search_clients" class="input-group-addon" title="<?php _trans('enable_permissive_search_clients'); ?>" style="cursor:pointer;">
                        <i class="fa fa-toggle-<?php echo get_setting('enable_permissive_search_clients') ? 'on' : 'off' ?> fa-fw" ></i>
                    </span>
                </div>
            </div>

            <div class="form-group has-feedback">
                <label for="delivery_order_date_created">
                    <?php _trans('delivery_order_date'); ?>
                </label>

                <div class="input-group">
                    <input name="delivery_order_date_created" id="delivery_order_date_created"
                           class="form-control datepicker"
                           value="<?php echo date(date_format_setting()); ?>">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar fa-fw"></i>
                    </span>
                </div>
            </div>
            <!--
            <div class="form-group">
                <label for="delivery_order_password"><?php _trans('delivery_order_password'); ?></label>
                <input type="text" name="delivery_order_password" id="delivery_order_password" class="form-control"
                       value="<?php echo get_setting('delivery_order_pre_password') ? '' : get_setting('delivery_order_pre_password') ?>"
                       autocomplete="off">
            </div>
            -->
            <div class="form-group">
                <label for="invoice_group_id"><?php _trans('invoice_group'); ?>: </label>
                <select name="invoice_group_id" id="invoice_group_id" class="form-control simple-select">
                    <?php foreach ($invoice_groups as $invoice_group) { ?>
                        <option value="<?php echo $invoice_group->invoice_group_id; ?>"
                            <?php check_select(get_setting('default_delivery_order_group'), $invoice_group->invoice_group_id); ?>>
                            <?php _htmlsc($invoice_group->invoice_group_name); ?>
                        </option>
                    <?php } ?>
                </select>
            </div>

        </div>

        <div class="modal-footer">
            <div class="btn-group">
                <button class="btn btn-success ajax-loader" id="delivery_order_create_confirm" type="button">
                    <i class="fa fa-check"></i> <?php _trans('submit'); ?>
                </button>
                <button class="btn btn-danger" type="button" data-dismiss="modal">
                    <i class="fa fa-times"></i> <?php _trans('cancel'); ?>
                </button>
            </div>
        </div>

    </form>

</div>
