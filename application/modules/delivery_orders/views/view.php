<?php
$cv = $this->controller->view_data["custom_values"];
?>
<script> 

    $(function () {
        $('.btn_add_product_do').click(function () {
            $('#modal-placeholder').load("<?php echo site_url('products/ajax/modal_product_outstanding_so_lookups/' . $delivery_order->sales_order_id) ?>/" + Math.floor(Math.random() * 1000));
        });

        $('.btn_add_row').click(function () {
            $('#new_row').clone().appendTo('#item_table').removeAttr('id').addClass('item').show();
        });

        $('#delivery_order_change_client').click(function () {
            $('#modal-placeholder').load("<?php echo site_url('delivery_orders/ajax/modal_change_client'); ?>", {
                delivery_order_id: <?php echo $delivery_order_id; ?>,
                client_id: "<?php echo $this->db->escape_str($delivery_order->client_id); ?>"
            });
        });

        <?php if (!$items) { ?>
        $('#new_row').clone().appendTo('#item_table').removeAttr('id').addClass('item').show();
        <?php } ?>

        $('#btn_save_delivery_order').click(function () {
            var items = [];
            var item_order = 1;
            $('table tbody.item').each(function () {
                var row = {};
                $(this).find('input,select,textarea').each(function () {
                    if ($(this).is(':checkbox')) {
                        row[$(this).attr('name')] = $(this).is(':checked');
                    } else {
                        row[$(this).attr('name')] = $(this).val();
                    }
                });
                row['item_order'] = item_order;
                item_order++;
                items.push(row);
            });
            $.post("<?php echo site_url('delivery_orders/ajax/save'); ?>", {
                    delivery_order_id: <?php echo $delivery_order_id; ?>,
                    delivery_order_number: $('#delivery_order_number').val(),
                    delivery_order_date_created: $('#delivery_order_date_created').val(),
                    //delivery_order_date_expires: $('#delivery_order_date_expires').val(),
                    delivery_order_status_id: $('#delivery_order_status_id').val(),
                    delivery_order_password: $('#delivery_order_password').val(),
                    items: JSON.stringify(items),
                    delivery_order_discount_amount: $('#delivery_order_discount_amount').val(),
                    delivery_order_discount_percent: $('#delivery_order_discount_percent').val(),
                    notes: $('#notes').val(),
                    custom: $('input[name^=custom],select[name^=custom]').serializeArray()
                },
                function (data) {
                    <?php echo(IP_DEBUG ? 'console.log(data);' : ''); ?>
                    var response = JSON.parse(data);
                    if (response.success === 1) {
                        window.location = "<?php echo site_url('delivery_orders/view'); ?>/" + <?php echo $delivery_order_id; ?>;
                    } else {
                        $('#fullpage-loader').hide();
                        $('.control-group').removeClass('has-error');
                        $('div.alert[class*="alert-"]').remove();
                        var resp_errors = response.validation_errors,
                            all_resp_errors = '';

                        if (typeof(resp_errors) == "string") {
                            all_resp_errors = resp_errors;
                        } else {
                            for (var key in resp_errors) {
                                $('#' + key).parent().addClass('has-error');
                                all_resp_errors += resp_errors[key];
                            }
                        }

                        $('#delivery_order_form').prepend('<div class="alert alert-danger">' + all_resp_errors + '</div>');
                    }
                });
        });

        $('#btn_generate_pdf').click(function () {
            window.open('<?php echo site_url('delivery_orders/generate_pdf/' . $delivery_order_id); ?>', '_blank');
        });

        $(document).ready(function () {
            if ($('#delivery_order_discount_percent').val().length > 0) {
                $('#delivery_order_discount_amount').prop('disabled', true);
            }
            if ($('#delivery_order_discount_amount').val().length > 0) {
                $('#delivery_order_discount_percent').prop('disabled', true);
            }
        });
        $('#delivery_order_discount_amount').keyup(function () {
            if (this.value.length > 0) {
                $('#delivery_order_discount_percent').prop('disabled', true);
            } else {
                $('#delivery_order_discount_percent').prop('disabled', false);
            }
        });
        $('#delivery_order_discount_percent').keyup(function () {
            if (this.value.length > 0) {
                $('#delivery_order_discount_amount').prop('disabled', true);
            } else {
                $('#delivery_order_discount_amount').prop('disabled', false);
            }
        });

        var fixHelper = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        $("#item_table").sortable({
            helper: fixHelper,
            items: 'tbody'
        });
    });
</script>

<?php echo $modal_delete_delivery_order; ?>
<?php echo $modal_add_delivery_order_tax; ?>

<div id="headerbar">
    <h1 class="headerbar-title">
        <?php
        echo trans('delivery_order') . ' ';
        echo($delivery_order->delivery_order_number ? '#' . $delivery_order->delivery_order_number : $delivery_order->delivery_order_id);
        ?>
    </h1>

    <div class="headerbar-item pull-right btn-group">
        <div class="btn-group btn-group-sm">
            <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="#">
                <!--<?php _trans('options'); ?> <i class="fa fa-chevron-down"></i>-->
                <i class="fa fa-caret-down no-margin"></i> <?php _trans('options'); ?>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
				<?php if ($delivery_order->delivery_order_status_id == 1) { ?>
                <li>
                    <a href="#add-delivery_order-tax" data-toggle="modal">
                        <i class="fa fa-plus fa-margin"></i>
                        <?php _trans('add_delivery_order_tax'); ?>
                    </a>
                </li> <?php } ?>
                <li>
                    <a href="#" id="btn_generate_pdf"
                       data-delivery_order-id="<?php echo $delivery_order_id; ?>">
                        <i class="fa fa-print fa-margin"></i>
                        <?php _trans('download_pdf'); ?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo site_url('mailer/delivery_order/' . $delivery_order->delivery_order_id); ?>">
                        <i class="fa fa-send fa-margin"></i>
                        <?php _trans('send_email'); ?>
                    </a>
                </li> 
                
                <?php //if (($delivery_order->delivery_order_status_id != 6) && ($delivery_order->delivery_order_status_id != 1) && (($balance->delivery_order_balance) > 0)   ) { ?>
                
                <li>
                    <a href="#" id="btn_delivery_order_to_invoice"
                       data-delivery_order-id="<?php echo $delivery_order_id; ?>">
                        <i class="fa fa-refresh fa-margin"></i>
                        <?php _trans('delivery_order_to_invoice'); ?>
                    </a>
                </li> 
                <!--
                <li>
                    <a href="#" id="btn_copy_delivery_order"
                       data-delivery_order-id="<?php echo $delivery_order_id; ?>"
                       data-client-id="<?php echo $delivery_order->client_id; ?>">
                        <i class="fa fa-copy fa-margin"></i>
                        <?php _trans('copy_delivery_order'); ?>
                    </a>
                </li>
				-->
                <?php if ($delivery_order->delivery_order_status_id == 1) { ?>
                <li>
                    <a href="#delete-delivery_order" data-toggle="modal">
                        <i class="fa fa-trash-o fa-margin"></i> <?php _trans('delete'); ?>
                    </a>
                </li> <?php } ?>
            </ul>
        </div>

        <a href="#" class="btn btn-sm btn-success ajax-loader" id="btn_save_delivery_order">
            <i class="fa fa-check"></i>
            <?php _trans('save'); ?>
        </a>
<!--
        <a href="#" class="btn btn-sm btn-info ajax-loader" id="btn_save_delivery_order">
            <i class="fa fa-thumbs-up"></i>
            <?php _trans('approve'); ?>
        </a>

        <a href="#" class="btn btn-sm btn-warning ajax-loader" id="btn_save_delivery_order">
            <i class="fa fa-ban"></i>
            <?php _trans('cancel'); ?>
        </a>
-->
        <a href="<?php echo site_url('delivery_orders/'); ?>" class="btn btn-danger btn-sm" id="btn_close">
            <i class="fa fa-close"></i>
            <?php _trans('close'); ?>
        </a>
    </div>

</div>

<div id="content">
    <?php echo $this->layout->load_view('layout/alerts'); ?>
    <form id="delivery_order_form">
        <div class="delivery_order">

            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-5">

                    <h3>
                        <a href="<?php echo site_url('clients/view/' . $delivery_order->client_id); ?>">
                            <?php _htmlsc(format_client($delivery_order)) ?>
                        </a>
                        <?php if ($delivery_order->delivery_order_status_id == 1) { ?>
                            <span id="delivery_order_change_client" class="fa fa-edit cursor-pointer small"
                                  data-toggle="tooltip" data-placement="bottom"
                                  title="<?php _trans('change_client'); ?>"></span>
                        <?php } ?>
                    </h3>
                    <br>
                    <div class="client-address">
                        <?php $this->layout->load_view('clients/partial_client_address', array('client' => $delivery_order)); ?>
                    </div>
                    <?php if ($delivery_order->client_phone || $delivery_order->client_email) : ?>
                        <hr>
                    <?php endif; ?>
                    <?php if ($delivery_order->client_phone): ?>
                        <div>
                            <?php _trans('phone'); ?>:&nbsp;
                            <?php _htmlsc($delivery_order->client_phone); ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($delivery_order->client_email): ?>
                        <div>
                            <?php _trans('email'); ?>:&nbsp;
                            <?php _auto_link($delivery_order->client_email); ?>
                        </div>
                    <?php endif; ?>

                </div>

                <div class="col-xs-12 visible-xs"><br></div>

                <div class="col-xs-12 col-sm-6 col-md-7">
                    <div class="details-box">
                        <div class="row">

                            <div class="col-xs-12 col-md-6">

                                <div class="delivery_order-properties">
                                    <label for="delivery_order_number">
                                        <?php _trans('delivery_order'); ?> #
                                    </label>
                                    <input type="text" id="delivery_order_number" class="form-control input-sm" disabled
                                        <?php if ($delivery_order->delivery_order_number) : ?> value="<?php echo $delivery_order->delivery_order_number; ?>"
                                        <?php else : ?> placeholder="<?php _trans('not_set'); ?>"
                                        <?php endif; ?>>
                                </div>
                                <div class="delivery_order-properties">
                                    <label for="sales_order_number">
                                        <?php _trans('sales_order'); ?> #
                                    </label>
                                    <input type="text" id="sales_order_number" class="form-control input-sm" disabled
                                        <?php if ($delivery_order->sales_order_number) : ?> value="<?php echo $delivery_order->sales_order_number; ?>"
                                        <?php else : ?> placeholder="<?php _trans('not_set'); ?>"
                                        <?php endif; ?>>
                                </div>
                                
                            <!--    <div class="delivery_order-properties has-feedback">
                                    <label for="delivery_order_date_expires">
                                        <?php _trans('expires'); ?>
                                    </label>
                                    <div class="input-group">
                                        <input name="delivery_order_date_expires" id="delivery_order_date_expires"
                                               class="form-control input-sm datepicker"
                                               value="<?php echo date_from_mysql($delivery_order->delivery_order_date_expires); ?>">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar fa-fw"></i>
                                        </span>
                                    </div>
                                </div> -->

                                <!-- Custom fields -->
                                <?php foreach ($custom_fields as $custom_field): ?>
                                    <?php if ($custom_field->custom_field_location != 1) {
                                        continue;
                                    } ?>
                                    <?php print_field($this->mdl_delivery_orders, $custom_field, $cv); ?>
                                <?php endforeach; ?>

                            </div>
                            <div class="col-xs-12 col-md-6">

                                <div class="delivery_order-properties">
                                    <label for="delivery_order_status_id">
                                        <?php _trans('status'); ?>
                                    </label> 
                                    <select name="delivery_order_status_id" id="delivery_order_status_id"
                                            class="form-control input-sm simple-select">
                                        <?php foreach ($delivery_order_statuses as $key => $status) { ?>
                                            <option value="<?php echo $key; ?>"
                                                    <?php if ($key == $delivery_order->delivery_order_status_id) { ?>selected="selected"
                                                <?php } ?>>
                                                <?php echo $status['label']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="delivery_order-properties has-feedback">
                                    <label for="delivery_order_date_created">
                                        <?php _trans('delivery_order_date'); ?>
                                    </label>
                                    <div class="input-group">
                                        <input name="delivery_order_date_created" id="delivery_order_date_created" <?php if ($delivery_order->delivery_order_status_id != 1) { echo 'disabled'; } ?>
                                               class="form-control input-sm datepicker"
                                               value="<?php echo date_from_mysql($delivery_order->delivery_order_date_created); ?>"/>
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar fa-fw"></i>
                                        </span>
                                    </div>
                                </div>
                                <!--
                                <div class="delivery_order-properties">
                                    <label for="delivery_order_password">
                                        <?php _trans('delivery_order_password'); ?>
                                    </label>
                                    <input type="text" id="delivery_order_password" class="form-control input-sm" <?php if ($delivery_order->delivery_order_status_id != 1) { echo 'disabled'; } ?>
                                           value="<?php echo $delivery_order->delivery_order_password; ?>">
                                </div>
                                -->
                                <?php if ($delivery_order->delivery_order_status_id != 1) { ?>
                                    <div class="delivery_order-properties">
                                        <label for="delivery_order-guest-url"><?php _trans('guest_url'); ?></label>
                                        <div class="input-group">
                                            <input type="text" id="delivery_order-guest-url" readonly class="form-control"
                                                   value="<?php echo site_url('guest/view/delivery_order/' . $delivery_order->delivery_order_url_key); ?>">
                                            <span class="input-group-addon to-clipboard cursor-pointer"
                                                  data-clipboard-target="#delivery_order-guest-url">
                                                <i class="fa fa-clipboard fa-fw"></i>
                                            </span>
                                        </div>
                                    </div>
                                <?php } ?>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>

        <?php $this->layout->load_view('delivery_orders/partial_item_table'); ?>

        <hr/>

        <div class="row">
            <div class="col-xs-12 col-md-6">

                <div class="panel panel-default no-margin">
                    <div class="panel-heading">
                        <?php _trans('notes'); ?>
                    </div>
                    <div class="panel-body">
                        <textarea name="notes" id="notes" rows="3"
                                  class="input-sm form-control"><?php _htmlsc($delivery_order->notes); ?></textarea>
                    </div>
                </div>

                <div class="col-xs-12 visible-xs visible-sm"><br></div>

            </div>
            <div class="col-xs-12 col-md-6">

                <?php // $this->layout->load_view('upload/dropzone-delivery_order-html'); ?>

                <?php if ($custom_fields): ?>
                    <?php $cv = $this->controller->view_data["custom_values"]; ?>
                    <div class="row">
                        <div class="col-xs-12">

                            <!--<hr>-->

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <?php _trans('custom_fields'); ?>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <?php $i = 0; ?>
                                            <?php foreach ($custom_fields as $custom_field): ?>
                                                <?php if ($custom_field->custom_field_location != 0) {
                                                    continue;
                                                } ?>
                                                <?php $i++; ?>
                                                <?php if ($i % 2 != 0): ?>
                                                    <?php print_field($this->mdl_delivery_orders, $custom_field, $cv); ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </div>
                                        <div class="col-xs-12">
                                            <?php $i = 0; ?>
                                            <?php foreach ($custom_fields as $custom_field): ?>
                                                <?php if ($custom_field->custom_field_location != 0) {
                                                    continue;
                                                } ?>
                                                <?php $i++; ?>
                                                <?php if ($i % 2 == 0): ?>
                                                    <?php print_field($this->mdl_delivery_orders, $custom_field, $cv); ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
    </form>
</div>

<?php $this->layout->load_view('upload/dropzone-delivery_order-scripts'); ?>
