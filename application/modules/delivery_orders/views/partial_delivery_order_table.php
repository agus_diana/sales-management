<div class="table-responsive">
    <table class="table table-striped">

        <thead>
        <tr>
            <th><?php _trans('status'); ?></th>
            <th><?php _trans('delivery_order'); ?></th>
            <th><?php _trans('sales_order'); ?></th>
            <th><?php _trans('created'); ?></th>
            <!--<th><?php _trans('due_date'); ?></th>-->
            <th><?php _trans('client_name'); ?></th>
			<th style="text-align: right;"><?php _trans('amount'); ?></th>
            <th><?php _trans('options'); ?></th>

        </tr>
        </thead>

        <tbody>
        <?php
        $delivery_order_idx = 1;
        $delivery_order_count = count($delivery_orders);
        $delivery_order_list_split = $delivery_order_count > 3 ? $delivery_order_count / 2 : 9999;

        foreach ($delivery_orders as $delivery_order) {
            // Convert the dropdown menu to a dropup if delivery_order is after the invoice split
            $dropup = $delivery_order_idx > $delivery_order_list_split ? true : false;
            ?>
            <tr>
                <td>
                    <span class="label <?php echo $delivery_order_statuses[$delivery_order->delivery_order_status_id]['class']; ?>">
                        <?php echo $delivery_order_statuses[$delivery_order->delivery_order_status_id]['label']; ?>
                    </span>
                </td>
                <td>
                    <a href="<?php echo site_url('delivery_orders/view/' . $delivery_order->delivery_order_id); ?>"
                       title="<?php _trans('edit'); ?>">
                        <?php echo($delivery_order->delivery_order_number ? $delivery_order->delivery_order_number : $delivery_order->delivery_order_id); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo site_url('sales_orders/view/' . $delivery_order->sales_order_id); ?>"
                       title="<?php _trans('edit'); ?>">
                        <?php echo($delivery_order->sales_order_number ? $delivery_order->sales_order_number : $delivery_order->sales_order_number); ?>
                    </a>
                </td>
                <td>
                    <?php echo date_from_mysql($delivery_order->delivery_order_date_created); ?>
                </td>
                <!--
                <td>
                    <?php echo date_from_mysql($delivery_order->delivery_order_date_expires); ?>
                </td>
                -->
                <td>
                    <a href="<?php echo site_url('clients/view/' . $delivery_order->client_id); ?>"
                       title="<?php _trans('view_client'); ?>">
                        <?php _htmlsc(format_client($delivery_order)); ?>
                    </a>
                </td>
				<td class="amount">
                    <?php echo format_amount($delivery_order->delivery_order_qty_total); ?>
                </td>
                <!--
				<td class="amount">
                    <?php echo format_currency($delivery_order->delivery_order_balance); ?>
                </td>
                -->
                <td>
                    <div class="options btn-group<?php echo $dropup ? ' dropup' : ''; ?>">
                        <a class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown"
                           href="#">
                            <i class="fa fa-cog"></i> <?php _trans('options'); ?>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo site_url('delivery_orders/view/' . $delivery_order->delivery_order_id); ?>">
                                    <i class="fa fa-edit fa-margin"></i> <?php _trans('edit'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('delivery_orders/generate_pdf/' . $delivery_order->delivery_order_id); ?>"
                                   target="_blank">
                                    <i class="fa fa-print fa-margin"></i> <?php _trans('download_pdf'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('mailer/delivery_order/' . $delivery_order->delivery_order_id); ?>">
                                    <i class="fa fa-send fa-margin"></i> <?php _trans('send_email'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('delivery_orders/delete/' . $delivery_order->delivery_order_id); ?>"
                                   onclick="return confirm('<?php _trans('delete_delivery_order_warning'); ?>');">
                                    <i class="fa fa-trash-o fa-margin"></i> <?php _trans('delete'); ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
            <?php
            $delivery_order_idx++;
        } ?>
        </tbody>

    </table>
</div>
