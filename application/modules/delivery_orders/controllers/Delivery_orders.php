<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * InvoicePlane
 *
 * @author		InvoicePlane Developers & Contributors
 * @copyright	Copyright (c) 2012 - 2017 InvoicePlane.com
 * @license		https://invoiceplane.com/license.txt
 * @link		https://invoiceplane.com
 */

/**
 * Class Delivery_orders
 */
class Delivery_orders extends Admin_Controller
{
    /**
     * delivery_orders constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('mdl_delivery_orders');
    }

    public function index()
    {
        // Display all delivery_orders by default
        redirect('delivery_orders/status/all');
    }

    /**
     * @param string $status
     * @param int $page
     */
    public function status($status = 'all', $page = 0)
    {
        // Determine which group of delivery_orders to load
        switch ($status) {
            case 'draft':
                $this->mdl_delivery_orders->is_draft();
                break;
            case 'sent':
                $this->mdl_delivery_orders->is_sent();
                break;
            case 'viewed':
                $this->mdl_delivery_orders->is_viewed();
                break;
            case 'approved':
                $this->mdl_delivery_orders->is_approved();
                break;
            case 'rejected':
                $this->mdl_delivery_orders->is_rejected();
                break;
            case 'canceled':
                $this->mdl_delivery_orders->is_canceled();
                break;
        }

        $this->mdl_delivery_orders->paginate(site_url('delivery_orders/status/' . $status), $page);
        $delivery_orders = $this->mdl_delivery_orders->result();

        $this->layout->set(
            array(
                'delivery_orders' => $delivery_orders,
                'status' => $status,
                'filter_display' => true,
                'filter_placeholder' => trans('filter_delivery_orders'),
                'filter_method' => 'filter_delivery_orders',
                'delivery_order_statuses' => $this->mdl_delivery_orders->statuses()
            )
        );
 
        $this->layout->buffer('content', 'delivery_orders/index');
        $this->layout->render();
    }

    /**
     * @param $delivery_order_id
     */
    public function view($delivery_order_id)
    {
        $this->load->helper('custom_values');
        $this->load->model('mdl_delivery_order_items');
        $this->load->model('mdl_delivery_order_amounts');
        $this->load->model('tax_rates/mdl_tax_rates');
        $this->load->model('units/mdl_units');
        $this->load->model('mdl_delivery_order_tax_rates');
        $this->load->model('custom_fields/mdl_custom_fields');
        $this->load->model('custom_values/mdl_custom_values');
        $this->load->model('custom_fields/mdl_delivery_order_custom');
 
        $fields = $this->mdl_delivery_order_custom->by_id($delivery_order_id)->get()->result();
        $this->db->reset_query();

        $delivery_order_custom = $this->mdl_delivery_order_custom->where('delivery_order_id', $delivery_order_id)->get();

        if ($delivery_order_custom->num_rows()) {
            $delivery_order_custom = $delivery_order_custom->row();

            unset($delivery_order_custom->delivery_order_id, $delivery_order_custom->delivery_order_custom_id);

            foreach ($delivery_order_custom as $key => $val) {
                $this->mdl_delivery_orders->set_form_value('custom[' . $key . ']', $val);
            }
        }

        $delivery_order = $this->mdl_delivery_orders->get_by_id($delivery_order_id);


        if (!$delivery_order) {
            show_404();
        }

        $custom_fields = $this->mdl_custom_fields->by_table('ip_delivery_order_custom')->get()->result();
        $custom_values = [];
        foreach ($custom_fields as $custom_field) {
            if (in_array($custom_field->custom_field_type, $this->mdl_custom_values->custom_value_fields())) {
                $values = $this->mdl_custom_values->get_by_fid($custom_field->custom_field_id)->result();
                $custom_values[$custom_field->custom_field_id] = $values;
            }
        }

        foreach ($custom_fields as $cfield) {
            foreach ($fields as $fvalue) {
                if ($fvalue->delivery_order_custom_fieldid == $cfield->custom_field_id) {
                    // TODO: Hackish, may need a better optimization
                    $this->mdl_delivery_orders->set_form_value(
                        'custom[' . $cfield->custom_field_id . ']',
                        $fvalue->delivery_order_custom_fieldvalue
                    );
                    break;
                }
            }
        }
   
        $this->layout->set(
            array(
                'delivery_order' => $delivery_order,
                'items' => $this->mdl_delivery_order_items->where('delivery_order_id', $delivery_order_id)->get()->result(),
                //'balance' => $this->db->where('delivery_order_id', $delivery_order_id)->get('ip_delivery_order_amounts')->row(),
                'delivery_order_id' => $delivery_order_id,
                'tax_rates' => $this->mdl_tax_rates->get()->result(),
                'units' => $this->mdl_units->get()->result(),
                'delivery_order_tax_rates' => $this->mdl_delivery_order_tax_rates->where('delivery_order_id', $delivery_order_id)->get()->result(),
                'custom_fields' => $custom_fields,
                'custom_values' => $custom_values,
                'custom_js_vars' => array(
                    'currency_symbol' => get_setting('currency_symbol'),
                    'currency_symbol_placement' => get_setting('currency_symbol_placement'),
                    'decimal_point' => get_setting('decimal_point')
                ),
                'delivery_order_statuses' => $this->mdl_delivery_orders->statuses()
            )
        );

        $this->layout->buffer(
            array(
                array('modal_delete_delivery_order', 'delivery_orders/modal_delete_delivery_order'),
                array('modal_add_delivery_order_tax', 'delivery_orders/modal_add_delivery_order_tax'),
                array('content', 'delivery_orders/view')
            )
        );

        $this->layout->render();
    }

    /**
     * @param $delivery_order_id
     */
    public function delete($delivery_order_id)
    {
        // Delete the delivery_order
        $this->mdl_delivery_orders->delete($delivery_order_id);

        // Redirect to delivery_order index
        redirect('delivery_orders/index');
    }

    /**
     * @param $delivery_order_id
     * @param $item_id
     */
    public function delete_item($delivery_order_id, $item_id)
    { 
        // Delete delivery_order item
        $this->load->model('mdl_delivery_order_items');
        $this->mdl_delivery_order_items->delete($item_id);

        // Redirect to delivery_order view
        redirect('delivery_orders/view/' . $delivery_order_id);
    }

    /**
     * @param $delivery_order_id
     * @param bool $stream
     * @param null $delivery_order_template
     */
    public function generate_pdf($delivery_order_id, $stream = true, $delivery_order_template = null)
    {
        $this->load->helper('pdf');

        if (get_setting('mark_delivery_orders_sent_pdf') == 1) {
            $this->mdl_delivery_orders->mark_sent($delivery_order_id);
        }

        generate_delivery_order_pdf($delivery_order_id, $stream, $delivery_order_template);
    }

    /**
     * @param $delivery_order_id
     * @param $delivery_order_tax_rate_id
     */
    public function delete_delivery_order_tax($delivery_order_id, $delivery_order_tax_rate_id)
    {
        $this->load->model('mdl_delivery_order_tax_rates');
        $this->mdl_delivery_order_tax_rates->delete($delivery_order_tax_rate_id);

        $this->load->model('mdl_delivery_order_amounts');
        $this->mdl_delivery_order_amounts->calculate($delivery_order_id);

        redirect('delivery_orders/view/' . $delivery_order_id);
    }

    public function recalculate_all_delivery_orders()
    {
        $this->db->select('delivery_order_id');
        $delivery_order_ids = $this->db->get('ip_delivery_orders')->result();

        $this->load->model('mdl_delivery_order_amounts');

        foreach ($delivery_order_ids as $delivery_order_id) {
            $this->mdl_delivery_order_amounts->calculate($delivery_order_id->delivery_order_id);
        }
    }

}
