<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * InvoicePlane
 *
 * @author		InvoicePlane Developers & Contributors
 * @copyright	Copyright (c) 2012 - 2017 InvoicePlane.com
 * @license		https://invoiceplane.com/license.txt
 * @link		https://invoiceplane.com
 */

/**
 * Class Ajax
 */
class Ajax extends Admin_Controller
{
    public $ajax_controller = true;

    public function save()
    {
        $this->load->model('delivery_orders/mdl_delivery_order_items');
        $this->load->model('delivery_orders/mdl_delivery_orders');
        $this->load->model('units/mdl_units');

        $delivery_order_id = $this->input->post('delivery_order_id');

        $this->mdl_delivery_orders->set_id($delivery_order_id);
 
        if ($this->mdl_delivery_orders->run_validation('validation_rules_save_delivery_order')) {
        
            //cek status jika delivery order sudah dibuat invoice maka tidak bisa diganti
            $invoice_no = $this->mdl_delivery_orders->get_invoice($delivery_order_id);
            if ((isset($invoice_no)) && (!empty($invoice_no)) ){
                $response = array(
                    'success' => 0,
                    'validation_errors' => 'Cannot change, delivery order already created invoce with invoice number ' . $invoice_no
                );
            }else{
                $items = json_decode($this->input->post('items'));

                foreach ($items as $item) {
                    if ($item->item_name) {
                        $item->item_quantity = ($item->item_quantity ? standardize_amount($item->item_quantity) : floatval(0));
                        $item->item_price = ($item->item_quantity ? standardize_amount($item->item_price) : floatval(0));
                        $item->item_discount_amount = ($item->item_discount_amount) ? standardize_amount($item->item_discount_amount) : null;
                        $item->item_product_id = ($item->item_product_id ? $item->item_product_id : null);
                        $item->item_product_unit_id = ($item->item_product_unit_id ? $item->item_product_unit_id : null);
                        $item->item_product_unit = $this->mdl_units->get_name($item->item_product_unit_id, $item->item_quantity);
    
                        $item_id = ($item->item_id) ?: null;
                        unset($item->item_id);
    
                        $this->mdl_delivery_order_items->save($item_id, $item);
                    }
                }
    
                if ($this->input->post('delivery_order_discount_amount') === '') {
                    $delivery_order_discount_amount = floatval(0);
                } else {
                    $delivery_order_discount_amount = $this->input->post('delivery_order_discount_amount');
                }
    
                if ($this->input->post('delivery_order_discount_percent') === '') {
                    $delivery_order_discount_percent = floatval(0);
                } else {
                    $delivery_order_discount_percent = $this->input->post('delivery_order_discount_percent');
                }
    
                // Generate new delivery_order number if needed
                $delivery_order_number = $this->input->post('delivery_order_number');
                $delivery_order_status_id = $this->input->post('delivery_order_status_id');
    
                if (empty($delivery_order_number) && $delivery_order_status_id != 1) {
                    $delivery_order_group_id = $this->mdl_delivery_orders->get_invoice_group_id($delivery_order_id);
                    $delivery_order_number = $this->mdl_delivery_orders->get_delivery_order_number($delivery_order_group_id);
                }
    
                $db_array = array(
                    'delivery_order_number' => $delivery_order_number,
                    'delivery_order_date_created' => date_to_mysql($this->input->post('delivery_order_date_created')),
                  //  'delivery_order_date_expires' => date_to_mysql($this->input->post('delivery_order_date_expires')),
                    'delivery_order_status_id' => $delivery_order_status_id,
                    'delivery_order_password' => $this->input->post('delivery_order_password'),
                    'notes' => $this->input->post('notes'),
                    'delivery_order_discount_amount' => standardize_amount($delivery_order_discount_amount),
                    'delivery_order_discount_percent' => standardize_amount($delivery_order_discount_percent),
                );
    
                $this->mdl_delivery_orders->save($delivery_order_id, $db_array);
    
                // Recalculate for discounts
                $this->load->model('delivery_orders/mdl_delivery_order_amounts');
                $this->mdl_delivery_order_amounts->calculate($delivery_order_id);
    
                $response = array(
                    'success' => 1
                );
            }

        } else {
            $this->load->helper('json_error');
            $response = array(
                'success' => 0,
                'validation_errors' => json_errors()
            );
        }


        // Save all custom fields
        if ($this->input->post('custom')) {
            $db_array = array();

            $values = [];
            foreach ($this->input->post('custom') as $custom) {
                if (preg_match("/^(.*)\[\]$/i", $custom['name'], $matches)) {
                    $values[$matches[1]][] = $custom['value'];
                } else {
                    $values[$custom['name']] = $custom['value'];
                }
            }

            foreach ($values as $key => $value) {
                preg_match("/^custom\[(.*?)\](?:\[\]|)$/", $key, $matches);
                if ($matches) {
                    $db_array[$matches[1]] = $value;
                }
            }
            $this->load->model('custom_fields/mdl_delivery_order_custom');
            $result = $this->mdl_delivery_order_custom->save_custom($delivery_order_id, $db_array);
            if ($result !== true) {
                $response = array(
                    'success' => 0,
                    'validation_errors' => $result
                );

                echo json_encode($response);
                exit;
            }
        }

        echo json_encode($response);
    }

    public function save_delivery_order_tax_rate()
    {
        $this->load->model('delivery_orders/mdl_delivery_order_tax_rates');

        if ($this->mdl_delivery_order_tax_rates->run_validation()) {
            $this->mdl_delivery_order_tax_rates->save();
 
            $response = array(
                'success' => 1
            );
        } else {
            $response = array(
                'success' => 0,
                'validation_errors' => $this->mdl_delivery_order_tax_rates->validation_errors
            );
        }

        echo json_encode($response);
    }

    public function create()
    {
        $this->load->model('delivery_orders/mdl_delivery_orders');

        if ($this->mdl_delivery_orders->run_validation()) {
            $delivery_order_id = $this->mdl_delivery_orders->create();

            $response = array(
                'success' => 1,
                'delivery_order_id' => $delivery_order_id
            );
        } else {
            $this->load->helper('json_error');
            $response = array(
                'success' => 0,
                'validation_errors' => json_errors()
            );
        }

        echo json_encode($response);
    }

    public function modal_change_client()
    {
        $this->load->module('layout');
        $this->load->model('clients/mdl_clients');

        $data = array(
            'client_id' => $this->input->post('client_id'),
            'delivery_order_id' => $this->input->post('delivery_order_id'),
            'clients' => $this->mdl_clients->get_latest(),
        );

        $this->layout->load_view('delivery_orders/modal_change_client', $data);
    }

    public function change_client()
    {
        $this->load->model('delivery_orders/mdl_delivery_orders');
        $this->load->model('clients/mdl_clients');

        // Get the client ID
        $client_id = $this->input->post('client_id');
        $client = $this->mdl_clients->where('ip_clients.client_id', $client_id)
            ->get()->row();

        if (!empty($client)) {
            $delivery_order_id = $this->input->post('delivery_order_id');

            $db_array = array(
                'client_id' => $client_id,
            );
            $this->db->where('delivery_order_id', $delivery_order_id);
            $this->db->update('ip_delivery_orders', $db_array);

            $response = array(
                'success' => 1,
                'delivery_order_id' => $delivery_order_id
            );
        } else {
            $this->load->helper('json_error');
            $response = array(
                'success' => 0,
                'validation_errors' => json_errors()
            );
        }

        echo json_encode($response);
    }

    public function get_item()
    {
        $this->load->model('delivery_orders/mdl_delivery_order_items');

        $item = $this->mdl_delivery_order_items->get_by_id($this->input->post('item_id'));

        echo json_encode($item);
    }

    public function modal_create_delivery_order()
    {
        $this->load->module('layout');
        $this->load->model('invoice_groups/mdl_invoice_groups');
        $this->load->model('tax_rates/mdl_tax_rates');
        $this->load->model('clients/mdl_clients');
        $this->load->model('sales_orders/mdl_sales_orders');
  
        $open_so = $this->mdl_sales_orders
            ->where('ip_sales_order_amounts.sales_order_qty_balance >', 0)
            ->where('ip_sales_orders.sales_order_status_id', 4)
            //->or_where('ip_sales_order_amounts.sales_order_qty_balance <', 0)
            ->get()->result();

        $data = array(
            'invoice_groups' => $this->mdl_invoice_groups->get()->result(),
            'tax_rates' => $this->mdl_tax_rates->get()->result(),
            'client' => $this->mdl_clients->get_by_id($this->input->post('client_id')),
            'clients' => $this->mdl_clients->get_latest(),
            'open_so' => $open_so,
        );

        $this->layout->load_view('delivery_orders/modal_create_delivery_order', $data);
    }

    public function modal_copy_delivery_order()
    {
        $this->load->module('layout');

        $this->load->model('delivery_orders/mdl_delivery_orders');
        $this->load->model('invoice_groups/mdl_invoice_groups');
        $this->load->model('tax_rates/mdl_tax_rates');
        $this->load->model('clients/mdl_clients');

        $data = array(
            'invoice_groups' => $this->mdl_invoice_groups->get()->result(),
            'tax_rates' => $this->mdl_tax_rates->get()->result(),
            'delivery_order_id' => $this->input->post('delivery_order_id'),
            'delivery_order' => $this->mdl_delivery_orders->where('ip_delivery_orders.delivery_order_id', $this->input->post('delivery_order_id'))->get()->row(),
            'client' => $this->mdl_clients->get_by_id($this->input->post('client_id')),
        );

        $this->layout->load_view('delivery_orders/modal_copy_delivery_order', $data);
    }

    public function copy_delivery_order()
    {
        $this->load->model('delivery_orders/mdl_delivery_orders');
        $this->load->model('delivery_orders/mdl_delivery_order_items');
        $this->load->model('delivery_orders/mdl_delivery_order_tax_rates');

        if ($this->mdl_delivery_orders->run_validation()) {
            $target_id = $this->mdl_delivery_orders->save();
            $source_id = $this->input->post('delivery_order_id');

            $this->mdl_delivery_orders->copy_delivery_order($source_id, $target_id);

            $response = array(
                'success' => 1,
                'delivery_order_id' => $target_id
            );
        } else {
            $this->load->helper('json_error');
            $response = array(
                'success' => 0,
                'validation_errors' => json_errors()
            );
        }

        echo json_encode($response);
    }
 
    public function modal_delivery_order_to_invoice($delivery_order_id)
    {
        $this->load->model('invoice_groups/mdl_invoice_groups');
        $this->load->model('delivery_orders/mdl_delivery_orders');

        $data = array(
            'invoice_groups' => $this->mdl_invoice_groups->get()->result(),
            'delivery_order_id' => $delivery_order_id,
            'delivery_order' => $this->mdl_delivery_orders->where('ip_delivery_orders.delivery_order_id', $delivery_order_id)->get()->row()
        );

        $this->load->view('delivery_orders/modal_delivery_order_to_invoice', $data);
    }

    public function delivery_order_to_invoice()
    {
        $this->load->model(
            array(
                'invoices/mdl_invoices',
                'invoices/mdl_items',
                'delivery_orders/mdl_delivery_orders',
                'delivery_orders/mdl_delivery_order_items',
                'invoices/mdl_invoice_tax_rates',
                'delivery_orders/mdl_delivery_order_tax_rates',
                'products/mdl_products'
            )
        );
 
        if ($this->mdl_invoices->run_validation()) {
                // Get the delivery_order
                $delivery_order = $this->mdl_delivery_orders->get_by_id($this->input->post('delivery_order_id'));

                $invoice_id = $this->mdl_invoices->create(null, false);

                // Update the discounts 
                $this->db->where('invoice_id', $invoice_id);
                $this->db->set('delivery_order_id', $delivery_order->delivery_order_id);
                $this->db->set('client_id', $delivery_order->client_id);
                $this->db->set('invoice_discount_amount', $delivery_order->delivery_order_discount_amount);
                $this->db->set('invoice_discount_percent', $delivery_order->delivery_order_discount_percent);
                $this->db->set('invoice_shipping_charges', $delivery_order->delivery_order_shipping_charges);
                $this->db->update('ip_invoices');

                $where = array(
                            'delivery_order_id' => $this->input->post('delivery_order_id'),
                            'ip_delivery_order_items.item_quantity_balance >' => 0
                        );
                $delivery_order_items = $this->mdl_delivery_order_items->where($where)->get()->result();
                
 
                foreach ($delivery_order_items as $delivery_order_item) {
                    $products = $this->mdl_products->where('product_id',$delivery_order_item->item_product_id)->get()->row();
                    $db_array = array(
                        'invoice_id' => $invoice_id,
                        'item_tax_rate_id' => $delivery_order_item->item_tax_rate_id,
                        'item_product_id' => $delivery_order_item->item_product_id,
                        'item_name' => $delivery_order_item->item_name,
                        'item_description' => $delivery_order_item->item_description,
                        'item_quantity' => $delivery_order_item->item_quantity_balance,
                        'item_price' => $products->product_price,
                        'item_product_unit_id' => $delivery_order_item->item_product_unit_id,
                        'item_product_unit' => $delivery_order_item->item_product_unit,
                        'item_discount_amount' => $delivery_order_item->item_discount_amount,
                        'item_order' => $delivery_order_item->item_order,
                        'delivery_order_id' => $delivery_order_item->delivery_order_id
                    );

                    $this->mdl_items->save(null, $db_array);
                }
 
                $delivery_order_tax_rates = $this->mdl_delivery_order_tax_rates->where('delivery_order_id', $this->input->post('delivery_order_id'))->get()->result();

                foreach ($delivery_order_tax_rates as $delivery_order_tax_rate) {
                    $db_array = array(
                        'invoice_id' => $invoice_id,
                        'tax_rate_id' => $delivery_order_tax_rate->tax_rate_id,
                        'include_item_tax' => $delivery_order_tax_rate->include_item_tax,
                        'invoice_tax_rate_amount' => $delivery_order_tax_rate->delivery_order_tax_rate_amount
                    );

                    $this->mdl_invoice_tax_rates->save(null, $db_array);
                }

                $response = array(
                    'success' => 1,
                    'invoice_id' => $invoice_id
                );
            
        } else {
            $this->load->helper('json_error');
            $response = array(
                'success' => 0,
                'validation_errors' => json_errors()
            );
        }

        echo json_encode($response);
    }

}
