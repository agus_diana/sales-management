<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * InvoicePlane
 *
 * @author		InvoicePlane Developers & Contributors
 * @copyright	Copyright (c) 2012 - 2017 InvoicePlane.com
 * @license		https://invoiceplane.com/license.txt
 * @link		https://invoiceplane.com
 */

/**
 * Class Mdl_delivery_order_Amounts
 */
class Mdl_delivery_order_Amounts extends CI_Model
{
    /**
     * IP_delivery_order_AMOUNTS
     * delivery_order_amount_id
     * delivery_order_id
     * delivery_order_item_subtotal      SUM(item_subtotal)
     * delivery_order_item_tax_total     SUM(item_tax_total)
     * delivery_order_tax_total
     * delivery_order_total              delivery_order_item_subtotal + delivery_order_item_tax_total + delivery_order_tax_total
     *
     * IP_delivery_order_ITEM_AMOUNTS
     * item_amount_id
     * item_id
     * item_tax_rate_id
     * item_subtotal             item_quantity * item_price
     * item_tax_total            item_subtotal * tax_rate_percent
     * item_total                item_subtotal + item_tax_total
     *
     * @param $delivery_order_id
     */
    public function calculate($delivery_order_id)
    {
        // Get the basic totals
        $query = $this->db->query("
            SELECT SUM(item_subtotal) AS delivery_order_item_subtotal,
		        SUM(item_tax_total) AS delivery_order_item_tax_total,
		        SUM(item_subtotal) + SUM(item_tax_total) AS delivery_order_total,
		        SUM(item_discount) AS delivery_order_item_discount
		    FROM ip_delivery_order_item_amounts
		    WHERE item_id
		        IN (SELECT item_id FROM ip_delivery_order_items WHERE delivery_order_id = " . $this->db->escape($delivery_order_id) . ")
            ");

        $delivery_order_amounts = $query->row();

        $delivery_order_item_subtotal = $delivery_order_amounts->delivery_order_item_subtotal - $delivery_order_amounts->delivery_order_item_discount;
        $delivery_order_subtotal = $delivery_order_item_subtotal + $delivery_order_amounts->delivery_order_item_tax_total;
        $delivery_order_total = $this->calculate_discount($delivery_order_id, $delivery_order_subtotal);
 
        // Create the database array and insert or update
        $db_array = array(
            'delivery_order_id' => $delivery_order_id,
            'delivery_order_item_subtotal' => $delivery_order_item_subtotal,
            'delivery_order_item_tax_total' => $delivery_order_amounts->delivery_order_item_tax_total,
            'delivery_order_total' => $delivery_order_total,
        );

        $this->db->where('delivery_order_id', $delivery_order_id);
        if ($this->db->get('ip_delivery_order_amounts')->num_rows()) {
            // The record already exists; update it
            $this->db->where('delivery_order_id', $delivery_order_id);
            $this->db->update('ip_delivery_order_amounts', $db_array);
        } else {
            // The record does not yet exist; insert it
            $this->db->insert('ip_delivery_order_amounts', $db_array);
        }

        // Calculate the delivery_order taxes
        $this->calculate_delivery_order_taxes($delivery_order_id);
    }

    /**
     * @param $delivery_order_id
     * @param $delivery_order_total
     * @return float
     */
    public function calculate_discount($delivery_order_id, $delivery_order_total)
    {
        $this->db->where('delivery_order_id', $delivery_order_id);
        $delivery_order_data = $this->db->get('ip_delivery_orders')->row();

        $total = (float)number_format($delivery_order_total, 2, '.', '');
        $discount_amount = (float)number_format($delivery_order_data->delivery_order_discount_amount, 2, '.', '');
        $discount_percent = (float)number_format($delivery_order_data->delivery_order_discount_percent, 2, '.', '');

        $shipping_charges = (float)number_format($delivery_order_data->delivery_order_shipping_charges, 2, '.', '');
        $total = $total + $shipping_charges;

        $total = $total - $discount_amount;
        $total = $total - round(($total / 100 * $discount_percent), 2);

        return $total;
    }

    /**
     * @param $delivery_order_id
     */
    public function calculate_delivery_order_taxes($delivery_order_id)
    {
        // First check to see if there are any delivery_order taxes applied
        $this->load->model('delivery_orders/mdl_delivery_order_tax_rates');
        $delivery_order_tax_rates = $this->mdl_delivery_order_tax_rates->where('delivery_order_id', $delivery_order_id)->get()->result();

        if ($delivery_order_tax_rates) {
            // There are delivery_order taxes applied
            // Get the current delivery_order amount record
            $delivery_order_amount = $this->db->where('delivery_order_id', $delivery_order_id)->get('ip_delivery_order_amounts')->row();

            // Loop through the delivery_order taxes and update the amount for each of the applied delivery_order taxes
            foreach ($delivery_order_tax_rates as $delivery_order_tax_rate) {
                if ($delivery_order_tax_rate->include_item_tax) {
                    // The delivery_order tax rate should include the applied item tax
                    $delivery_order_tax_rate_amount = ($delivery_order_amount->delivery_order_item_subtotal + $delivery_order_amount->delivery_order_item_tax_total) * ($delivery_order_tax_rate->delivery_order_tax_rate_percent / 100);
                } else {
                    // The delivery_order tax rate should not include the applied item tax
                    $delivery_order_tax_rate_amount = $delivery_order_amount->delivery_order_item_subtotal * ($delivery_order_tax_rate->delivery_order_tax_rate_percent / 100);
                }

                // Update the delivery_order tax rate record
                $db_array = array(
                    'delivery_order_tax_rate_amount' => $delivery_order_tax_rate_amount
                );
                $this->db->where('delivery_order_tax_rate_id', $delivery_order_tax_rate->delivery_order_tax_rate_id);
                $this->db->update('ip_delivery_order_tax_rates', $db_array);
            }

            // Update the delivery_order amount record with the total delivery_order tax amount
            $this->db->query("
                UPDATE ip_delivery_order_amounts SET delivery_order_tax_total =
                (
                    SELECT SUM(delivery_order_tax_rate_amount)
                    FROM ip_delivery_order_tax_rates
                    WHERE delivery_order_id = " . $this->db->escape($delivery_order_id) . "
                )
                WHERE delivery_order_id = " . $this->db->escape($delivery_order_id)
            );

            // Get the updated delivery_order amount record
            $delivery_order_amount = $this->db->where('delivery_order_id', $delivery_order_id)->get('ip_delivery_order_amounts')->row();

            // Recalculate the delivery_order total
            $delivery_order_total = $delivery_order_amount->delivery_order_item_subtotal + $delivery_order_amount->delivery_order_item_tax_total + $delivery_order_amount->delivery_order_tax_total;

            $delivery_order_total = $this->calculate_discount($delivery_order_id, $delivery_order_total);

            // Update the delivery_order amount record
            $db_array = array(
                'delivery_order_total' => $delivery_order_total
            );

            $this->db->where('delivery_order_id', $delivery_order_id);
            $this->db->update('ip_delivery_order_amounts', $db_array);
        } else {
            // No delivery_order taxes applied

            $db_array = array(
                'delivery_order_tax_total' => '0.00'
            );

            $this->db->where('delivery_order_id', $delivery_order_id);
            $this->db->update('ip_delivery_order_amounts', $db_array);
        }
    }

    /**
     * @param null $period
     * @return mixed
     */
    public function get_total_delivery_orderd($period = null)
    {
        switch ($period) {
            case 'month':
                return $this->db->query("
					SELECT SUM(delivery_order_total) AS total_delivery_orderd 
					FROM ip_delivery_order_amounts
					WHERE delivery_order_id IN 
					(SELECT delivery_order_id FROM ip_delivery_orders
					WHERE MONTH(delivery_order_date_created) = MONTH(NOW()) 
					AND YEAR(delivery_order_date_created) = YEAR(NOW()))")->row()->total_delivery_orderd;
            case 'last_month':
                return $this->db->query("
					SELECT SUM(delivery_order_total) AS total_delivery_orderd 
					FROM ip_delivery_order_amounts
					WHERE delivery_order_id IN 
					(SELECT delivery_order_id FROM ip_delivery_orders
					WHERE MONTH(delivery_order_date_created) = MONTH(NOW() - INTERVAL 1 MONTH)
					AND YEAR(delivery_order_date_created) = YEAR(NOW() - INTERVAL 1 MONTH))")->row()->total_delivery_orderd;
            case 'year':
                return $this->db->query("
					SELECT SUM(delivery_order_total) AS total_delivery_orderd 
					FROM ip_delivery_order_amounts
					WHERE delivery_order_id IN 
					(SELECT delivery_order_id FROM ip_delivery_orders WHERE YEAR(delivery_order_date_created) = YEAR(NOW()))")->row()->total_delivery_orderd;
            case 'last_year':
                return $this->db->query("
					SELECT SUM(delivery_order_total) AS total_delivery_orderd 
					FROM ip_delivery_order_amounts
					WHERE delivery_order_id IN 
					(SELECT delivery_order_id FROM ip_delivery_orders WHERE YEAR(delivery_order_date_created) = YEAR(NOW() - INTERVAL 1 YEAR))")->row()->total_delivery_orderd;
            default:
                return $this->db->query("SELECT SUM(delivery_order_total) AS total_delivery_orderd FROM ip_delivery_order_amounts")->row()->total_delivery_orderd;
        }
    }

    /**
     * @param string $period
     * @return array
     */
    public function get_status_totals($period = '')
    {
        switch ($period) {
            default:
            case 'this-month':
                $results = $this->db->query("
					SELECT delivery_order_status_id,
					    SUM(delivery_order_total) AS sum_total,
					    COUNT(*) AS num_total
					FROM ip_delivery_order_amounts
					JOIN ip_delivery_orders ON ip_delivery_orders.delivery_order_id = ip_delivery_order_amounts.delivery_order_id
                        AND MONTH(ip_delivery_orders.delivery_order_date_created) = MONTH(NOW())
                        AND YEAR(ip_delivery_orders.delivery_order_date_created) = YEAR(NOW())
					GROUP BY ip_delivery_orders.delivery_order_status_id")->result_array();
                break;
            case 'last-month':
                $results = $this->db->query("
					SELECT delivery_order_status_id,
					    SUM(delivery_order_total) AS sum_total,
					    COUNT(*) AS num_total
					FROM ip_delivery_order_amounts
					JOIN ip_delivery_orders ON ip_delivery_orders.delivery_order_id = ip_delivery_order_amounts.delivery_order_id
                        AND MONTH(ip_delivery_orders.delivery_order_date_created) = MONTH(NOW() - INTERVAL 1 MONTH)
                        AND YEAR(ip_delivery_orders.delivery_order_date_created) = YEAR(NOW())
					GROUP BY ip_delivery_orders.delivery_order_status_id")->result_array();
                break;
            case 'this-quarter':
                $results = $this->db->query("
					SELECT delivery_order_status_id,
					    SUM(delivery_order_total) AS sum_total,
					    COUNT(*) AS num_total
					FROM ip_delivery_order_amounts
					JOIN ip_delivery_orders ON ip_delivery_orders.delivery_order_id = ip_delivery_order_amounts.delivery_order_id
                        AND QUARTER(ip_delivery_orders.delivery_order_date_created) = QUARTER(NOW())
                        AND YEAR(ip_delivery_orders.delivery_order_date_created) = YEAR(NOW())
					GROUP BY ip_delivery_orders.delivery_order_status_id")->result_array();
                break;
            case 'last-quarter':
                $results = $this->db->query("
					SELECT delivery_order_status_id,
					    SUM(delivery_order_total) AS sum_total,
					    COUNT(*) AS num_total
					FROM ip_delivery_order_amounts
					JOIN ip_delivery_orders ON ip_delivery_orders.delivery_order_id = ip_delivery_order_amounts.delivery_order_id
                        AND QUARTER(ip_delivery_orders.delivery_order_date_created) = QUARTER(NOW() - INTERVAL 1 QUARTER)
                        AND YEAR(ip_delivery_orders.delivery_order_date_created) = YEAR(NOW())
					GROUP BY ip_delivery_orders.delivery_order_status_id")->result_array();
                break;
            case 'this-year':
                $results = $this->db->query("
					SELECT delivery_order_status_id,
					    SUM(delivery_order_total) AS sum_total,
					    COUNT(*) AS num_total
					FROM ip_delivery_order_amounts
					JOIN ip_delivery_orders ON ip_delivery_orders.delivery_order_id = ip_delivery_order_amounts.delivery_order_id
                        AND YEAR(ip_delivery_orders.delivery_order_date_created) = YEAR(NOW())
					GROUP BY ip_delivery_orders.delivery_order_status_id")->result_array();
                break;
            case 'last-year':
                $results = $this->db->query("
					SELECT delivery_order_status_id,
					    SUM(delivery_order_total) AS sum_total,
					    COUNT(*) AS num_total
					FROM ip_delivery_order_amounts
					JOIN ip_delivery_orders ON ip_delivery_orders.delivery_order_id = ip_delivery_order_amounts.delivery_order_id
                        AND YEAR(ip_delivery_orders.delivery_order_date_created) = YEAR(NOW() - INTERVAL 1 YEAR)
					GROUP BY ip_delivery_orders.delivery_order_status_id")->result_array();
                break;
        }

        $return = array();

        foreach ($this->mdl_delivery_orders->statuses() as $key => $status) {
            $return[$key] = array(
                'delivery_order_status_id' => $key,
                'class' => $status['class'],
                'label' => $status['label'],
                'href' => $status['href'],
                'sum_total' => 0,
                'num_total' => 0
            );
        }

        foreach ($results as $result) {
            $return[$result['delivery_order_status_id']] = array_merge($return[$result['delivery_order_status_id']], $result);
        }

        return $return;
    }

}
