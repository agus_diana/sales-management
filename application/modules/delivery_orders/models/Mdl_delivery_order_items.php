<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * InvoicePlane
 *
 * @author		InvoicePlane Developers & Contributors
 * @copyright	Copyright (c) 2012 - 2017 InvoicePlane.com
 * @license		https://invoiceplane.com/license.txt
 * @link		https://invoiceplane.com
 */

/**
 * Class Mdl_delivery_order_Items
 */
class Mdl_delivery_order_Items extends Response_Model
{
    public $table = 'ip_delivery_order_items';
    public $primary_key = 'ip_delivery_order_items.item_id';
    public $date_created_field = 'item_date_added';

    public function default_select()
    {
        $this->db->select('ip_sales_order_items.item_quantity_balance as sales_order_item_quantity_balance, ip_delivery_order_item_amounts.*, ip_delivery_order_items.*, item_tax_rates.tax_rate_percent AS item_tax_rate_percent');
    }

    public function default_order_by()
    {
        $this->db->order_by('ip_delivery_order_items.item_order');
    }

    public function default_join()
    {
        $this->db->join('ip_delivery_order_item_amounts', 'ip_delivery_order_item_amounts.item_id = ip_delivery_order_items.item_id', 'left');
        $this->db->join('ip_tax_rates AS item_tax_rates', 'item_tax_rates.tax_rate_id = ip_delivery_order_items.item_tax_rate_id', 'left');
        $this->db->join('ip_sales_order_items', 'ip_sales_order_items.sales_order_id = ip_delivery_order_items.sales_order_id and ip_sales_order_items.item_product_id = ip_delivery_order_items.item_product_id', 'left');
    }

    /**
     * @return array
     */
    public function validation_rules()
    {
        return array(
            'delivery_order_id' => array(
                'field' => 'delivery_order_id',
                'label' => trans('delivery_order'),
                'rules' => 'required'
            ),
            'item_name' => array(
                'field' => 'item_name',
                'label' => trans('item_name'),
                'rules' => 'required'
            ),
            'item_description' => array(
                'field' => 'item_description',
                'label' => trans('description')
            ),
            'item_quantity' => array(
                'field' => 'item_quantity',
                'label' => trans('quantity'),
            ),
            'item_price' => array(
                'field' => 'item_price',
                'label' => trans('price'),
            ),
            'item_tax_rate_id' => array(
                'field' => 'item_tax_rate_id',
                'label' => trans('item_tax_rate')
            ),
            'item_product_id' => array(
                'field' => 'item_product_id',
                'label' => trans('original_product')
            ),
        );
    }

    /**
     * @param null $id
     * @param null $db_array
     * @return int|null
     */
    public function save($id = null, $db_array = null)
    {
        $id = parent::save($id, $db_array);

        $this->load->model('delivery_orders/mdl_delivery_order_item_amounts');
        $this->mdl_delivery_order_item_amounts->calculate($id);

        $this->load->model('delivery_orders/mdl_delivery_order_amounts');

        if (is_object($db_array) && isset($db_array->delivery_order_id)) {
            $this->mdl_delivery_order_amounts->calculate($db_array->delivery_order_id);
        } elseif (is_array($db_array) && isset($db_array['delivery_order_id'])) {
            $this->mdl_delivery_order_amounts->calculate($db_array['delivery_order_id']);
        }

        return $id;
    }

    /**
     * @param int $item_id
     */
    public function delete($item_id)
    {
        // Get the delivery_order id so we can recalculate delivery_order amounts
        $this->db->select('delivery_order_id');
        $this->db->where('item_id', $item_id);
        $delivery_order_id = $this->db->get('ip_delivery_order_items')->row()->delivery_order_id;

        // Delete the item amounts
        $this->db->where('item_id', $item_id);
        $this->db->delete('ip_delivery_order_item_amounts');

        // Delete the item
        parent::delete($item_id);

        // Recalculate delivery_order amounts
        $this->load->model('delivery_orders/mdl_delivery_order_amounts');
        $this->mdl_delivery_order_amounts->calculate($delivery_order_id);
    }

}
