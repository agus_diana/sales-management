<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * InvoicePlane
 *
 * @author		InvoicePlane Developers & Contributors
 * @copyright	Copyright (c) 2012 - 2017 InvoicePlane.com
 * @license		https://invoiceplane.com/license.txt
 * @link		https://invoiceplane.com
 */

/**
 * Class Mdl_delivery_orders
 */
class Mdl_delivery_orders extends Response_Model
{
    public $table = 'ip_delivery_orders';
    public $primary_key = 'ip_delivery_orders.delivery_order_id';
    public $date_modified_field = 'delivery_order_date_modified';

    /**
     * @return array
     */
    public function statuses()
    {
        return array(
            '1' => array(
                'label' => trans('draft'),
                'class' => 'draft',
                'href' => 'delivery_orders/status/draft'
            ),
        
            '2' => array(
                'label' => trans('sent'),
                'class' => 'sent',
                'href' => 'delivery_orders/status/sent'
            ),
        /*
            '3' => array(
                'label' => trans('viewed'),
                'class' => 'viewed',
                'href' => 'delivery_orders/status/viewed'
            ), */
            '4' => array(
                'label' => trans('approved'),
                'class' => 'approved',
                'href' => 'delivery_orders/status/approved'
            ),
        /*    '5' => array(
                'label' => trans('rejected'),
                'class' => 'rejected',
                'href' => 'delivery_orders/status/rejected'
            ), */
            '6' => array(
                'label' => trans('canceled'),
                'class' => 'canceled',
                'href' => 'delivery_orders/status/canceled'
            )
        );
    }

    public function default_select()
    {
        $this->db->select("
            SQL_CALC_FOUND_ROWS
            ip_users.user_name,
			ip_users.user_company,
			ip_users.user_address_1,
			ip_users.user_address_2,
			ip_users.user_city,
			ip_users.user_state,
			ip_users.user_zip,
			ip_users.user_country,
			ip_users.user_phone,
			ip_users.user_fax,
			ip_users.user_mobile,
			ip_users.user_email,
			ip_users.user_web,
			ip_users.user_vat_id,
			ip_users.user_tax_code,
            ip_clients.*,
            (select a.sales_order_number from ip_sales_orders a where a.sales_order_id = ip_delivery_orders.sales_order_id) as sales_order_number,
            ip_delivery_order_amounts.delivery_order_amount_id,
            ip_delivery_order_amounts.delivery_order_qty_total,
            IFnull(ip_delivery_order_amounts.delivery_order_qty_balance, '0.00') AS delivery_order_qty_balance,
			IFnull(ip_delivery_order_amounts.delivery_order_item_subtotal, '0.00') AS delivery_order_item_subtotal,
			IFnull(ip_delivery_order_amounts.delivery_order_item_tax_total, '0.00') AS delivery_order_item_tax_total,
			IFnull(ip_delivery_order_amounts.delivery_order_tax_total, '0.00') AS delivery_order_tax_total,
            IFnull(ip_delivery_order_amounts.delivery_order_total, '0.00') AS delivery_order_total,
            IFnull(ip_delivery_order_amounts.delivery_order_balance, '0.00') + IFnull(ip_delivery_order_amounts.delivery_order_tax_total, '0.00') AS delivery_order_balance,
			ip_delivery_orders.*", false);
    }

    public function default_order_by()
    {
        $this->db->order_by('ip_delivery_orders.delivery_order_id DESC');
    }

    public function default_join()
    {
        $this->db->join('ip_clients', 'ip_clients.client_id = ip_delivery_orders.client_id');
        $this->db->join('ip_users', 'ip_users.user_id = ip_delivery_orders.user_id');
        $this->db->join('ip_delivery_order_amounts', 'ip_delivery_order_amounts.delivery_order_id = ip_delivery_orders.delivery_order_id', 'left');
    }
 
    /**
     * @return array
     */
    public function validation_rules()
    {
        return array(
            'client_id' => array(
                'field' => 'client_id',
                'label' => trans('client')
//                'rules' => 'required'
            ),
            'delivery_order_date_created' => array(
                'field' => 'delivery_order_date_created',
                'label' => trans('delivery_order_date'),
                'rules' => 'required'
            ),
            'invoice_group_id' => array(
                'field' => 'invoice_group_id',
                'label' => trans('delivery_order_group'),
                'rules' => 'required'
            ),
            'delivery_order_password' => array(
                'field' => 'delivery_order_password',
                'label' => trans('delivery_order_password')
            ),
            'user_id' => array(
                'field' => 'user_id',
                'label' => trans('user'),
                'rule' => 'required'
            )
        );
    }

    /**
     * @param $delivery_order_id
     * @return mixed
     */
    public function get_invoice($delivery_order_id)
    {
        $this->load->model('invoices/mdl_invoices');

        $this->db->select('invoice_number');
        $this->db->where('delivery_order_id', $delivery_order_id);
        $invoice_results = $this->db->get('ip_invoices');
        $hasil=null;
        foreach ($invoice_results->result_array() as $value){
            $hasil[]=$value['invoice_number'];
        }
        if ($hasil){
            return implode(",",$hasil);
        }
        return $hasil;
    }

    /**
     * @param $delivery_order_id
     * @return mixed
     */
    public function check_delivery_order_outstanding($delivery_order_id)
    {
        $this->load->model('delivery_orders/mdl_delivery_order_amounts');

        $this->db->select('delivery_order_balance');
        $this->db->where('delivery_order_id', $delivery_order_id);
        $balance = $this->db->get('ip_delivery_order_amounts');
        return $balance->row();
    }


    /**
     * @return array
     */
    public function validation_rules_save_delivery_order()
    {
        return array(
            'delivery_order_number' => array(
                'field' => 'delivery_order_number',
                'label' => trans('delivery_order') . ' #',
                'rules' => 'is_unique[ip_delivery_orders.delivery_order_number' . (($this->id) ? '.delivery_order_id.' . $this->id : '') . ']'
            ),
            'delivery_order_date_created' => array(
                'field' => 'delivery_order_date_created',
                'label' => trans('date'),
                'rules' => 'required'
            ),
        /*    'delivery_order_date_expires' => array(
                'field' => 'delivery_order_date_expires',
                'label' => trans('due_date'),
                'rules' => 'required'
            ), */
            'delivery_order_password' => array(
                'field' => 'delivery_order_password',
                'label' => trans('delivery_order_password')
            )
        );
    }

    /**
     * @param null $db_array 
     * @return int|null
     */
    public function create($db_array = null)
    {
        $delivery_order_id = parent::save(null, $db_array);

        // Create an delivery_order amount record
        $db_array = array(
            'delivery_order_id' => $delivery_order_id
        );

        $this->db->insert('ip_delivery_order_amounts', $db_array);

        // Create the default invoice tax record if applicable
        if (get_setting('default_invoice_tax_rate')) {
            $db_array = array(
                'delivery_order_id' => $delivery_order_id,
                'tax_rate_id' => get_setting('default_invoice_tax_rate'),
                'include_item_tax' => get_setting('default_include_item_tax'),
                'delivery_order_tax_rate_amount' => 0
            );

            $this->db->insert('ip_delivery_order_tax_rates', $db_array);
        }

        return $delivery_order_id;
    }

    /**
     * Copies delivery_order items, tax rates, etc from source to target
     * @param int $source_id
     * @param int $target_id
     */
    public function copy_delivery_order($source_id, $target_id)
    {
        $this->load->model('delivery_orders/mdl_delivery_order_items');

        $delivery_order_items = $this->mdl_delivery_order_items->where('delivery_order_id', $source_id)->get()->result();

        foreach ($delivery_order_items as $delivery_order_item) {
            $db_array = array(
                'delivery_order_id' => $target_id,
                'item_tax_rate_id' => $delivery_order_item->item_tax_rate_id,
                'item_name' => $delivery_order_item->item_name,
                'item_description' => $delivery_order_item->item_description,
                'item_quantity' => $delivery_order_item->item_quantity,
                'item_price' => $delivery_order_item->item_price,
                'item_order' => $delivery_order_item->item_order
            );

            $this->mdl_delivery_order_items->save(null, $db_array);
        }

        $delivery_order_tax_rates = $this->mdl_delivery_order_tax_rates->where('delivery_order_id', $source_id)->get()->result();

        foreach ($delivery_order_tax_rates as $delivery_order_tax_rate) {
            $db_array = array(
                'delivery_order_id' => $target_id,
                'tax_rate_id' => $delivery_order_tax_rate->tax_rate_id,
                'include_item_tax' => $delivery_order_tax_rate->include_item_tax,
                'delivery_order_tax_rate_amount' => $delivery_order_tax_rate->delivery_order_tax_rate_amount
            );

            $this->mdl_delivery_order_tax_rates->save(null, $db_array);
        }

        // Copy the custom fields
        $this->load->model('custom_fields/mdl_delivery_order_custom');
        $db_array = $this->mdl_delivery_order_custom->where('delivery_order_id', $source_id)->get()->row_array();

        if (count($db_array) > 2) {
            unset($db_array['delivery_order_custom_id']);
            $db_array['delivery_order_id'] = $target_id;
            $this->mdl_delivery_order_custom->save_custom($target_id, $db_array);
        }
    }

    /**
     * @return array
     */
    public function db_array()
    {
        $db_array = parent::db_array();

        // Get the client id for the submitted delivery_order
        if (!isset($db_array['client_id'])){
            $db_array['client_id'] = null;
        }else{
            $this->load->model('clients/mdl_clients');
            $cid = $this->mdl_clients->where('ip_clients.client_id', $db_array['client_id'])->get()->row()->client_id;
            $db_array['client_id'] = $cid;
        }
        
        $db_array['delivery_order_date_created'] = date_to_mysql($db_array['delivery_order_date_created']);
        $db_array['delivery_order_date_expires'] = $this->get_date_due($db_array['delivery_order_date_created']);

        $db_array['notes'] = get_setting('default_delivery_order_notes');

        if (!isset($db_array['delivery_order_status_id'])) {
            $db_array['delivery_order_status_id'] = 1;
        }

        $generate_delivery_order_number = get_setting('generate_delivery_order_number_for_draft');

        if ($db_array['delivery_order_status_id'] === 1 && $generate_delivery_order_number == 1) {
            $db_array['delivery_order_number'] = $this->get_delivery_order_number($db_array['invoice_group_id']);
        } elseif ($db_array['delivery_order_status_id'] != 1) {
            $db_array['delivery_order_number'] = $this->get_delivery_order_number($db_array['invoice_group_id']);
        } else {
            $db_array['delivery_order_number'] = '';
        }

        // Generate the unique url key
        $db_array['delivery_order_url_key'] = $this->get_url_key();

        return $db_array;
    }

    /**
     * @param string $delivery_order_date_created
     */
    public function get_date_due($delivery_order_date_created)
    {
        $delivery_order_date_expires = new DateTime($delivery_order_date_created);
        $delivery_order_date_expires->add(new DateInterval('P' . get_setting('delivery_orders_expire_after') . 'D'));
        return $delivery_order_date_expires->format('Y-m-d');
    }

    /**
     * @param $invoice_group_id
     * @return mixed
     */
    public function get_delivery_order_number($invoice_group_id)
    {
        $this->load->model('invoice_groups/mdl_invoice_groups');
        return $this->mdl_invoice_groups->generate_invoice_number($invoice_group_id);
    }

    /**
     * @return string
     */
    public function get_url_key()
    {
        $this->load->helper('string');
        return random_string('alnum', 15);
    }

    /**
     * @param $invoice_id
     * @return mixed
     */
    public function get_invoice_group_id($invoice_id)
    {
        $invoice = $this->get_by_id($invoice_id);
        return $invoice->invoice_group_id;
    }

    /**
     * @param int $delivery_order_id
     */
    public function delete($delivery_order_id)
    {
        parent::delete($delivery_order_id);

        $this->load->helper('orphan');
        delete_orphans();
    }

    /**
     * @return $this
     */
    public function is_draft()
    {
        $this->filter_where('delivery_order_status_id', 1);
        return $this;
    }

    /**
     * @return $this
     */
    public function is_sent()
    {
        $this->filter_where('delivery_order_status_id', 2);
        return $this;
    }

    /**
     * @return $this
     */
    public function is_viewed()
    {
        $this->filter_where('delivery_order_status_id', 3);
        return $this;
    }

    /**
     * @return $this
     */
    public function is_approved()
    {
        $this->filter_where('delivery_order_status_id', 4);
        return $this;
    }

    /**
     * @return $this
     */
    public function is_rejected()
    {
        $this->filter_where('delivery_order_status_id', 5);
        return $this;
    }

    /**
     * @return $this
     */
    public function is_canceled()
    {
        $this->filter_where('delivery_order_status_id', 6);
        return $this;
    }

    /**
     * Used by guest module; includes only sent and viewed
     *
     * @return $this
     */
    public function is_open()
    {
        $this->filter_where_in('delivery_order_status_id', array(2, 3));
        return $this;
    }

    /**
     * @return $this
     */
    public function guest_visible()
    {
        $this->filter_where_in('delivery_order_status_id', array(2, 3, 4, 5));
        return $this;
    }

    /**
     * @param $client_id
     * @return $this
     */
    public function by_client($client_id)
    {
        $this->filter_where('ip_delivery_orders.client_id', $client_id);
        return $this;
    }

    /**
     * @param $delivery_order_url_key
     */
    public function approve_delivery_order_by_key($delivery_order_url_key)
    {
        $this->db->where_in('delivery_order_status_id', array(2, 3));
        $this->db->where('delivery_order_url_key', $delivery_order_url_key);
        $this->db->set('delivery_order_status_id', 4);
        $this->db->update('ip_delivery_orders');
    }

    /**
     * @param $delivery_order_url_key
     */
    public function reject_delivery_order_by_key($delivery_order_url_key)
    {
        $this->db->where_in('delivery_order_status_id', array(2, 3));
        $this->db->where('delivery_order_url_key', $delivery_order_url_key);
        $this->db->set('delivery_order_status_id', 5);
        $this->db->update('ip_delivery_orders');
    }

    /**
     * @param $delivery_order_id
     */
    public function approve_delivery_order_by_id($delivery_order_id)
    {
        $this->db->where_in('delivery_order_status_id', array(2, 3));
        $this->db->where('delivery_order_id', $delivery_order_id);
        $this->db->set('delivery_order_status_id', 4);
        $this->db->update('ip_delivery_orders');
    }

    /**
     * @param $delivery_order_id
     */
    public function reject_delivery_order_by_id($delivery_order_id)
    {
        $this->db->where_in('delivery_order_status_id', array(2, 3));
        $this->db->where('delivery_order_id', $delivery_order_id);
        $this->db->set('delivery_order_status_id', 5);
        $this->db->update('ip_delivery_orders');
    }

    /**
     * @param $delivery_order_id
     */
    public function mark_viewed($delivery_order_id)
    {
        $this->db->select('delivery_order_status_id');
        $this->db->where('delivery_order_id', $delivery_order_id);

        $delivery_order = $this->db->get('ip_delivery_orders');

        if ($delivery_order->num_rows()) {
            if ($delivery_order->row()->delivery_order_status_id == 2) {
                $this->db->where('delivery_order_id', $delivery_order_id);
                $this->db->set('delivery_order_status_id', 3);
                $this->db->update('ip_delivery_orders');
            }
        }
    }

    /**
     * @param $delivery_order_id
     */
    public function mark_sent($delivery_order_id)
    {
        $this->db->select('delivery_order_status_id');
        $this->db->where('delivery_order_id', $delivery_order_id);

        $delivery_order = $this->db->get('ip_delivery_orders');

        if ($delivery_order->num_rows()) {
            if ($delivery_order->row()->delivery_order_status_id == 1) {
                $this->db->where('delivery_order_id', $delivery_order_id);
                $this->db->set('delivery_order_status_id', 2);
                $this->db->update('ip_delivery_orders');
            }
        }
    }

}
