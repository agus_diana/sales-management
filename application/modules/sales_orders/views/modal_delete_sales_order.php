<script>
    $(function () {
        $('#modal_delete_sales_order_confirm').click(function () {
            sales_order_id = $(this).data('sales_order-id');
            window.location = '<?php echo site_url('sales_orders/delete'); ?>/' + sales_order_id;
        });
    });
</script>

<div id="delete-sales_order" class="modal modal-lg" role="dialog" aria-labelledby="modal_delete_sales_order" aria-hidden="true">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="panel-title"><?php _trans('delete_sales_order'); ?></h4>
        </div>
        <div class="modal-body">

            <div class="alert alert-danger"><?php _trans('delete_sales_order_warning'); ?></div>

        </div>
        <div class="modal-footer">
            <div class="btn-group">
                <button id="modal_delete_sales_order_confirm" class="btn btn-danger"
                        data-sales_order-id="<?php echo $sales_order->sales_order_id; ?>">
                    <i class="fa fa-trash-o"></i> <?php _trans('yes'); ?>
                </button>
                <button class="btn btn-success" data-dismiss="modal">
                    <i class="fa fa-times"></i> <?php _trans('no'); ?>
                </button>
            </div>
        </div>
    </div>

</div>
