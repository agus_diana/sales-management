<div class="table-responsive">
    <table class="table table-striped">

        <thead>
        <tr>
            <th><?php _trans('status'); ?></th>
            <th><?php _trans('sales_order'); ?></th>
            <th><?php _trans('sales_order_reff_number'); ?></th>
            <th><?php _trans('created'); ?></th>
            <th><?php _trans('expected_delivery'); ?></th>
            <th><?php _trans('client_name'); ?></th>
            <th style="text-align: right; padding-right: 25px;"><?php _trans('amount'); ?></th>
            <!--<th style="text-align: right; padding-right: 25px;"><?php _trans('quantity'); ?></th>-->
            <!--<th style="text-align: right; padding-right: 25px;"><?php _trans('delivered_amounts'); ?></th>-->
			<!--<th style="text-align: right; padding-right: 25px;"><?php _trans('balance'); ?></th>-->
            <th><?php _trans('options'); ?></th>
        </tr>
        </thead>

        <tbody>
        <?php
        $sales_order_idx = 1;
        $sales_order_count = count($sales_orders);
        $sales_order_list_split = $sales_order_count > 3 ? $sales_order_count / 2 : 9999;

        foreach ($sales_orders as $sales_order) {
            // Convert the dropdown menu to a dropup if sales_order is after the invoice split
            $dropup = $sales_order_idx > $sales_order_list_split ? true : false;
            ?>
            <tr>
                <td>
                    <span class="label <?php echo $sales_order_statuses[$sales_order->sales_order_status_id]['class']; ?>">
                        <?php echo $sales_order_statuses[$sales_order->sales_order_status_id]['label']; ?>
                    </span>
                </td>
                <td>
                    <a href="<?php echo site_url('sales_orders/view/' . $sales_order->sales_order_id); ?>"
                       title="<?php _trans('edit'); ?>">
                        <?php echo($sales_order->sales_order_number ? $sales_order->sales_order_number : $sales_order->sales_order_id); ?>
                    </a>
                </td>
                <td>
                    <?php echo $sales_order->sales_order_reff_number; ?>
                </td>
                <td>
                    <?php echo date_from_mysql($sales_order->sales_order_date_created); ?>
                </td>
                <td>
                    <?php echo date_from_mysql($sales_order->expected_delivery); ?>
                </td>
                <td>
                    <a href="<?php echo site_url('clients/view/' . $sales_order->client_id); ?>"
                       title="<?php _trans('view_client'); ?>">
                        <?php _htmlsc(format_client($sales_order)); ?>
                    </a>
                </td>
                <td class="amount">
                    <?php echo format_currency($sales_order->sales_order_total); ?>
                </td>
                <!--
                <td class="amount">
                    <?php echo format_currency($sales_order->sales_order_delivered); ?>
                </td>
                -->
                <td>
                    <div class="options btn-group<?php echo $dropup ? ' dropup' : ''; ?>">
                        <a class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown"
                           href="#">
                            <i class="fa fa-cog"></i> <?php _trans('options'); ?>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo site_url('sales_orders/view/' . $sales_order->sales_order_id); ?>">
                                    <i class="fa fa-edit fa-margin"></i> <?php _trans('edit'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('sales_orders/generate_pdf/' . $sales_order->sales_order_id); ?>"
                                   target="_blank">
                                    <i class="fa fa-print fa-margin"></i> <?php _trans('download_pdf'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('mailer/sales_order/' . $sales_order->sales_order_id); ?>">
                                    <i class="fa fa-send fa-margin"></i> <?php _trans('send_email'); ?>
                                </a>
                            </li>
                            <!--
                            <li>
                                <a href="#" class="invoice-add-payment"
                                   data-invoice-id="<?php echo $sales_order->sales_order_id; ?>">
                                    <i class="fa fa-truck fa-margin"></i>
                                    <?php _trans('enter_delivery_order'); ?>
                                </a>
                            </li>
                            -->
							<?php if ($sales_order->sales_order_status_id == 1) { ?>
                            <li>
                                <a href="<?php echo site_url('sales_orders/delete/' . $sales_order->sales_order_id); ?>"
                                   onclick="return confirm('<?php _trans('delete_sales_order_warning'); ?>');">
                                    <i class="fa fa-trash-o fa-margin"></i> <?php _trans('delete'); ?>
                                </a>
                            </li> <?php } ?>
                        </ul>
                    </div>
                </td>
            </tr>
            <?php
            $sales_order_idx++;
        } ?>
        </tbody>

    </table>
</div>
