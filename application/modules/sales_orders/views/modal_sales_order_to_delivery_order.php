<script>
    $(function () {
        // Display the create sales_order modal
        $('#modal_sales_order_to_delivery_order').modal('show');

        // Select2 for all select inputs
        $(".simple-select").select2();

        // Creates the delivery_order
        $('#sales_order_to_delivery_order_confirm').click(function () {
            $.post("<?php echo site_url('sales_orders/ajax/sales_order_to_delivery_order'); ?>", {
                    sales_order_id: $('#sales_order_id').val(),
                    client_id: $('#client_id').val(),
                    delivery_order_date_created: $('#delivery_order_date_created').val(),
                    delivery_order_time_created: '<?php echo date('H:i:s') ?>',
                    invoice_group_id: $('#delivery_order_group_id').val(),
                    delivery_order_password: $('#delivery_order_password').val(),
                    user_id: $('#user_id').val()
                },
                function (data) {
                    <?php echo(IP_DEBUG ? 'console.log(data);' : ''); ?>
                    var response = JSON.parse(data);
                    if (response.success === 1) {
                        window.location = "<?php echo site_url('delivery_orders/view'); ?>/" + response.delivery_order_id;
                    }
                    else {
                        // The validation was not successful
                        $('.control-group').removeClass('has-error');
                        for (var key in response.validation_errors) {
                            $('#' + key).parent().parent().addClass('has-error');
                        }
                    }
                });
        });
    });
</script>

<div id="modal_sales_order_to_delivery_order" class="modal modal-lg" role="dialog" aria-labelledby="modal_sales_order_to_delivery_order"
     aria-hidden="true">
    <form class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="panel-title"><?php _trans('sales_order_to_delivery_order'); ?></h4>
        </div>
        <div class="modal-body">

            <input type="hidden" name="sales_order_id" id="sales_order_id"
                   value="<?php echo $sales_order->sales_order_id; ?>">
            <input type="hidden" name="client_id" id="client_id"
                   value="<?php echo $sales_order->client_id; ?>">
            <input type="hidden" name="user_id" id="user_id"
                   value="<?php echo $sales_order->user_id; ?>">

            <div class="form-group has-feedback">
                <label for="delivery_order_date_created">
                    <?php _trans('delivery_order_date'); ?>
                </label>

                <div class="input-group">
                    <input name="delivery_order_date_created" id="delivery_order_date_created"
                           class="form-control datepicker">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar fa-fw"></i>
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label for="delivery_order_password"><?php _trans('delivery_order_password'); ?></label>
                <input type="text" name="delivery_order_password" id="delivery_order_password" class="form-control"
                       value="<?php echo get_setting('delivery_order_pre_password') == '' ? '' : get_setting('delivery_order_pre_password') ?>"
                       autocomplete="off">
            </div>

            <div class="form-group">
                <label for="delivery_order_group_id">
                    <?php _trans('delivery_order_group'); ?>
                </label>
                <select name="delivery_order_group_id" id="delivery_order_group_id" class="form-control simple-select">
                    <?php foreach ($delivery_order_groups as $delivery_order_group) { ?>
                        <option value="<?php echo $delivery_order_group->invoice_group_id; ?>"
                            <?php check_select(get_setting('default_delivery_order_group'), $delivery_order_group->invoice_group_id); ?>>
                            <?php _htmlsc($delivery_order_group->invoice_group_name); ?></option>
                    <?php } ?>
                </select>
            </div>

        </div>

        <div class="modal-footer">
            <div class="btn-group">
                <button class="btn btn-success" id="sales_order_to_delivery_order_confirm" type="button">
                    <i class="fa fa-check"></i> <?php _trans('submit'); ?>
                </button>
                <button class="btn btn-danger" type="button" data-dismiss="modal">
                    <i class="fa fa-times"></i> <?php _trans('cancel'); ?>
                </button>
            </div>
        </div>

    </form>

</div>
