<script>
    $(function () {
        // Display the create sales_order modal
        $('#create-sales_order').modal('show');

        $('.simple-select').select2();

        <?php $this->layout->load_view('clients/script_select2_client_id.js'); ?>

        // Toggle on/off permissive search on clients names
        $('span#toggle_permissive_search_clients').click(function () {
            if ($('input#input_permissive_search_clients').val() == ('1')) {
                $.get("<?php echo site_url('clients/ajax/save_preference_permissive_search_clients'); ?>", {
                    permissive_search_clients: '0'
                });
                $('input#input_permissive_search_clients').val('0');
                $('span#toggle_permissive_search_clients i').removeClass('fa-toggle-on');
                $('span#toggle_permissive_search_clients i').addClass('fa-toggle-off');
            } else {
                $.get("<?php echo site_url('clients/ajax/save_preference_permissive_search_clients'); ?>", {
                    permissive_search_clients: '1'
                });
                $('input#input_permissive_search_clients').val('1');
                $('span#toggle_permissive_search_clients i').removeClass('fa-toggle-off');
                $('span#toggle_permissive_search_clients i').addClass('fa-toggle-on');
            }
        });

        // Creates the sales_order
        $('#sales_order_create_confirm').click(function () {
            console.log('clicked');
            // Posts the data to validate and create the sales_order;
            // will create the new client if necessary
            $.post("<?php echo site_url('sales_orders/ajax/create'); ?>", {
                    client_id: $('#create_sales_order_client_id').val(),
                    sales_order_date_created: $('#sales_order_date_created').val(),
                    sales_order_password: $('#sales_order_password').val(),
                    sales_order_reff_number: $('#sales_order_reff_number').val(),
                    expected_delivery: $('#expected_delivery').val(),
                    user_id: '<?php echo $this->session->userdata('user_id'); ?>',
                    invoice_group_id: $('#invoice_group_id').val()
                },
                function (data) {
                    <?php echo(IP_DEBUG ? 'console.log(data);' : ''); ?>
                    var response = JSON.parse(data);
                    if (response.success === 1) {
                        // The validation was successful and sales_order was created
                        window.location = "<?php echo site_url('sales_orders/view'); ?>/" + response.sales_order_id;
                    }
                    else {
                        // The validation was not successful
                        $('.control-group').removeClass('has-error');
                        for (var key in response.validation_errors) {
                            $('#' + key).parent().parent().addClass('has-error');
                        }
                    }
                });
        });
    });
</script>

<div id="create-sales_order" class="modal modal-lg" role="dialog" aria-labelledby="modal_create_sales_order" aria-hidden="true">
    <form class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            <h4 class="panel-title"><?php _trans('create_sales_order'); ?></h4>
        </div>
        <div class="modal-body">

            <input class="hidden" id="input_permissive_search_clients"
                   value="<?php echo get_setting('enable_permissive_search_clients'); ?>">

            <div class="form-group has-feedback">
                <label for="create_sales_order_client_id"><?php _trans('client'); ?></label>
                <div class="input-group">
                    <select name="client_id" id="create_sales_order_client_id" class="client-id-select form-control"
                            autofocus="autofocus">
                        <?php if (!empty($client)) : ?>
                            <option value="<?php echo $client->client_id; ?>"><?php _htmlsc(format_client($client)); ?></option>
                        <?php endif; ?>
                    </select>
                    <span id="toggle_permissive_search_clients" class="input-group-addon" title="<?php _trans('enable_permissive_search_clients'); ?>" style="cursor:pointer;">
                        <i class="fa fa-toggle-<?php echo get_setting('enable_permissive_search_clients') ? 'on' : 'off' ?> fa-fw" ></i>
                    </span>
                </div>
            </div>

            <div class="form-group has-feedback">
                <label for="sales_order_date_created">
                    <?php _trans('sales_order_date'); ?>
                </label>

                <div class="input-group">
                    <input name="sales_order_date_created" id="sales_order_date_created"
                           class="form-control datepicker"
                           value="<?php echo date(date_format_setting()); ?>">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar fa-fw"></i>
                    </span>
                </div>
            </div>

            <div class="form-group has-feedback">
                <label for="expected_delivery">
                    <?php _trans('expected_delivery'); ?>
                </label>

                <div class="input-group">
                    <input name="expected_delivery" id="expected_delivery"
                           class="form-control datepicker"
                           value="<?php echo date(date_format_setting()); ?>">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar fa-fw"></i>
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label for="sales_order_reff_number"><?php _trans('sales_order_reff_number'); ?></label>
                <input type="text" name="sales_order_reff_number" id="sales_order_reff_number" class="form-control">
            </div>
            <!--
            <div class="form-group">
                <label for="sales_order_password"><?php _trans('sales_order_password'); ?></label>
                <input type="text" name="sales_order_password" id="sales_order_password" class="form-control"
                       value="<?php echo get_setting('sales_order_pre_password') ? '' : get_setting('sales_order_pre_password') ?>"
                       autocomplete="off">
            </div>
            -->

            <div class="form-group">
                <label for="invoice_group_id"><?php _trans('invoice_group'); ?>: </label>
                <select name="invoice_group_id" id="invoice_group_id" class="form-control simple-select">
                    <?php foreach ($invoice_groups as $invoice_group) { ?>
                        <option value="<?php echo $invoice_group->invoice_group_id; ?>"
                            <?php check_select(get_setting('default_sales_order_group'), $invoice_group->invoice_group_id); ?>>
                            <?php _htmlsc($invoice_group->invoice_group_name); ?>
                        </option>
                    <?php } ?>
                </select>
            </div>

        </div>

        <div class="modal-footer">
            <div class="btn-group">
                <button class="btn btn-success ajax-loader" id="sales_order_create_confirm" type="button">
                    <i class="fa fa-check"></i> <?php _trans('submit'); ?>
                </button>
                <button class="btn btn-danger" type="button" data-dismiss="modal">
                    <i class="fa fa-times"></i> <?php _trans('cancel'); ?>
                </button>
            </div>
        </div>

    </form>

</div>
