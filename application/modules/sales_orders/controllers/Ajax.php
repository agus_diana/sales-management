<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * InvoicePlane
 *
 * @author		InvoicePlane Developers & Contributors
 * @copyright	Copyright (c) 2012 - 2017 InvoicePlane.com
 * @license		https://invoiceplane.com/license.txt
 * @link		https://invoiceplane.com
 */

/**
 * Class Ajax
 */
class Ajax extends Admin_Controller
{
    public $ajax_controller = true;

    public function save()
    {
        $this->load->model('sales_orders/mdl_sales_order_items');
        $this->load->model('sales_orders/mdl_sales_orders');
        $this->load->model('units/mdl_units');

        $sales_order_id = $this->input->post('sales_order_id');

        $this->mdl_sales_orders->set_id($sales_order_id);

        if ($this->mdl_sales_orders->run_validation('validation_rules_save_sales_order')) {
            $items = json_decode($this->input->post('items'));

            foreach ($items as $item) {
                if ($item->item_name) {
                    $item->item_quantity = ($item->item_quantity ? standardize_amount($item->item_quantity) : floatval(0));
                    $item->item_price = ($item->item_quantity ? standardize_amount($item->item_price) : floatval(0));
                    $item->item_discount_amount = ($item->item_discount_amount) ? standardize_amount($item->item_discount_amount) : null;
                    $item->item_product_id = ($item->item_product_id ? $item->item_product_id : null);
                    $item->item_product_unit_id = ($item->item_product_unit_id ? $item->item_product_unit_id : null);
                    $item->item_product_unit = $this->mdl_units->get_name($item->item_product_unit_id, $item->item_quantity);
                    $item_id = ($item->item_id) ?: null;
                    unset($item->item_id);

                    $this->mdl_sales_order_items->save($item_id, $item);
                }
            }

            if ($this->input->post('sales_order_discount_amount') === '') {
                $sales_order_discount_amount = floatval(0);
            } else {
                $sales_order_discount_amount = $this->input->post('sales_order_discount_amount');
            }

            if ($this->input->post('sales_order_shipping_charges') === '') {
                $sales_order_shipping_charges = floatval(0);
            } else {
                $sales_order_shipping_charges = $this->input->post('sales_order_shipping_charges');
            }

            if ($this->input->post('sales_order_discount_percent') === '') {
                $sales_order_discount_percent = floatval(0);
            } else {
                $sales_order_discount_percent = $this->input->post('sales_order_discount_percent');
            }

            // Generate new sales_order number if needed
            $sales_order_number = $this->input->post('sales_order_number');
            $sales_order_status_id = $this->input->post('sales_order_status_id');

            if (empty($sales_order_number) && $sales_order_status_id != 1) {
                $sales_order_group_id = $this->mdl_sales_orders->get_invoice_group_id($sales_order_id);
                $sales_order_number = $this->mdl_sales_orders->get_sales_order_number($sales_order_group_id);
            }

            $db_array = array(
                'sales_order_number' => $sales_order_number,
                'sales_order_date_created' => date_to_mysql($this->input->post('sales_order_date_created')),
                'expected_delivery' => date_to_mysql($this->input->post('expected_delivery')),
                //'sales_order_date_expires' => date_to_mysql($this->input->post('sales_order_date_expires')),
                'sales_order_status_id' => $sales_order_status_id,
                'sales_order_password' => $this->input->post('sales_order_password'),
                'sales_order_reff_number' => $this->input->post('sales_order_reff_number'),
                'notes' => $this->input->post('notes'),
                'sales_order_discount_amount' => standardize_amount($sales_order_discount_amount),
                'sales_order_discount_percent' => standardize_amount($sales_order_discount_percent),
                'sales_order_shipping_charges' => standardize_amount($sales_order_shipping_charges),
            );

            $this->mdl_sales_orders->save($sales_order_id, $db_array);

            // Recalculate for discounts
            $this->load->model('sales_orders/mdl_sales_order_amounts');
            $this->mdl_sales_order_amounts->calculate($sales_order_id);

            $response = array(
                'success' => 1
            );
        } else {
            $this->load->helper('json_error');
            $response = array(
                'success' => 0,
                'validation_errors' => json_errors()
            );
        }


        // Save all custom fields
        if ($this->input->post('custom')) {
            $db_array = array();

            $values = [];
            foreach ($this->input->post('custom') as $custom) {
                if (preg_match("/^(.*)\[\]$/i", $custom['name'], $matches)) {
                    $values[$matches[1]][] = $custom['value'];
                } else {
                    $values[$custom['name']] = $custom['value'];
                }
            }

            foreach ($values as $key => $value) {
                preg_match("/^custom\[(.*?)\](?:\[\]|)$/", $key, $matches);
                if ($matches) {
                    $db_array[$matches[1]] = $value;
                }
            }
            $this->load->model('custom_fields/mdl_sales_order_custom');
            $result = $this->mdl_sales_order_custom->save_custom($sales_order_id, $db_array);
            if ($result !== true) {
                $response = array(
                    'success' => 0,
                    'validation_errors' => $result
                );

                echo json_encode($response);
                exit;
            }
        }

        echo json_encode($response);
    }

    public function save_sales_order_tax_rate()
    {
        $this->load->model('sales_orders/mdl_sales_order_tax_rates');

        if ($this->mdl_sales_order_tax_rates->run_validation()) {
            $this->mdl_sales_order_tax_rates->save();

            $response = array(
                'success' => 1
            );
        } else {
            $response = array(
                'success' => 0,
                'validation_errors' => $this->mdl_sales_order_tax_rates->validation_errors
            );
        }

        echo json_encode($response);
    }

    public function create()
    {
        $this->load->model('sales_orders/mdl_sales_orders');
 
        if ($this->mdl_sales_orders->run_validation()) {
            $sales_order_id = $this->mdl_sales_orders->create();

            $response = array(
                'success' => 1,
                'sales_order_id' => $sales_order_id
            );
        } else {
            $this->load->helper('json_error');
            $response = array(
                'success' => 0,
                'validation_errors' => json_errors()
            );
        }

        echo json_encode($response);
    }

    public function modal_change_client()
    {
        $this->load->module('layout');
        $this->load->model('clients/mdl_clients');

        $data = array(
            'client_id' => $this->input->post('client_id'),
            'sales_order_id' => $this->input->post('sales_order_id'),
            'clients' => $this->mdl_clients->get_latest(),
        );

        $this->layout->load_view('sales_orders/modal_change_client', $data);
    }

    public function change_client()
    {
        $this->load->model('sales_orders/mdl_sales_orders');
        $this->load->model('clients/mdl_clients');

        // Get the client ID
        $client_id = $this->input->post('client_id');
        $client = $this->mdl_clients->where('ip_clients.client_id', $client_id)
            ->get()->row();

        if (!empty($client)) {
            $sales_order_id = $this->input->post('sales_order_id');

            $db_array = array(
                'client_id' => $client_id,
            );
            $this->db->where('sales_order_id', $sales_order_id);
            $this->db->update('ip_sales_orders', $db_array);

            $response = array(
                'success' => 1,
                'sales_order_id' => $sales_order_id
            );
        } else {
            $this->load->helper('json_error');
            $response = array(
                'success' => 0,
                'validation_errors' => json_errors()
            );
        }

        echo json_encode($response);
    }

    public function get_item()
    {
        $this->load->model('sales_orders/mdl_sales_order_items');

        $item = $this->mdl_sales_order_items->get_by_id($this->input->post('item_id'));

        echo json_encode($item);
    }

    public function modal_create_sales_order()
    {
        $this->load->module('layout');
        $this->load->model('invoice_groups/mdl_invoice_groups');
        $this->load->model('tax_rates/mdl_tax_rates');
        $this->load->model('clients/mdl_clients');

        $data = array(
            'invoice_groups' => $this->mdl_invoice_groups->get()->result(),
            'tax_rates' => $this->mdl_tax_rates->get()->result(),
            'client' => $this->mdl_clients->get_by_id($this->input->post('client_id')),
            'clients' => $this->mdl_clients->get_latest(),
        );

        $this->layout->load_view('sales_orders/modal_create_sales_order', $data);
    }

    public function modal_copy_sales_order()
    {
        $this->load->module('layout');

        $this->load->model('sales_orders/mdl_sales_orders');
        $this->load->model('invoice_groups/mdl_invoice_groups');
        $this->load->model('tax_rates/mdl_tax_rates');
        $this->load->model('clients/mdl_clients');

        $data = array(
            'invoice_groups' => $this->mdl_invoice_groups->get()->result(),
            'tax_rates' => $this->mdl_tax_rates->get()->result(),
            'sales_order_id' => $this->input->post('sales_order_id'),
            'sales_order' => $this->mdl_sales_orders->where('ip_sales_orders.sales_order_id', $this->input->post('sales_order_id'))->get()->row(),
            'client' => $this->mdl_clients->get_by_id($this->input->post('client_id')),
        );

        $this->layout->load_view('sales_orders/modal_copy_sales_order', $data);
    }

    public function copy_sales_order()
    {
        $this->load->model('sales_orders/mdl_sales_orders');
        $this->load->model('sales_orders/mdl_sales_order_items');
        $this->load->model('sales_orders/mdl_sales_order_tax_rates');

        if ($this->mdl_sales_orders->run_validation()) {
            $target_id = $this->mdl_sales_orders->save();
            $source_id = $this->input->post('sales_order_id');

            $this->mdl_sales_orders->copy_sales_order($source_id, $target_id);

            $response = array(
                'success' => 1,
                'sales_order_id' => $target_id
            );
        } else {
            $this->load->helper('json_error');
            $response = array(
                'success' => 0,
                'validation_errors' => json_errors()
            );
        }

        echo json_encode($response);
    }
	
	public function modal_sales_order_to_delivery_order($sales_order_id)
	{
        $this->load->model('invoice_groups/mdl_invoice_groups');
        $this->load->model('sales_orders/mdl_sales_orders');

        $data = array(
            'delivery_order_groups' => $this->mdl_invoice_groups->get()->result(),
            'sales_order_id' => $sales_order_id,
            'sales_order' => $this->mdl_sales_orders->where('ip_sales_orders.sales_order_id', $sales_order_id)->get()->row()
        );

        $this->load->view('sales_orders/modal_sales_order_to_delivery_order', $data);
    }

    public function modal_sales_order_to_invoice($sales_order_id)
    {
        $this->load->model('invoice_groups/mdl_invoice_groups');
        $this->load->model('sales_orders/mdl_sales_orders');

        $data = array(
            'invoice_groups' => $this->mdl_invoice_groups->get()->result(),
            'sales_order_id' => $sales_order_id,
            'sales_order' => $this->mdl_sales_orders->where('ip_sales_orders.sales_order_id', $sales_order_id)->get()->row()
        );

        $this->load->view('sales_orders/modal_sales_order_to_invoice', $data);
    }
	
	public function sales_order_to_delivery_order()
	{
        $this->load->model(
            array(
                'delivery_orders/mdl_delivery_orders',
                'delivery_orders/mdl_delivery_order_items',
                'sales_orders/mdl_sales_orders',
                'sales_orders/mdl_sales_order_items',
                'delivery_orders/mdl_delivery_order_tax_rates',
                'sales_orders/mdl_sales_order_tax_rates'
            )
        );
  
        if ($this->mdl_delivery_orders->run_validation()) {
            // Get the sales_order
            $sales_order = $this->mdl_sales_orders->get_by_id($this->input->post('sales_order_id'));

            $delivery_order_id = $this->mdl_delivery_orders->create(null, false);

            // Update the discounts
            $this->db->where('delivery_order_id', $delivery_order_id);
            $this->db->set('client_id', $sales_order->client_id);
            $this->db->set('sales_order_id', $sales_order->sales_order_id);
            $this->db->set('delivery_order_discount_amount', $sales_order->sales_order_discount_amount);
            $this->db->set('delivery_order_discount_percent', $sales_order->sales_order_discount_percent);
            $this->db->set('delivery_order_shipping_charges', $sales_order->sales_order_shipping_charges);
            $this->db->update('ip_delivery_orders');

            // Save the delivery_order id to the sales_order
            //$this->db->where('sales_order_id', $this->input->post('sales_order_id'));
            //$this->db->set('delivery_order_id', $delivery_order_id);
            //$this->db->update('ip_sales_orders');

            //ambil custom fields
            $custom_fields = $this->db->join('ip_custom_fields','ip_sales_order_custom.sales_order_custom_fieldid = ip_custom_fields.custom_field_id')->where('sales_order_id', $this->input->post('sales_order_id'))->get('ip_sales_order_custom')->result();
            foreach ($custom_fields as $custom_field) {
                $db_array = array(
                    'delivery_order_id' => $delivery_order_id,
                    'delivery_order_custom_fieldid' => $custom_field->delivery_order_code,
                    'delivery_order_custom_fieldvalue' => $custom_field->sales_order_custom_fieldvalue
                );
                $this->db->insert('ip_delivery_order_custom',$db_array);
            }

            $sales_order_items = $this->mdl_sales_order_items->where('item_quantity_balance >',0)->where('sales_order_id', $this->input->post('sales_order_id'))->get()->result();

            foreach ($sales_order_items as $sales_order_item) {
                $db_array = array(
                    'delivery_order_id' => $delivery_order_id,
                    'item_tax_rate_id' => $sales_order_item->item_tax_rate_id,
                    'item_product_id' => $sales_order_item->item_product_id,
                    'item_name' => $sales_order_item->item_name,
                    'item_description' => $sales_order_item->item_description,
                    'item_quantity' => $sales_order_item->item_quantity_balance,
                    'item_price' => $sales_order_item->item_price,
                    'item_product_unit_id' => $sales_order_item->item_product_unit_id,
                    'item_product_unit' => $sales_order_item->item_product_unit,
                    'item_discount_amount' => $sales_order_item->item_discount_amount,
                    'item_order' => $sales_order_item->item_order,
                    'sales_order_id' => $this->input->post('sales_order_id')
                );
				
                $this->mdl_delivery_order_items->save(null, $db_array);
            }

            $sales_order_tax_rates = $this->mdl_sales_order_tax_rates->where('sales_order_id', $this->input->post('sales_order_id'))->get()->result();

            foreach ($sales_order_tax_rates as $sales_order_tax_rate) {
                $db_array = array(
                    'delivery_order_id' => $delivery_order_id,
                    'tax_rate_id' => $sales_order_tax_rate->tax_rate_id,
                    'include_item_tax' => $sales_order_tax_rate->include_item_tax,
                    'delivery_order_tax_rate_amount' => $sales_order_tax_rate->sales_order_tax_rate_amount
                );

                $this->mdl_delivery_order_tax_rates->save(null, $db_array);
            }

            $response = array(
                'success' => 1,
                'delivery_order_id' => $delivery_order_id
            );
        } else {
            $this->load->helper('json_error');
            $response = array(
                'success' => 0,
                'validation_errors' => json_errors()
            );
        }

        echo json_encode($response);
    }

    public function sales_order_to_invoice()
    {
        $this->load->model(
            array(
                'invoices/mdl_invoices',
                'invoices/mdl_items',
                'sales_orders/mdl_sales_orders',
                'sales_orders/mdl_sales_order_items',
                'invoices/mdl_invoice_tax_rates',
                'sales_orders/mdl_sales_order_tax_rates'
            )
        );

        if ($this->mdl_invoices->run_validation()) {
            // Get the sales_order
            $sales_order = $this->mdl_sales_orders->get_by_id($this->input->post('sales_order_id'));

            $invoice_id = $this->mdl_invoices->create(null, false);

            // Update the discounts
            $this->db->where('invoice_id', $invoice_id);
            $this->db->set('invoice_discount_amount', $sales_order->sales_order_discount_amount);
            $this->db->set('invoice_discount_percent', $sales_order->sales_order_discount_percent);
            $this->db->update('ip_invoices');

            // Save the invoice id to the sales_order
            $this->db->where('sales_order_id', $this->input->post('sales_order_id'));
            $this->db->set('invoice_id', $invoice_id);
            $this->db->update('ip_sales_orders');

            $sales_order_items = $this->mdl_sales_order_items->where('sales_order_id', $this->input->post('sales_order_id'))->get()->result();

            foreach ($sales_order_items as $sales_order_item) {
                $db_array = array(
                    'invoice_id' => $invoice_id,
                    'item_tax_rate_id' => $sales_order_item->item_tax_rate_id,
                    'item_product_id' => $sales_order_item->item_product_id,
                    'item_name' => $sales_order_item->item_name,
                    'item_description' => $sales_order_item->item_description,
                    'item_quantity' => $sales_order_item->item_quantity,
                    'item_price' => $sales_order_item->item_price,
                    'item_product_unit_id' => $sales_order_item->item_product_unit_id,
                    'item_product_unit' => $sales_order_item->item_product_unit,
                    'item_discount_amount' => $sales_order_item->item_discount_amount,
                    'item_order' => $sales_order_item->item_order
                );

                $this->mdl_items->save(null, $db_array);
            }

            $sales_order_tax_rates = $this->mdl_sales_order_tax_rates->where('sales_order_id', $this->input->post('sales_order_id'))->get()->result();

            foreach ($sales_order_tax_rates as $sales_order_tax_rate) {
                $db_array = array(
                    'invoice_id' => $invoice_id,
                    'tax_rate_id' => $sales_order_tax_rate->tax_rate_id,
                    'include_item_tax' => $sales_order_tax_rate->include_item_tax,
                    'invoice_tax_rate_amount' => $sales_order_tax_rate->sales_order_tax_rate_amount
                );

                $this->mdl_invoice_tax_rates->save(null, $db_array);
            }

            $response = array(
                'success' => 1,
                'invoice_id' => $invoice_id
            );
        } else {
            $this->load->helper('json_error');
            $response = array(
                'success' => 0,
                'validation_errors' => json_errors()
            );
        }

        echo json_encode($response);
    }

}
