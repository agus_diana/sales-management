<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * InvoicePlane
 *
 * @author		InvoicePlane Developers & Contributors
 * @copyright	Copyright (c) 2012 - 2017 InvoicePlane.com
 * @license		https://invoiceplane.com/license.txt
 * @link		https://invoiceplane.com
 */

/**
 * Class sales_orders
 */
class Sales_orders extends Admin_Controller
{
    /**
     * sales_orders constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('mdl_sales_orders');
    }

    public function index()
    {
        // Display all sales_orders by default
        redirect('sales_orders/status/all');
    }

    /**
     * @param string $status
     * @param int $page
     */
    public function status($status = 'all', $page = 0)
    {
        // Determine which group of sales_orders to load
        switch ($status) {
            case 'draft':
                $this->mdl_sales_orders->is_draft();
                break;
            case 'sent':
                $this->mdl_sales_orders->is_sent();
                break;
            case 'viewed':
                $this->mdl_sales_orders->is_viewed();
                break;
            case 'approved':
                $this->mdl_sales_orders->is_approved();
                break;
            case 'rejected':
                $this->mdl_sales_orders->is_rejected();
                break;
            case 'canceled':
                $this->mdl_sales_orders->is_canceled();
                break;
        }

        $this->mdl_sales_orders->paginate(site_url('sales_orders/status/' . $status), $page);
        $sales_orders = $this->mdl_sales_orders->result();

        $this->layout->set(
            array(
                'sales_orders' => $sales_orders,
                'status' => $status,
                'filter_display' => true,
                'filter_placeholder' => trans('filter_sales_orders'),
                'filter_method' => 'filter_sales_orders',
                'sales_order_statuses' => $this->mdl_sales_orders->statuses()
            )
        );

        $this->layout->buffer('content', 'sales_orders/index');
        $this->layout->render();
    }

    /**
     * @param $sales_order_id
     */
    public function view($sales_order_id)
    {
        $this->load->helper('custom_values');
        $this->load->model('mdl_sales_order_items');
        $this->load->model('tax_rates/mdl_tax_rates');
        $this->load->model('units/mdl_units');
        $this->load->model('mdl_sales_order_tax_rates');
        $this->load->model('custom_fields/mdl_custom_fields');
        $this->load->model('custom_values/mdl_custom_values');
        $this->load->model('custom_fields/mdl_sales_order_custom');
        $this->load->model('delivery_orders/mdl_delivery_orders');

        $fields = $this->mdl_sales_order_custom->by_id($sales_order_id)->get()->result();
        $this->db->reset_query();

        $sales_order_custom = $this->mdl_sales_order_custom->where('sales_order_id', $sales_order_id)->get();

        if ($sales_order_custom->num_rows()) {
            $sales_order_custom = $sales_order_custom->row();

            unset($sales_order_custom->sales_order_id, $sales_order_custom->sales_order_custom_id);

            foreach ($sales_order_custom as $key => $val) {
                $this->mdl_sales_orders->set_form_value('custom[' . $key . ']', $val);
            }
        }

        $sales_order = $this->mdl_sales_orders->get_by_id($sales_order_id);


        if (!$sales_order) {
            show_404();
        }

        $custom_fields = $this->mdl_custom_fields->by_table('ip_sales_order_custom')->get()->result();
        $custom_values = [];
        foreach ($custom_fields as $custom_field) {
            if (in_array($custom_field->custom_field_type, $this->mdl_custom_values->custom_value_fields())) {
                $values = $this->mdl_custom_values->get_by_fid($custom_field->custom_field_id)->result();
                $custom_values[$custom_field->custom_field_id] = $values;
            }
        }

        foreach ($custom_fields as $cfield) {
            foreach ($fields as $fvalue) {
                if ($fvalue->sales_order_custom_fieldid == $cfield->custom_field_id) {
                    // TODO: Hackish, may need a better optimization
                    $this->mdl_sales_orders->set_form_value(
                        'custom[' . $cfield->custom_field_id . ']',
                        $fvalue->sales_order_custom_fieldvalue
                    );
                    break;
                }
            }
        } 

        $this->layout->set(
            array(
                'sales_order' => $sales_order,
                'items' => $this->mdl_sales_order_items->where('sales_order_id', $sales_order_id)->get()->result(),
                'delivery_order' => $this->mdl_delivery_orders->where('sales_order_id', $sales_order_id)->get()->result(),
                'sales_order_id' => $sales_order_id,
                'tax_rates' => $this->mdl_tax_rates->get()->result(),
                'units' => $this->mdl_units->get()->result(),
                'sales_order_tax_rates' => $this->mdl_sales_order_tax_rates->where('sales_order_id', $sales_order_id)->get()->result(),
                'custom_fields' => $custom_fields,
                'custom_values' => $custom_values,
                'custom_js_vars' => array(
                    'currency_symbol' => get_setting('currency_symbol'),
                    'currency_symbol_placement' => get_setting('currency_symbol_placement'),
                    'decimal_point' => get_setting('decimal_point')
                ),
                'sales_order_statuses' => $this->mdl_sales_orders->statuses()
            )
        );

        $this->layout->buffer(
            array(
                array('modal_delete_sales_order', 'sales_orders/modal_delete_sales_order'),
                array('modal_add_sales_order_tax', 'sales_orders/modal_add_sales_order_tax'),
                array('content', 'sales_orders/view')
            )
        );

        $this->layout->render();
    }

    /**
     * @param $sales_order_id
     */
    public function delete($sales_order_id)
    {
        // Delete the sales_order
        $this->mdl_sales_orders->delete($sales_order_id);

        // Redirect to sales_order index
        redirect('sales_orders/index');
    }

    /**
     * @param $sales_order_id
     * @param $item_id
     */
    public function delete_item($sales_order_id, $item_id)
    {
        // Delete sales_order item 
        $this->load->model('mdl_sales_order_items');
        $this->mdl_sales_order_items->delete($item_id);

        // Redirect to sales_order view
        redirect('sales_orders/view/' . $sales_order_id);
    }

    /**
     * @param $sales_order_id
     * @param bool $stream
     * @param null $sales_order_template
     */
    public function generate_pdf($sales_order_id, $stream = true, $sales_order_template = null)
    {
        $this->load->helper('pdf');

        if (get_setting('mark_sales_orders_sent_pdf') == 1) {
            $this->mdl_sales_orders->mark_sent($sales_order_id);
        }

        generate_sales_order_pdf($sales_order_id, $stream, $sales_order_template);
    }

    /**
     * @param $sales_order_id
     * @param $sales_order_tax_rate_id
     */
    public function delete_sales_order_tax($sales_order_id, $sales_order_tax_rate_id)
    {
        $this->load->model('mdl_sales_order_tax_rates');
        $this->mdl_sales_order_tax_rates->delete($sales_order_tax_rate_id);

        $this->load->model('mdl_sales_order_amounts');
        $this->mdl_sales_order_amounts->calculate($sales_order_id);

        redirect('sales_orders/view/' . $sales_order_id);
    }

    public function recalculate_all_sales_orders()
    {
        $this->db->select('sales_order_id');
        $sales_order_ids = $this->db->get('ip_sales_orders')->result();

        $this->load->model('mdl_sales_order_amounts');

        foreach ($sales_order_ids as $sales_order_id) {
            $this->mdl_sales_order_amounts->calculate($sales_order_id->sales_order_id);
        }
    }

}
