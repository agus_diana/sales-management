<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * InvoicePlane
 *
 * @author		InvoicePlane Developers & Contributors
 * @copyright	Copyright (c) 2012 - 2017 InvoicePlane.com
 * @license		https://invoiceplane.com/license.txt
 * @link		https://invoiceplane.com
 */

/**
 * Class Mdl_sales_order_Amounts
 */
class Mdl_sales_order_Amounts extends CI_Model
{
    /**
     * IP_sales_order_AMOUNTS
     * sales_order_amount_id
     * sales_order_id
     * sales_order_item_subtotal      SUM(item_subtotal)
     * sales_order_item_tax_total     SUM(item_tax_total)
     * sales_order_tax_total
     * sales_order_total              sales_order_item_subtotal + sales_order_item_tax_total + sales_order_tax_total
     *
     * IP_sales_order_ITEM_AMOUNTS
     * item_amount_id
     * item_id
     * item_tax_rate_id
     * item_subtotal             item_quantity * item_price
     * item_tax_total            item_subtotal * tax_rate_percent
     * item_total                item_subtotal + item_tax_total
     *
     * @param $sales_order_id
     */
    public function calculate($sales_order_id)
    {
        // Get the basic totals
        $query = $this->db->query("
            SELECT SUM(item_subtotal) AS sales_order_item_subtotal,
		        SUM(item_tax_total) AS sales_order_item_tax_total,
		        SUM(item_subtotal) + SUM(item_tax_total) AS sales_order_total,
		        SUM(item_discount) AS sales_order_item_discount
		    FROM ip_sales_order_item_amounts
		    WHERE item_id
		        IN (SELECT item_id FROM ip_sales_order_items WHERE sales_order_id = " . $this->db->escape($sales_order_id) . ")
            ");

        $sales_order_amounts = $query->row();

        $sales_order_item_subtotal = $sales_order_amounts->sales_order_item_subtotal - $sales_order_amounts->sales_order_item_discount;
        $sales_order_subtotal = $sales_order_item_subtotal + $sales_order_amounts->sales_order_item_tax_total;
        $sales_order_total = $this->calculate_discount($sales_order_id, $sales_order_subtotal);

        // Create the database array and insert or update
        $db_array = array(
            'sales_order_id' => $sales_order_id,
            'sales_order_item_subtotal' => $sales_order_item_subtotal,
            'sales_order_item_tax_total' => $sales_order_amounts->sales_order_item_tax_total,
            'sales_order_total' => $sales_order_total,
        );

        $this->db->where('sales_order_id', $sales_order_id);
        if ($this->db->get('ip_sales_order_amounts')->num_rows()) {
            // The record already exists; update it
            $this->db->where('sales_order_id', $sales_order_id);
            $this->db->update('ip_sales_order_amounts', $db_array);
        } else {
            // The record does not yet exist; insert it
            $this->db->insert('ip_sales_order_amounts', $db_array);
        }

        // Calculate the sales_order taxes
        $this->calculate_sales_order_taxes($sales_order_id);
    }

    /**
     * @param $sales_order_id
     * @param $sales_order_total
     * @return float
     */
    public function calculate_discount($sales_order_id, $sales_order_total)
    {
        $this->db->where('sales_order_id', $sales_order_id);
        $sales_order_data = $this->db->get('ip_sales_orders')->row();

        $total = (float)number_format($sales_order_total, 2, '.', '');
        $discount_amount = (float)number_format($sales_order_data->sales_order_discount_amount, 2, '.', '');
        $discount_percent = (float)number_format($sales_order_data->sales_order_discount_percent, 2, '.', '');

        $shipping_charges = (float)number_format($sales_order_data->sales_order_shipping_charges, 2, '.', '');
        $total = $total + $shipping_charges;

        $total = $total - $discount_amount;
        $total = $total - round(($total / 100 * $discount_percent), 2);

        return $total;
    }

    /**
     * @param $sales_order_id
     */
    public function calculate_sales_order_taxes($sales_order_id)
    {
        // First check to see if there are any sales_order taxes applied
        $this->load->model('sales_orders/mdl_sales_order_tax_rates');
        $sales_order_tax_rates = $this->mdl_sales_order_tax_rates->where('sales_order_id', $sales_order_id)->get()->result();

        if ($sales_order_tax_rates) {
            // There are sales_order taxes applied
            // Get the current sales_order amount record
            $sales_order_amount = $this->db->where('sales_order_id', $sales_order_id)->get('ip_sales_order_amounts')->row();

            // Loop through the sales_order taxes and update the amount for each of the applied sales_order taxes
            foreach ($sales_order_tax_rates as $sales_order_tax_rate) {
                if ($sales_order_tax_rate->include_item_tax) {
                    // The sales_order tax rate should include the applied item tax
                    //$sales_order_tax_rate_amount = ($sales_order_amount->sales_order_item_subtotal + $sales_order_amount->sales_order_item_tax_total) * ($sales_order_tax_rate->sales_order_tax_rate_percent / 100);
                    $sales_order_tax_rate_amount = ($sales_order_amount->sales_order_total + $sales_order_amount->sales_order_item_tax_total) * ($sales_order_tax_rate->sales_order_tax_rate_percent / 100);
                } else {
                    // The sales_order tax rate should not include the applied item tax
                    //$sales_order_tax_rate_amount = $sales_order_amount->sales_order_item_subtotal * ($sales_order_tax_rate->sales_order_tax_rate_percent / 100);
                    $sales_order_tax_rate_amount = $sales_order_amount->sales_order_total * ($sales_order_tax_rate->sales_order_tax_rate_percent / 100);
                }

                // Update the sales_order tax rate record
                $db_array = array(
                    'sales_order_tax_rate_amount' => $sales_order_tax_rate_amount
                );
                $this->db->where('sales_order_tax_rate_id', $sales_order_tax_rate->sales_order_tax_rate_id);
                $this->db->update('ip_sales_order_tax_rates', $db_array);
            }

            // Update the sales_order amount record with the total sales_order tax amount
            $this->db->query("
                UPDATE ip_sales_order_amounts SET sales_order_tax_total =
                (
                    SELECT SUM(sales_order_tax_rate_amount)
                    FROM ip_sales_order_tax_rates
                    WHERE sales_order_id = " . $this->db->escape($sales_order_id) . "
                )
                WHERE sales_order_id = " . $this->db->escape($sales_order_id)
            );

            // Get the updated sales_order amount record
            $sales_order_amount = $this->db->where('sales_order_id', $sales_order_id)->get('ip_sales_order_amounts')->row();

            // Recalculate the sales_order total
            $sales_order_total = $sales_order_amount->sales_order_item_subtotal + $sales_order_amount->sales_order_item_tax_total + $sales_order_amount->sales_order_tax_total;

            $sales_order_total = $this->calculate_discount($sales_order_id, $sales_order_total);

            // Update the sales_order amount record
            $db_array = array(
                'sales_order_total' => $sales_order_total
            );

            $this->db->where('sales_order_id', $sales_order_id);
            $this->db->update('ip_sales_order_amounts', $db_array);

            //calculate sales order tax item
            $listitems = $this->db->where('sales_order_id',$sales_order_id)->get('ip_sales_order_items')->result();
            $jumlah_item = count($listitems);

            $tax_item = $sales_order_amount->sales_order_tax_total / $jumlah_item;
            $db_array = array(
                'item_tax' => $tax_item
            );
            foreach ($listitems as $listitem) {
                $this->db->where('item_id', $listitem->item_id);
                $this->db->update('ip_sales_order_item_amounts', $db_array);
            }

        } else {
            // No sales_order taxes applied

            $db_array = array(
                'sales_order_tax_total' => '0.00'
            );

            $this->db->where('sales_order_id', $sales_order_id);
            $this->db->update('ip_sales_order_amounts', $db_array);
        }
    }

    /**
     * @param null $period
     * @return mixed
     */
    public function get_total_sales_orderd($period = null)
    {
        switch ($period) {
            case 'month':
                return $this->db->query("
					SELECT SUM(sales_order_total) AS total_sales_orderd 
					FROM ip_sales_order_amounts
					WHERE sales_order_id IN 
					(SELECT sales_order_id FROM ip_sales_orders
					WHERE MONTH(sales_order_date_created) = MONTH(NOW()) 
					AND YEAR(sales_order_date_created) = YEAR(NOW()))")->row()->total_sales_orderd;
            case 'last_month':
                return $this->db->query("
					SELECT SUM(sales_order_total) AS total_sales_orderd 
					FROM ip_sales_order_amounts
					WHERE sales_order_id IN 
					(SELECT sales_order_id FROM ip_sales_orders
					WHERE MONTH(sales_order_date_created) = MONTH(NOW() - INTERVAL 1 MONTH)
					AND YEAR(sales_order_date_created) = YEAR(NOW() - INTERVAL 1 MONTH))")->row()->total_sales_orderd;
            case 'year':
                return $this->db->query("
					SELECT SUM(sales_order_total) AS total_sales_orderd 
					FROM ip_sales_order_amounts
					WHERE sales_order_id IN 
					(SELECT sales_order_id FROM ip_sales_orders WHERE YEAR(sales_order_date_created) = YEAR(NOW()))")->row()->total_sales_orderd;
            case 'last_year':
                return $this->db->query("
					SELECT SUM(sales_order_total) AS total_sales_orderd 
					FROM ip_sales_order_amounts
					WHERE sales_order_id IN 
					(SELECT sales_order_id FROM ip_sales_orders WHERE YEAR(sales_order_date_created) = YEAR(NOW() - INTERVAL 1 YEAR))")->row()->total_sales_orderd;
            default:
                return $this->db->query("SELECT SUM(sales_order_total) AS total_sales_orderd FROM ip_sales_order_amounts")->row()->total_sales_orderd;
        }
    }

    /**
     * @param string $period
     * @return array
     */
    public function get_status_totals($period = '')
    {
        switch ($period) {
            default:
            case 'this-month':
                $results = $this->db->query("
					SELECT sales_order_status_id,
					    SUM(sales_order_total) AS sum_total,
					    COUNT(*) AS num_total
					FROM ip_sales_order_amounts
					JOIN ip_sales_orders ON ip_sales_orders.sales_order_id = ip_sales_order_amounts.sales_order_id
                        AND MONTH(ip_sales_orders.sales_order_date_created) = MONTH(NOW())
                        AND YEAR(ip_sales_orders.sales_order_date_created) = YEAR(NOW())
					GROUP BY ip_sales_orders.sales_order_status_id")->result_array();
                break;
            case 'last-month':
                $results = $this->db->query("
					SELECT sales_order_status_id,
					    SUM(sales_order_total) AS sum_total,
					    COUNT(*) AS num_total
					FROM ip_sales_order_amounts
					JOIN ip_sales_orders ON ip_sales_orders.sales_order_id = ip_sales_order_amounts.sales_order_id
                        AND MONTH(ip_sales_orders.sales_order_date_created) = MONTH(NOW() - INTERVAL 1 MONTH)
                        AND YEAR(ip_sales_orders.sales_order_date_created) = YEAR(NOW())
					GROUP BY ip_sales_orders.sales_order_status_id")->result_array();
                break;
            case 'this-quarter':
                $results = $this->db->query("
					SELECT sales_order_status_id,
					    SUM(sales_order_total) AS sum_total,
					    COUNT(*) AS num_total
					FROM ip_sales_order_amounts
					JOIN ip_sales_orders ON ip_sales_orders.sales_order_id = ip_sales_order_amounts.sales_order_id
                        AND QUARTER(ip_sales_orders.sales_order_date_created) = QUARTER(NOW())
                        AND YEAR(ip_sales_orders.sales_order_date_created) = YEAR(NOW())
					GROUP BY ip_sales_orders.sales_order_status_id")->result_array();
                break;
            case 'last-quarter':
                $results = $this->db->query("
					SELECT sales_order_status_id,
					    SUM(sales_order_total) AS sum_total,
					    COUNT(*) AS num_total
					FROM ip_sales_order_amounts
					JOIN ip_sales_orders ON ip_sales_orders.sales_order_id = ip_sales_order_amounts.sales_order_id
                        AND QUARTER(ip_sales_orders.sales_order_date_created) = QUARTER(NOW() - INTERVAL 1 QUARTER)
                        AND YEAR(ip_sales_orders.sales_order_date_created) = YEAR(NOW())
					GROUP BY ip_sales_orders.sales_order_status_id")->result_array();
                break;
            case 'this-year':
                $results = $this->db->query("
					SELECT sales_order_status_id,
					    SUM(sales_order_total) AS sum_total,
					    COUNT(*) AS num_total
					FROM ip_sales_order_amounts
					JOIN ip_sales_orders ON ip_sales_orders.sales_order_id = ip_sales_order_amounts.sales_order_id
                        AND YEAR(ip_sales_orders.sales_order_date_created) = YEAR(NOW())
					GROUP BY ip_sales_orders.sales_order_status_id")->result_array();
                break;
            case 'last-year':
                $results = $this->db->query("
					SELECT sales_order_status_id,
					    SUM(sales_order_total) AS sum_total,
					    COUNT(*) AS num_total
					FROM ip_sales_order_amounts
					JOIN ip_sales_orders ON ip_sales_orders.sales_order_id = ip_sales_order_amounts.sales_order_id
                        AND YEAR(ip_sales_orders.sales_order_date_created) = YEAR(NOW() - INTERVAL 1 YEAR)
					GROUP BY ip_sales_orders.sales_order_status_id")->result_array();
                break;
        }

        $return = array();

        foreach ($this->mdl_sales_orders->statuses() as $key => $status) {
            $return[$key] = array(
                'sales_order_status_id' => $key,
                'class' => $status['class'],
                'label' => $status['label'],
                'href' => $status['href'],
                'sum_total' => 0,
                'num_total' => 0
            );
        }

        foreach ($results as $result) {
            $return[$result['sales_order_status_id']] = array_merge($return[$result['sales_order_status_id']], $result);
        }

        return $return;
    }

}
