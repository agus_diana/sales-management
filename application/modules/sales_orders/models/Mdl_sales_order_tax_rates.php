<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * InvoicePlane
 *
 * @author		InvoicePlane Developers & Contributors
 * @copyright	Copyright (c) 2012 - 2017 InvoicePlane.com
 * @license		https://invoiceplane.com/license.txt
 * @link		https://invoiceplane.com
 */

/**
 * Class Mdl_sales_order_Tax_Rates
 */
class Mdl_sales_order_Tax_Rates extends Response_Model
{
    public $table = 'ip_sales_order_tax_rates';
    public $primary_key = 'ip_sales_order_tax_rates.sales_order_tax_rate_id';

    public function default_select()
    {
        $this->db->select('ip_tax_rates.tax_rate_name AS sales_order_tax_rate_name');
        $this->db->select('ip_tax_rates.tax_rate_percent AS sales_order_tax_rate_percent');
        $this->db->select('ip_sales_order_tax_rates.*');
    }

    public function default_join()
    {
        $this->db->join('ip_tax_rates', 'ip_tax_rates.tax_rate_id = ip_sales_order_tax_rates.tax_rate_id');
    }

    /**
     * @param null $id
     * @param null $db_array
     * @return void
     */
    public function save($id = null, $db_array = null)
    {
        parent::save($id, $db_array);

        $this->load->model('sales_orders/mdl_sales_order_amounts');

        $sales_order_id = $this->input->post('sales_order_id');

        if ($sales_order_id) {
            $this->mdl_sales_order_amounts->calculate($sales_order_id);
        }
    }

    /**
     * @return array
     * @return void
     */
    public function validation_rules()
    {
        return array(
            'sales_order_id' => array(
                'field' => 'sales_order_id',
                'label' => trans('sales_order'),
                'rules' => 'required'
            ),
            'tax_rate_id' => array(
                'field' => 'tax_rate_id',
                'label' => trans('tax_rate'),
                'rules' => 'required'
            ),
            'include_item_tax' => array(
                'field' => 'include_item_tax',
                'label' => trans('tax_rate_placement'),
                'rules' => 'required'
            )
        );
    }

}
