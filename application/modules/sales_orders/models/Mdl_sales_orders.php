<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * InvoicePlane
 *
 * @author		InvoicePlane Developers & Contributors
 * @copyright	Copyright (c) 2012 - 2017 InvoicePlane.com
 * @license		https://invoiceplane.com/license.txt
 * @link		https://invoiceplane.com
 */

/**
 * Class Mdl_sales_orders
 */
class Mdl_sales_orders extends Response_Model
{
    public $table = 'ip_sales_orders';
    public $primary_key = 'ip_sales_orders.sales_order_id';
    public $date_modified_field = 'sales_order_date_modified';

    /**
     * @return array
     */
    public function statuses()
    {
        return array(
            '1' => array(
                'label' => trans('draft'),
                'class' => 'draft',
                'href' => 'sales_orders/status/draft'
            ),
/*
            '2' => array(
                'label' => trans('sent'),
                'class' => 'sent',
                'href' => 'sales_orders/status/sent'
            ),
            '3' => array(
                'label' => trans('viewed'),
                'class' => 'viewed',
                'href' => 'sales_orders/status/viewed'
            ), */
            '4' => array(
                'label' => trans('approved'),
                'class' => 'approved',
                'href' => 'sales_orders/status/approved'
            ),
/*            '5' => array(
                'label' => trans('rejected'),
                'class' => 'rejected',
                'href' => 'sales_orders/status/rejected'
            ), */
            '6' => array(
                'label' => trans('canceled'),
                'class' => 'canceled',
                'href' => 'sales_orders/status/canceled'
            )
        );
    }

    public function default_select()
    {
        $this->db->select("
            SQL_CALC_FOUND_ROWS
            ip_users.user_name,
			ip_users.user_company,
			ip_users.user_address_1,
			ip_users.user_address_2,
			ip_users.user_city,
			ip_users.user_state,
			ip_users.user_zip,
			ip_users.user_country,
			ip_users.user_phone,
			ip_users.user_fax,
			ip_users.user_mobile,
			ip_users.user_email,
			ip_users.user_web,
			ip_users.user_vat_id,
			ip_users.user_tax_code,
			ip_clients.*,
            ip_sales_order_amounts.sales_order_amount_id,
            IFnull(ip_sales_order_amounts.sales_order_qty_balance, '0.00') AS sales_order_qty_balance,
			IFnull(ip_sales_order_amounts.sales_order_item_subtotal, '0.00') AS sales_order_item_subtotal,
			IFnull(ip_sales_order_amounts.sales_order_item_tax_total, '0.00') AS sales_order_item_tax_total,
			IFnull(ip_sales_order_amounts.sales_order_tax_total, '0.00') AS sales_order_tax_total,
            IFnull(ip_sales_order_amounts.sales_order_total, '0.00') AS sales_order_total,
			ip_sales_orders.*", false);
    }

    public function default_order_by()
    {
        $this->db->order_by('ip_sales_orders.sales_order_id DESC');
    }

    public function default_join()
    {
        $this->db->join('ip_clients', 'ip_clients.client_id = ip_sales_orders.client_id');
        $this->db->join('ip_users', 'ip_users.user_id = ip_sales_orders.user_id');
        $this->db->join('ip_sales_order_amounts', 'ip_sales_order_amounts.sales_order_id = ip_sales_orders.sales_order_id', 'left');
    }

    /**
     * @return array
     */
    public function validation_rules()
    {
        return array(
            'client_id' => array(
                'field' => 'client_id',
                'label' => trans('client'),
                'rules' => 'required'
            ),
            'sales_order_date_created' => array(
                'field' => 'sales_order_date_created',
                'label' => trans('sales_order_date'),
                'rules' => 'required'
            ),
            'expected_delivery' => array(
                'field' => 'expected_delivery',
                'label' => trans('expected_delivery'),
                'rules' => 'required'
            ),
            'invoice_group_id' => array(
                'field' => 'invoice_group_id',
                'label' => trans('sales_order_group'),
                'rules' => 'required'
            ),
            'sales_order_password' => array(
                'field' => 'sales_order_password',
                'label' => trans('sales_order_password')
            ),
            'sales_order_reff_number' => array(
                'field' => 'sales_order_reff_number',
                'label' => trans('sales_order_reff_number')
            ),
            'user_id' => array(
                'field' => 'user_id',
                'label' => trans('user'),
                'rule' => 'required'
            )
        );
    }

    /**
     * @return array
     */
    public function validation_rules_save_sales_order()
    {
        return array(
            'sales_order_number' => array(
                'field' => 'sales_order_number',
                'label' => trans('sales_order') . ' #',
                'rules' => 'is_unique[ip_sales_orders.sales_order_number' . (($this->id) ? '.sales_order_id.' . $this->id : '') . ']'
            ),
            'sales_order_date_created' => array(
                'field' => 'sales_order_date_created',
                'label' => trans('date'),
                'rules' => 'required'
            ),
            'expected_delivery' => array(
                'field' => 'expected_delivery',
                'label' => trans('date'),
                'rules' => 'required'
            ),
/*            'sales_order_date_expires' => array(
                'field' => 'sales_order_date_expires',
                'label' => trans('due_date'),
                'rules' => 'required'
            ), */
            'sales_order_password' => array(
                'field' => 'sales_order_password',
                'label' => trans('sales_order_password')
            )
        );
    }

    /**
     * @param null $db_array
     * @return int|null
     */ 
    public function create($db_array = null)
    {
        $sales_order_id = parent::save(null, $db_array);

        // Create an sales_order amount record
        $db_array = array(
            'sales_order_id' => $sales_order_id
        );

        $this->db->insert('ip_sales_order_amounts', $db_array);

        // Create the default invoice tax record if applicable
        if (get_setting('default_invoice_tax_rate')) {
            $db_array = array(
                'sales_order_id' => $sales_order_id,
                'tax_rate_id' => get_setting('default_invoice_tax_rate'),
                'include_item_tax' => get_setting('default_include_item_tax'),
                'sales_order_tax_rate_amount' => 0
            );

            $this->db->insert('ip_sales_order_tax_rates', $db_array);
        }

        return $sales_order_id;
    }

    /**
     * Copies sales_order items, tax rates, etc from source to target
     * @param int $source_id
     * @param int $target_id
     */
    public function copy_sales_order($source_id, $target_id)
    {
        $this->load->model('sales_orders/mdl_sales_order_items');

        $sales_order_items = $this->mdl_sales_order_items->where('sales_order_id', $source_id)->get()->result();

        foreach ($sales_order_items as $sales_order_item) {
            $db_array = array(
                'sales_order_id' => $target_id,
                'item_tax_rate_id' => $sales_order_item->item_tax_rate_id,
                'item_name' => $sales_order_item->item_name,
                'item_description' => $sales_order_item->item_description,
                'item_quantity' => $sales_order_item->item_quantity,
                'item_price' => $sales_order_item->item_price,
                'item_order' => $sales_order_item->item_order
            );

            $this->mdl_sales_order_items->save(null, $db_array);
        }

        $sales_order_tax_rates = $this->mdl_sales_order_tax_rates->where('sales_order_id', $source_id)->get()->result();

        foreach ($sales_order_tax_rates as $sales_order_tax_rate) {
            $db_array = array(
                'sales_order_id' => $target_id,
                'tax_rate_id' => $sales_order_tax_rate->tax_rate_id,
                'include_item_tax' => $sales_order_tax_rate->include_item_tax,
                'sales_order_tax_rate_amount' => $sales_order_tax_rate->sales_order_tax_rate_amount
            );

            $this->mdl_sales_order_tax_rates->save(null, $db_array);
        }

        // Copy the custom fields
        $this->load->model('custom_fields/mdl_sales_order_custom');
        $db_array = $this->mdl_sales_order_custom->where('sales_order_id', $source_id)->get()->row_array();

        if (count($db_array) > 2) {
            unset($db_array['sales_order_custom_id']);
            $db_array['sales_order_id'] = $target_id;
            $this->mdl_sales_order_custom->save_custom($target_id, $db_array);
        }
    }

    /**
     * @return array
     */
    public function db_array()
    {
        $db_array = parent::db_array();

        // Get the client id for the submitted sales_order
        $this->load->model('clients/mdl_clients');
        $cid = $this->mdl_clients->where('ip_clients.client_id', $db_array['client_id'])->get()->row()->client_id;
        $db_array['client_id'] = $cid;

        $db_array['sales_order_date_created'] = date_to_mysql($db_array['sales_order_date_created']);
        $db_array['expected_delivery'] = date_to_mysql($db_array['expected_delivery']);
        //$db_array['sales_order_date_expires'] = $this->get_date_due($db_array['sales_order_date_created']);

        $db_array['notes'] = get_setting('default_sales_order_notes');

        if (!isset($db_array['sales_order_status_id'])) {
            $db_array['sales_order_status_id'] = 1;
        }

        $generate_sales_order_number = get_setting('generate_sales_order_number_for_draft');

        if ($db_array['sales_order_status_id'] === 1 && $generate_sales_order_number == 1) {
            $db_array['sales_order_number'] = $this->get_sales_order_number($db_array['invoice_group_id']);
        } elseif ($db_array['sales_order_status_id'] != 1) {
            $db_array['sales_order_number'] = $this->get_sales_order_number($db_array['invoice_group_id']);
        } else {
            $db_array['sales_order_number'] = '';
        }

        // Generate the unique url key
        $db_array['sales_order_url_key'] = $this->get_url_key();

        return $db_array;
    }

    /**
     * @param string $sales_order_date_created
     */
    public function get_date_due($sales_order_date_created)
    {
        $sales_order_date_expires = new DateTime($sales_order_date_created);
        $sales_order_date_expires->add(new DateInterval('P' . get_setting('sales_orders_expire_after') . 'D'));
        return $sales_order_date_expires->format('Y-m-d');
    }

    /**
     * @param $invoice_group_id
     * @return mixed
     */
    public function get_sales_order_number($invoice_group_id)
    {
        $this->load->model('invoice_groups/mdl_invoice_groups');
        return $this->mdl_invoice_groups->generate_invoice_number($invoice_group_id);
    }

    /**
     * @return string
     */
    public function get_url_key()
    {
        $this->load->helper('string');
        return random_string('alnum', 15);
    }

    /**
     * @param $invoice_id
     * @return mixed
     */
    public function get_invoice_group_id($invoice_id)
    {
        $invoice = $this->get_by_id($invoice_id);
        return $invoice->invoice_group_id;
    }

    /**
     * @param int $sales_order_id
     */
    public function delete($sales_order_id)
    {
        parent::delete($sales_order_id);

        $this->load->helper('orphan');
        delete_orphans();
    }

    /**
     * @return $this
     */
    
    public function is_draft()
    {
        $this->filter_where('sales_order_status_id', 1);
        return $this;
    }

    /**
     * @return $this
     */
    public function is_sent()
    {
        $this->filter_where('sales_order_status_id', 2);
        return $this;
    }

    /**
     * @return $this
     */
    public function is_viewed()
    {
        $this->filter_where('sales_order_status_id', 3);
        return $this;
    }

    /**
     * @return $this
     */
    public function is_approved()
    {
        $this->filter_where('sales_order_status_id', 4);
        return $this;
    }

    /**
     * @return $this
     */
    public function is_rejected()
    {
        $this->filter_where('sales_order_status_id', 5);
        return $this;
    }

    /**
     * @return $this
     */
    public function is_canceled()
    {
        $this->filter_where('sales_order_status_id', 6);
        return $this;
    }

    /**
     * Used by guest module; includes only sent and viewed
     *
     * @return $this
     */
    public function is_open()
    {
        $this->filter_where_in('sales_order_status_id', array(2, 3));
        return $this;
    }

    /**
     * @return $this
     */
    public function guest_visible()
    {
        $this->filter_where_in('sales_order_status_id', array(2, 3, 4, 5));
        return $this;
    }

    /**
     * @param $client_id
     * @return $this
     */
    public function by_client($client_id)
    {
        $this->filter_where('ip_sales_orders.client_id', $client_id);
        return $this;
    }

    /**
     * @param $sales_order_url_key
     */
    public function approve_sales_order_by_key($sales_order_url_key)
    {
        $this->db->where_in('sales_order_status_id', array(2, 3));
        $this->db->where('sales_order_url_key', $sales_order_url_key);
        $this->db->set('sales_order_status_id', 4);
        $this->db->update('ip_sales_orders');
    }

    /**
     * @param $sales_order_url_key
     */
    public function reject_sales_order_by_key($sales_order_url_key)
    {
        $this->db->where_in('sales_order_status_id', array(2, 3));
        $this->db->where('sales_order_url_key', $sales_order_url_key);
        $this->db->set('sales_order_status_id', 5);
        $this->db->update('ip_sales_orders');
    }

    /**
     * @param $sales_order_id
     */
    public function approve_sales_order_by_id($sales_order_id)
    {
        $this->db->where_in('sales_order_status_id', array(2, 3));
        $this->db->where('sales_order_id', $sales_order_id);
        $this->db->set('sales_order_status_id', 4);
        $this->db->update('ip_sales_orders');
    }

    /**
     * @param $sales_order_id
     */
    public function reject_sales_order_by_id($sales_order_id)
    {
        $this->db->where_in('sales_order_status_id', array(2, 3));
        $this->db->where('sales_order_id', $sales_order_id);
        $this->db->set('sales_order_status_id', 5);
        $this->db->update('ip_sales_orders');
    }

    /**
     * @param $sales_order_id
     */
    public function mark_viewed($sales_order_id)
    {
        $this->db->select('sales_order_status_id');
        $this->db->where('sales_order_id', $sales_order_id);

        $sales_order = $this->db->get('ip_sales_orders');

        if ($sales_order->num_rows()) {
            if ($sales_order->row()->sales_order_status_id == 2) {
                $this->db->where('sales_order_id', $sales_order_id);
                $this->db->set('sales_order_status_id', 3);
                $this->db->update('ip_sales_orders');
            }
        }
    }

    /**
     * @param $sales_order_id
     */
    public function mark_sent($sales_order_id)
    {
        $this->db->select('sales_order_status_id');
        $this->db->where('sales_order_id', $sales_order_id);

        $sales_order = $this->db->get('ip_sales_orders');

        if ($sales_order->num_rows()) {
            if ($sales_order->row()->sales_order_status_id == 1) {
                $this->db->where('sales_order_id', $sales_order_id);
                $this->db->set('sales_order_status_id', 2);
                $this->db->update('ip_sales_orders');
            }
        }
    }

}
