<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * InvoicePlane
 *
 * @author		InvoicePlane Developers & Contributors
 * @copyright	Copyright (c) 2012 - 2017 InvoicePlane.com
 * @license		https://invoiceplane.com/license.txt
 * @link		https://invoiceplane.com
 */

/**
 * Class Mdl_sales_order_Custom
 */
class Mdl_sales_order_Custom extends Validator
{
    public static $positions = array(
        'custom_fields',
        'properties'
    );
    public $table = 'ip_sales_order_custom';
    public $primary_key = 'ip_sales_order_custom.sales_order_custom_id';

    public function default_select()
    {
        $this->db->select('SQL_CALC_FOUND_ROWS ip_sales_order_custom.*, ip_custom_fields.*', false);
    }

    public function default_join()
    {
        $this->db->join('ip_custom_fields', 'ip_sales_order_custom.sales_order_custom_fieldid = ip_custom_fields.custom_field_id');
    }

    public function default_order_by()
    {
        $this->db->order_by('custom_field_table ASC, custom_field_order ASC, custom_field_label ASC');
    }


    /**
     * @param $sales_order_id
     * @param $db_array
     * @return bool|string
     */
    public function save_custom($sales_order_id, $db_array)
    {
        $result = $this->validate($db_array);

        if ($result === true) {
            $form_data = isset($this->_formdata) ? $this->_formdata : null;

            if (is_null($form_data)) {
                return true;
            }

            $sales_order_custom_id = null;

            foreach ($form_data as $key => $value) {
                $db_array = array(
                    'sales_order_id' => $sales_order_id,
                    'sales_order_custom_fieldid' => $key,
                    'sales_order_custom_fieldvalue' => $value
                );

                $sales_order_custom = $this->where('sales_order_id', $sales_order_id)->where('sales_order_custom_fieldid', $key)->get();

                if ($sales_order_custom->num_rows()) {
                    $sales_order_custom_id = $sales_order_custom->row()->sales_order_custom_id;
                }

                parent::save($sales_order_custom_id, $db_array);
            }

            return true;
        }

        return $result;
    }

    public function by_id($sales_order_id)
    {
        $this->db->where('ip_sales_order_custom.sales_order_id', $sales_order_id);
        return $this;
    }

}
