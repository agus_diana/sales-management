-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 29 Agu 2018 pada 09.29
-- Versi server: 10.1.30-MariaDB
-- Versi PHP: 7.1.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `agansoft`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_clients`
--

CREATE TABLE `ip_clients` (
  `client_id` int(11) NOT NULL,
  `client_date_created` datetime NOT NULL,
  `client_date_modified` datetime NOT NULL,
  `client_name` text,
  `client_address_1` text,
  `client_address_2` text,
  `client_city` text,
  `client_state` text,
  `client_zip` text,
  `client_country` text,
  `client_phone` text,
  `client_fax` text,
  `client_mobile` text,
  `client_email` text,
  `client_web` text,
  `client_vat_id` text,
  `client_tax_code` text,
  `client_language` varchar(255) DEFAULT 'system',
  `client_active` int(1) NOT NULL DEFAULT '1',
  `client_surname` varchar(255) DEFAULT NULL,
  `client_avs` varchar(16) DEFAULT NULL,
  `client_insurednumber` varchar(30) DEFAULT NULL,
  `client_veka` varchar(30) DEFAULT NULL,
  `client_birthdate` date DEFAULT NULL,
  `client_gender` int(1) DEFAULT '0',
  `client_taxable` int(11) DEFAULT '0',
  `client_taxnumber` varchar(125) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_clients`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_client_custom`
--

CREATE TABLE `ip_client_custom` (
  `client_custom_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `client_custom_fieldid` int(11) NOT NULL,
  `client_custom_fieldvalue` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_client_notes`
--

CREATE TABLE `ip_client_notes` (
  `client_note_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `client_note_date` date NOT NULL,
  `client_note` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_custom_fields`
--

CREATE TABLE `ip_custom_fields` (
  `custom_field_id` int(11) NOT NULL,
  `custom_field_table` varchar(50) DEFAULT NULL,
  `custom_field_label` varchar(50) DEFAULT NULL,
  `custom_field_type` varchar(255) NOT NULL DEFAULT 'TEXT',
  `custom_field_location` int(11) DEFAULT '0',
  `custom_field_order` int(11) DEFAULT '999',
  `delivery_order_code` int(11) DEFAULT '999'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_custom_fields`
--

INSERT INTO `ip_custom_fields` (`custom_field_id`, `custom_field_table`, `custom_field_label`, `custom_field_type`, `custom_field_location`, `custom_field_order`, `delivery_order_code`) VALUES
(7, 'ip_sales_order_custom', 'Alamat Pengiriman', 'TEXT', 0, 3, 12),
(8, 'ip_sales_order_custom', 'Nama Kontak Pengiriman', 'TEXT', 0, 1, 10),
(9, 'ip_sales_order_custom', 'Telp Kontak Pengiriman', 'TEXT', 0, 2, 11),
(10, 'ip_delivery_order_custom', 'Nama Kontak Pengiriman', 'TEXT', 0, 1, 999),
(11, 'ip_delivery_order_custom', 'Telp Kontak Pengiriman', 'TEXT', 0, 2, 999),
(12, 'ip_delivery_order_custom', 'Alamat Pengiriman', 'TEXT', 0, 3, 999);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_custom_values`
--

CREATE TABLE `ip_custom_values` (
  `custom_values_id` int(11) NOT NULL,
  `custom_values_field` int(11) NOT NULL,
  `custom_values_value` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_delivery_orders`
--

CREATE TABLE `ip_delivery_orders` (
  `delivery_order_id` int(11) NOT NULL,
  `is_read_only` tinyint(4) DEFAULT '0',
  `sales_order_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `invoice_group_id` int(11) NOT NULL,
  `delivery_order_status_id` tinyint(2) NOT NULL DEFAULT '1',
  `delivery_order_date_created` date NOT NULL,
  `delivery_order_date_modified` datetime NOT NULL,
  `delivery_order_date_expires` date NOT NULL,
  `delivery_order_number` varchar(100) DEFAULT NULL,
  `delivery_order_discount_amount` decimal(20,2) DEFAULT NULL,
  `delivery_order_discount_percent` decimal(20,2) DEFAULT NULL,
  `delivery_order_url_key` char(32) NOT NULL,
  `delivery_order_password` varchar(90) DEFAULT NULL,
  `notes` longtext,
  `delivered_date` date DEFAULT NULL,
  `delivery_order_shipping_charges` decimal(20,2) DEFAULT '0.00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_delivery_orders`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_delivery_order_amounts`
--

CREATE TABLE `ip_delivery_order_amounts` (
  `delivery_order_amount_id` int(11) NOT NULL,
  `delivery_order_id` int(11) NOT NULL,
  `delivery_order_item_subtotal` decimal(20,2) DEFAULT NULL,
  `delivery_order_item_tax_total` decimal(20,2) DEFAULT NULL,
  `delivery_order_tax_total` decimal(20,2) DEFAULT NULL,
  `delivery_order_total` decimal(20,2) DEFAULT NULL,
  `delivery_order_paid` decimal(20,2) DEFAULT '0.00',
  `delivery_order_balance` decimal(20,2) DEFAULT '0.00',
  `delivery_order_qty_total` decimal(20,2) DEFAULT '0.00',
  `delivery_order_qty_balance` decimal(20,2) DEFAULT '0.00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_delivery_order_amounts`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_delivery_order_custom`
--

CREATE TABLE `ip_delivery_order_custom` (
  `delivery_order_custom_id` int(11) NOT NULL,
  `delivery_order_id` int(11) NOT NULL,
  `delivery_order_custom_fieldid` int(11) NOT NULL,
  `delivery_order_custom_fieldvalue` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ip_delivery_order_custom`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_delivery_order_items`
--

CREATE TABLE `ip_delivery_order_items` (
  `item_id` int(11) NOT NULL,
  `delivery_order_id` int(11) NOT NULL,
  `item_tax_rate_id` int(11) NOT NULL,
  `item_product_id` int(11) DEFAULT NULL,
  `item_date_added` date NOT NULL,
  `item_name` text,
  `item_description` text,
  `item_quantity` decimal(20,2) DEFAULT NULL,
  `item_price` decimal(20,2) DEFAULT NULL,
  `item_discount_amount` decimal(20,2) DEFAULT NULL,
  `item_order` int(2) NOT NULL DEFAULT '0',
  `item_product_unit` varchar(50) DEFAULT NULL,
  `item_product_unit_id` int(11) DEFAULT NULL,
  `item_quantity_paid` int(11) DEFAULT '0',
  `item_quantity_balance` int(11) DEFAULT '0',
  `sales_order_id` int(11) DEFAULT NULL,
  `item_initial_quantity` decimal(20,2) DEFAULT '0.00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_delivery_order_items`
--


--
-- Trigger `ip_delivery_order_items`
--
DELIMITER $$
CREATE TRIGGER `b4del_tbl_delivery_order_items` BEFORE DELETE ON `ip_delivery_order_items` FOR EACH ROW BEGIN
	DECLARE delta_qty_total INTEGER;
	DECLARE delta_qty_balance INTEGER;
	
	SET delta_qty_balance = -1 * OLD.item_quantity;
	SET delta_qty_total = -1 * OLD.item_quantity;
	
	UPDATE ip_delivery_order_amounts 
		SET delivery_order_qty_total = delivery_order_qty_total + delta_qty_total,
			delivery_order_qty_balance = delivery_order_qty_balance + delta_qty_balance
		WHERE delivery_order_id = OLD.delivery_order_id;
	
	UPDATE ip_sales_order_items SET item_quantity_delivered = item_quantity_delivered + delta_qty_total
		WHERE sales_order_id = OLD.sales_order_id
		AND item_product_id = OLD.item_product_id;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `b4ins_tbl_delivery_order_items` BEFORE INSERT ON `ip_delivery_order_items` FOR EACH ROW BEGIN
	DECLARE delta_balance DECIMAL;
	
	SET NEW.item_quantity_balance = NEW.item_quantity;
	
	UPDATE ip_delivery_order_amounts 
		SET delivery_order_qty_total = delivery_order_qty_total + NEW.item_quantity,
			delivery_order_qty_balance = delivery_order_qty_balance + NEW.item_quantity
		WHERE delivery_order_id = NEW.delivery_order_id;
		
	UPDATE ip_sales_order_items SET item_quantity_delivered = item_quantity_delivered + NEW.item_quantity
		WHERE sales_order_id = NEW.sales_order_id
		AND item_product_id = NEW.item_product_id;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `b4upd_tbl_delivery_order_items` BEFORE UPDATE ON `ip_delivery_order_items` FOR EACH ROW BEGIN
	DECLARE delta_qty_total INTEGER;
	DECLARE delta_qty_balance INTEGER;
	
	SET delta_qty_balance = NEW.item_quantity - OLD.item_quantity - NEW.item_quantity_paid + OLD.item_quantity_paid;
	SET delta_qty_total = NEW.item_quantity - OLD.item_quantity;
	
	SET NEW.item_quantity_balance = NEW.item_quantity - NEW.item_quantity_paid;
	
	UPDATE ip_delivery_order_amounts 
		SET delivery_order_qty_total = delivery_order_qty_total + delta_qty_total,
			delivery_order_qty_balance = (if ((delivery_order_qty_balance + delta_qty_balance) < 0, 0, delivery_order_qty_balance + delta_qty_balance))
		WHERE delivery_order_id = NEW.delivery_order_id;
		
	UPDATE ip_sales_order_items SET item_quantity_delivered = item_quantity_delivered + delta_qty_total
		WHERE sales_order_id = NEW.sales_order_id
		AND item_product_id = NEW.item_product_id;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_delivery_order_item_amounts`
--

CREATE TABLE `ip_delivery_order_item_amounts` (
  `item_amount_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_subtotal` decimal(20,2) DEFAULT NULL,
  `item_tax_total` decimal(20,2) DEFAULT NULL,
  `item_discount` decimal(20,2) DEFAULT NULL,
  `item_total` decimal(20,2) DEFAULT NULL,
  `delivery_order_paid` decimal(20,2) DEFAULT '0.00',
  `delivery_order_balance` decimal(20,2) DEFAULT '0.00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_delivery_order_item_amounts`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_delivery_order_tax_rates`
--

CREATE TABLE `ip_delivery_order_tax_rates` (
  `delivery_order_tax_rate_id` int(11) NOT NULL,
  `delivery_order_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `include_item_tax` int(1) NOT NULL DEFAULT '0',
  `delivery_order_tax_rate_amount` decimal(20,2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_delivery_order_tax_rates`
--

INSERT INTO `ip_delivery_order_tax_rates` (`delivery_order_tax_rate_id`, `delivery_order_id`, `tax_rate_id`, `include_item_tax`, `delivery_order_tax_rate_amount`) VALUES
(56, 135, 1, 0, '11149525.50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_email_templates`
--

CREATE TABLE `ip_email_templates` (
  `email_template_id` int(11) NOT NULL,
  `email_template_title` text,
  `email_template_type` varchar(255) DEFAULT NULL,
  `email_template_body` longtext NOT NULL,
  `email_template_subject` text,
  `email_template_from_name` text,
  `email_template_from_email` text,
  `email_template_cc` text,
  `email_template_bcc` text,
  `email_template_pdf_template` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_families`
--

CREATE TABLE `ip_families` (
  `family_id` int(11) NOT NULL,
  `family_name` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_families`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_imports`
--

CREATE TABLE `ip_imports` (
  `import_id` int(11) NOT NULL,
  `import_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_import_details`
--

CREATE TABLE `ip_import_details` (
  `import_detail_id` int(11) NOT NULL,
  `import_id` int(11) NOT NULL,
  `import_lang_key` varchar(35) NOT NULL,
  `import_table_name` varchar(35) NOT NULL,
  `import_record_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_invoices`
--

CREATE TABLE `ip_invoices` (
  `invoice_id` int(11) NOT NULL,
  `delivery_order_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `invoice_group_id` int(11) NOT NULL,
  `invoice_status_id` tinyint(2) NOT NULL DEFAULT '1',
  `is_read_only` tinyint(1) DEFAULT NULL,
  `invoice_password` varchar(90) DEFAULT NULL,
  `invoice_date_created` date NOT NULL,
  `invoice_time_created` time NOT NULL DEFAULT '00:00:00',
  `invoice_date_modified` datetime NOT NULL,
  `invoice_date_due` date NOT NULL,
  `invoice_number` varchar(100) DEFAULT NULL,
  `invoice_discount_amount` decimal(20,2) DEFAULT NULL,
  `invoice_discount_percent` decimal(20,2) DEFAULT NULL,
  `invoice_terms` longtext NOT NULL,
  `invoice_url_key` char(32) NOT NULL,
  `payment_method` int(11) NOT NULL DEFAULT '0',
  `creditinvoice_parent_id` int(11) DEFAULT NULL,
  `invoice_shipping_charges` decimal(20,2) DEFAULT '0.00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_invoices`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_invoices_recurring`
--

CREATE TABLE `ip_invoices_recurring` (
  `invoice_recurring_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `recur_start_date` date NOT NULL,
  `recur_end_date` date NOT NULL,
  `recur_frequency` varchar(255) NOT NULL,
  `recur_next_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_invoice_amounts`
--

CREATE TABLE `ip_invoice_amounts` (
  `invoice_amount_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `invoice_sign` enum('1','-1') NOT NULL DEFAULT '1',
  `invoice_item_subtotal` decimal(20,2) DEFAULT NULL,
  `invoice_item_tax_total` decimal(20,2) DEFAULT NULL,
  `invoice_tax_total` decimal(20,2) DEFAULT NULL,
  `invoice_total` decimal(20,2) DEFAULT NULL,
  `invoice_paid` decimal(20,2) DEFAULT NULL,
  `invoice_balance` decimal(20,2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_invoice_amounts`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_invoice_custom`
--

CREATE TABLE `ip_invoice_custom` (
  `invoice_custom_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `invoice_custom_fieldid` int(11) NOT NULL,
  `invoice_custom_fieldvalue` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_invoice_groups`
--

CREATE TABLE `ip_invoice_groups` (
  `invoice_group_id` int(11) NOT NULL,
  `invoice_group_name` text,
  `invoice_group_identifier_format` varchar(255) NOT NULL,
  `invoice_group_next_id` int(11) NOT NULL,
  `invoice_group_left_pad` int(2) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_invoice_groups`
--

INSERT INTO `ip_invoice_groups` (`invoice_group_id`, `invoice_group_name`, `invoice_group_identifier_format`, `invoice_group_next_id`, `invoice_group_left_pad`) VALUES
(5, 'INVOICE', 'I0{{{month}}}{{{yy}}}{{{id}}}', 96, 5),
(6, 'DO', '{{{id}}}/SJ/MNA/{{{month_romawi}}}/{{{yy}}}', 47, 0),
(7, 'SO', 'S0/{{{yy}}}/{{{month_romawi}}}/{{{id}}}', 44, 5),
(8, 'SO-AGUS', '{{{id}}}/{{{month_romawi}}}/{{{yy}}}/AG', 9, 3),
(9, 'SO-ARIZA', '{{{id}}}/{{{month_romawi}}}/{{{yy}}}/AR', 3, 3),
(10, 'SO-HANIF', '{{{id}}}/{{{month_romawi}}}/{{{yy}}}/HF', 2, 3),
(11, 'SO-LAUREN', '{{{id}}}/{{{month_romawi}}}/{{{yy}}}/LR', 1, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_invoice_items`
--

CREATE TABLE `ip_invoice_items` (
  `item_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `item_tax_rate_id` int(11) NOT NULL DEFAULT '0',
  `item_product_id` int(11) DEFAULT NULL,
  `item_date_added` date NOT NULL,
  `item_task_id` int(11) DEFAULT NULL,
  `item_name` text,
  `item_description` longtext,
  `item_quantity` decimal(10,2) NOT NULL,
  `item_price` decimal(20,2) DEFAULT NULL,
  `item_discount_amount` decimal(20,2) DEFAULT NULL,
  `item_order` int(2) NOT NULL DEFAULT '0',
  `item_is_recurring` tinyint(1) DEFAULT NULL,
  `item_product_unit` varchar(50) DEFAULT NULL,
  `item_product_unit_id` int(11) DEFAULT NULL,
  `item_date` date DEFAULT NULL,
  `delivery_order_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_invoice_items`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_invoice_item_amounts`
--

CREATE TABLE `ip_invoice_item_amounts` (
  `item_amount_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_subtotal` decimal(20,2) DEFAULT NULL,
  `item_tax_total` decimal(20,2) DEFAULT NULL,
  `item_discount` decimal(20,2) DEFAULT NULL,
  `item_total` decimal(20,2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_invoice_item_amounts`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_invoice_sumex`
--

CREATE TABLE `ip_invoice_sumex` (
  `sumex_id` int(11) NOT NULL,
  `sumex_invoice` int(11) NOT NULL,
  `sumex_reason` int(11) NOT NULL,
  `sumex_diagnosis` varchar(500) NOT NULL,
  `sumex_observations` varchar(500) NOT NULL,
  `sumex_treatmentstart` date NOT NULL,
  `sumex_treatmentend` date NOT NULL,
  `sumex_casedate` date NOT NULL,
  `sumex_casenumber` varchar(35) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_invoice_tax_rates`
--

CREATE TABLE `ip_invoice_tax_rates` (
  `invoice_tax_rate_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `include_item_tax` int(1) NOT NULL DEFAULT '0',
  `invoice_tax_rate_amount` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_invoice_tax_rates`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_item_lookups`
--

CREATE TABLE `ip_item_lookups` (
  `item_lookup_id` int(11) NOT NULL,
  `item_name` varchar(100) NOT NULL DEFAULT '',
  `item_description` longtext NOT NULL,
  `item_price` decimal(10,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_merchant_responses`
--

CREATE TABLE `ip_merchant_responses` (
  `merchant_response_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `merchant_response_date` date NOT NULL,
  `merchant_response_driver` varchar(35) NOT NULL,
  `merchant_response` varchar(255) NOT NULL,
  `merchant_response_reference` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_payments`
--

CREATE TABLE `ip_payments` (
  `payment_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `payment_method_id` int(11) NOT NULL DEFAULT '0',
  `payment_date` date NOT NULL,
  `payment_amount` decimal(20,2) DEFAULT NULL,
  `payment_note` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_payment_custom`
--

CREATE TABLE `ip_payment_custom` (
  `payment_custom_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `payment_custom_fieldid` int(11) NOT NULL,
  `payment_custom_fieldvalue` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_payment_methods`
--

CREATE TABLE `ip_payment_methods` (
  `payment_method_id` int(11) NOT NULL,
  `payment_method_name` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_payment_methods`
--

INSERT INTO `ip_payment_methods` (`payment_method_id`, `payment_method_name`) VALUES
(1, 'Cash'),
(2, 'Credit Card');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_products`
--

CREATE TABLE `ip_products` (
  `product_id` int(11) NOT NULL,
  `family_id` int(11) DEFAULT NULL,
  `product_sku` text,
  `product_name` text,
  `product_description` longtext NOT NULL,
  `product_price` decimal(20,2) DEFAULT NULL,
  `purchase_price` decimal(20,2) DEFAULT NULL,
  `provider_name` text,
  `tax_rate_id` int(11) DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `product_tariff` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_products`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_projects`
--

CREATE TABLE `ip_projects` (
  `project_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `project_name` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_quotes`
--

CREATE TABLE `ip_quotes` (
  `quote_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `invoice_group_id` int(11) NOT NULL,
  `quote_status_id` tinyint(2) NOT NULL DEFAULT '1',
  `quote_date_created` date NOT NULL,
  `quote_date_modified` datetime NOT NULL,
  `quote_date_expires` date NOT NULL,
  `quote_number` varchar(100) DEFAULT NULL,
  `quote_discount_amount` decimal(20,2) DEFAULT NULL,
  `quote_discount_percent` decimal(20,2) DEFAULT NULL,
  `quote_url_key` char(32) NOT NULL,
  `quote_password` varchar(90) DEFAULT NULL,
  `notes` longtext
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_quote_amounts`
--

CREATE TABLE `ip_quote_amounts` (
  `quote_amount_id` int(11) NOT NULL,
  `quote_id` int(11) NOT NULL,
  `quote_item_subtotal` decimal(20,2) DEFAULT NULL,
  `quote_item_tax_total` decimal(20,2) DEFAULT NULL,
  `quote_tax_total` decimal(20,2) DEFAULT NULL,
  `quote_total` decimal(20,2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_quote_custom`
--

CREATE TABLE `ip_quote_custom` (
  `quote_custom_id` int(11) NOT NULL,
  `quote_id` int(11) NOT NULL,
  `quote_custom_fieldid` int(11) NOT NULL,
  `quote_custom_fieldvalue` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_quote_items`
--

CREATE TABLE `ip_quote_items` (
  `item_id` int(11) NOT NULL,
  `quote_id` int(11) NOT NULL,
  `item_tax_rate_id` int(11) NOT NULL,
  `item_product_id` int(11) DEFAULT NULL,
  `item_date_added` date NOT NULL,
  `item_name` text,
  `item_description` text,
  `item_quantity` decimal(20,2) DEFAULT NULL,
  `item_price` decimal(20,2) DEFAULT NULL,
  `item_discount_amount` decimal(20,2) DEFAULT NULL,
  `item_order` int(2) NOT NULL DEFAULT '0',
  `item_product_unit` varchar(50) DEFAULT NULL,
  `item_product_unit_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_quote_item_amounts`
--

CREATE TABLE `ip_quote_item_amounts` (
  `item_amount_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_subtotal` decimal(20,2) DEFAULT NULL,
  `item_tax_total` decimal(20,2) DEFAULT NULL,
  `item_discount` decimal(20,2) DEFAULT NULL,
  `item_total` decimal(20,2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_quote_tax_rates`
--

CREATE TABLE `ip_quote_tax_rates` (
  `quote_tax_rate_id` int(11) NOT NULL,
  `quote_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `include_item_tax` int(1) NOT NULL DEFAULT '0',
  `quote_tax_rate_amount` decimal(20,2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_sales_orders`
--

CREATE TABLE `ip_sales_orders` (
  `sales_order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `invoice_group_id` int(11) NOT NULL,
  `sales_order_status_id` tinyint(2) NOT NULL DEFAULT '1',
  `sales_order_date_created` date NOT NULL,
  `sales_order_date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `sales_order_date_expires` date NOT NULL,
  `sales_order_number` varchar(100) DEFAULT NULL,
  `sales_order_discount_amount` decimal(20,2) DEFAULT NULL,
  `sales_order_discount_percent` decimal(20,2) DEFAULT NULL,
  `sales_order_url_key` char(32) NOT NULL,
  `sales_order_password` varchar(90) DEFAULT NULL,
  `notes` longtext,
  `sales_order_reff_number` varchar(50) DEFAULT NULL,
  `expected_delivery` date DEFAULT NULL,
  `sales_order_shipping_charges` decimal(20,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_sales_orders`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_sales_order_amounts`
--

CREATE TABLE `ip_sales_order_amounts` (
  `sales_order_amount_id` int(11) NOT NULL,
  `sales_order_id` int(11) NOT NULL,
  `sales_order_item_subtotal` decimal(20,2) DEFAULT '0.00',
  `sales_order_item_tax_total` decimal(20,2) DEFAULT '0.00',
  `sales_order_tax_total` decimal(20,2) DEFAULT '0.00',
  `sales_order_total` decimal(20,2) DEFAULT '0.00',
  `sales_order_delivered` decimal(20,2) DEFAULT '0.00',
  `sales_order_balance` decimal(20,2) DEFAULT '0.00',
  `sales_order_qty_balance` decimal(20,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_sales_order_amounts`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_sales_order_custom`
--

CREATE TABLE `ip_sales_order_custom` (
  `sales_order_custom_id` int(11) NOT NULL,
  `sales_order_id` int(11) NOT NULL,
  `sales_order_custom_fieldid` int(11) NOT NULL,
  `sales_order_custom_fieldvalue` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ip_sales_order_custom`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_sales_order_items`
--

CREATE TABLE `ip_sales_order_items` (
  `item_id` int(11) NOT NULL,
  `sales_order_id` int(11) NOT NULL,
  `item_tax_rate_id` int(11) NOT NULL,
  `item_product_id` int(11) DEFAULT NULL,
  `item_date_added` date NOT NULL,
  `item_name` text,
  `item_description` text,
  `item_quantity` decimal(20,2) DEFAULT NULL,
  `item_price` decimal(20,2) DEFAULT NULL,
  `item_discount_amount` decimal(20,2) DEFAULT '0.00',
  `item_order` int(2) NOT NULL DEFAULT '0',
  `item_product_unit` varchar(50) DEFAULT NULL,
  `item_product_unit_id` int(11) DEFAULT NULL,
  `item_quantity_delivered` decimal(20,2) DEFAULT '0.00',
  `item_quantity_balance` decimal(20,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_sales_order_items`
--

INSERT INTO `ip_sales_order_items` (`item_id`, `sales_order_id`, `item_tax_rate_id`, `item_product_id`, `item_date_added`, `item_name`, `item_description`, `item_quantity`, `item_price`, `item_discount_amount`, `item_order`, `item_product_unit`, `item_product_unit_id`, `item_quantity_delivered`, `item_quantity_balance`) VALUES
(129, 58, 0, 2808, '2018-08-25', 'Tea - Vanilla Chai', NULL, '50.00', '775145.00', NULL, 1, 'PCS', 1, '50.00', '0.00'),
(131, 59, 0, 2508, '2018-08-25', 'Brandy Cherry - Mcguinness', NULL, '5.00', '5459438.00', NULL, 1, 'PCS', 1, '5.00', '0.00'),
(132, 59, 0, 2944, '2018-08-25', 'Corn Shoots', NULL, '10.00', '1440343.00', NULL, 2, 'PCS', 1, '5.00', '5.00'),
(133, 59, 0, 2996, '2018-08-25', 'Mushroom - White Button', NULL, '15.00', '7699635.00', NULL, 3, 'PCS', 1, '10.00', '5.00'),
(134, 60, 0, 2608, '2018-08-27', 'Mousse - Mango', NULL, '2.00', '4391396.00', NULL, 1, 'PCS', 1, '0.00', '2.00'),
(135, 60, 0, 2920, '2018-08-27', 'Kellogs Special K Cereal', NULL, '3.00', '3772092.00', NULL, 2, 'PCS', 1, '0.00', '3.00'),
(136, 60, 0, 2797, '2018-08-27', 'Mint - Fresh', NULL, '4.00', '6391366.00', NULL, 3, 'PCS', 1, '0.00', '4.00');

--
-- Trigger `ip_sales_order_items`
--
DELIMITER $$
CREATE TRIGGER `b4del_tbl_sales_order_items` BEFORE DELETE ON `ip_sales_order_items` FOR EACH ROW BEGIN
	DECLARE delta_qty_balance INTEGER;
	
	SET delta_qty_balance = -1 * OLD.item_quantity;
	
	UPDATE ip_sales_order_amounts SET sales_order_qty_balance = sales_order_qty_balance + delta_qty_balance
		WHERE sales_order_id = OLD.sales_order_id;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `b4inst_tbl_sales_order_items` BEFORE INSERT ON `ip_sales_order_items` FOR EACH ROW BEGIN
	DECLARE delta_balance DECIMAL;
	
	SET NEW.item_quantity_balance = NEW.item_quantity;
	
	UPDATE ip_sales_order_amounts SET sales_order_qty_balance = sales_order_qty_balance + NEW.item_quantity
		WHERE sales_order_id = NEW.sales_order_id;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `b4upd_tbl_sales_order_items` BEFORE UPDATE ON `ip_sales_order_items` FOR EACH ROW BEGIN
	DECLARE delta_qty_balance INTEGER;
	
	SET delta_qty_balance = NEW.item_quantity - OLD.item_quantity - NEW.item_quantity_delivered + OLD.item_quantity_delivered;
	
	SET NEW.item_quantity_balance = NEW.item_quantity - NEW.item_quantity_delivered;
	
	UPDATE ip_sales_order_amounts SET sales_order_qty_balance = (if ((sales_order_qty_balance + delta_qty_balance) < 0, 0, sales_order_qty_balance + delta_qty_balance))
		WHERE sales_order_id = NEW.sales_order_id;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_sales_order_item_amounts`
--

CREATE TABLE `ip_sales_order_item_amounts` (
  `item_amount_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_subtotal` decimal(20,2) DEFAULT NULL,
  `item_tax_total` decimal(20,2) DEFAULT NULL,
  `item_discount` decimal(20,2) DEFAULT NULL,
  `item_total` decimal(20,2) DEFAULT NULL,
  `sales_order_delivered` decimal(20,2) DEFAULT '0.00',
  `sales_order_balance` decimal(20,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_sales_order_item_amounts`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_sales_order_tax_rates`
--

CREATE TABLE `ip_sales_order_tax_rates` (
  `sales_order_tax_rate_id` int(11) NOT NULL,
  `sales_order_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `include_item_tax` int(1) NOT NULL DEFAULT '0',
  `sales_order_tax_rate_amount` decimal(20,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_sales_order_tax_rates`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_sessions`
--

CREATE TABLE `ip_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_settings`
--

CREATE TABLE `ip_settings` (
  `setting_id` int(11) NOT NULL,
  `setting_key` varchar(50) NOT NULL,
  `setting_value` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_settings`
--

INSERT INTO `ip_settings` (`setting_id`, `setting_key`, `setting_value`) VALUES
(19, 'default_language', 'Indonesian'),
(20, 'date_format', 'd-m-Y'),
(21, 'currency_symbol', 'Rp'),
(22, 'currency_symbol_placement', 'before'),
(23, 'currency_code', 'USD'),
(24, 'invoices_due_after', '30'),
(25, 'quotes_expire_after', '15'),
(26, 'default_invoice_group', '5'),
(27, 'default_quote_group', ''),
(28, 'thousands_separator', '.'),
(29, 'decimal_point', ''),
(30, 'cron_key', '4gpm57iqUz8JLMIQ'),
(31, 'tax_rate_decimal_places', '2'),
(32, 'pdf_invoice_template', 'InvoicePlane'),
(33, 'pdf_invoice_template_paid', 'InvoicePlane - paid'),
(34, 'pdf_invoice_template_overdue', 'InvoicePlane - overdue'),
(35, 'pdf_quote_template', 'InvoicePlane'),
(36, 'public_invoice_template', 'InvoicePlane_Web'),
(37, 'public_quote_template', 'InvoicePlane_Web'),
(38, 'disable_sidebar', '0'),
(39, 'read_only_toggle', '4'),
(40, 'invoice_pre_password', ''),
(41, 'quote_pre_password', ''),
(42, 'email_pdf_attachment', '1'),
(43, 'generate_invoice_number_for_draft', '1'),
(44, 'generate_quote_number_for_draft', '1'),
(45, 'sumex', '0'),
(46, 'sumex_sliptype', '1'),
(47, 'sumex_canton', '0'),
(48, 'system_theme', 'invoiceplane'),
(49, 'default_hourly_rate', '0'),
(50, 'projects_enabled', '0'),
(51, 'first_day_of_week', '1'),
(52, 'default_country', 'ID'),
(53, 'default_list_limit', '15'),
(54, 'quote_overview_period', 'this-month'),
(55, 'invoice_overview_period', 'this-month'),
(56, 'disable_quickactions', '0'),
(57, 'custom_title', 'FastPoint'),
(58, 'monospace_amounts', '0'),
(59, 'reports_in_new_tab', '0'),
(60, 'bcc_mails_to_admin', '0'),
(61, 'default_invoice_terms', ''),
(62, 'invoice_default_payment_method', ''),
(63, 'mark_invoices_sent_pdf', '0'),
(64, 'pdf_watermark', '0'),
(65, 'include_zugferd', '0'),
(66, 'email_invoice_template', ''),
(67, 'email_invoice_template_paid', ''),
(68, 'email_invoice_template_overdue', ''),
(69, 'pdf_invoice_footer', ''),
(70, 'automatic_email_on_recur', '0'),
(71, 'sumex_role', '0'),
(72, 'sumex_place', '0'),
(73, 'mark_quotes_sent_pdf', '0'),
(74, 'default_quote_notes', ''),
(75, 'email_quote_template', ''),
(76, 'default_invoice_tax_rate', ''),
(77, 'default_include_item_tax', ''),
(78, 'default_item_tax_rate', ''),
(79, 'email_send_method', ''),
(80, 'smtp_server_address', ''),
(81, 'smtp_authentication', '0'),
(82, 'smtp_username', ''),
(83, 'smtp_port', ''),
(84, 'smtp_security', ''),
(85, 'smtp_verify_certs', '1'),
(86, 'enable_online_payments', '0'),
(87, 'gateway_authorizenet_aim_enabled', '0'),
(88, 'gateway_authorizenet_aim_apiLoginId', ''),
(89, 'gateway_authorizenet_aim_transactionKey', ''),
(90, 'gateway_authorizenet_aim_testMode', '0'),
(91, 'gateway_authorizenet_aim_developerMode', '0'),
(92, 'gateway_authorizenet_aim_currency', 'ARS'),
(93, 'gateway_authorizenet_aim_payment_method', ''),
(94, 'gateway_authorizenet_sim_enabled', '0'),
(95, 'gateway_authorizenet_sim_apiLoginId', ''),
(96, 'gateway_authorizenet_sim_transactionKey', ''),
(97, 'gateway_authorizenet_sim_testMode', '0'),
(98, 'gateway_authorizenet_sim_developerMode', '0'),
(99, 'gateway_authorizenet_sim_currency', 'ARS'),
(100, 'gateway_authorizenet_sim_payment_method', ''),
(101, 'gateway_buckaroo_ideal_enabled', '0'),
(102, 'gateway_buckaroo_ideal_websiteKey', ''),
(103, 'gateway_buckaroo_ideal_testMode', '0'),
(104, 'gateway_buckaroo_ideal_currency', 'ARS'),
(105, 'gateway_buckaroo_ideal_payment_method', ''),
(106, 'gateway_buckaroo_paypal_enabled', '0'),
(107, 'gateway_buckaroo_paypal_websiteKey', ''),
(108, 'gateway_buckaroo_paypal_testMode', '0'),
(109, 'gateway_buckaroo_paypal_currency', 'ARS'),
(110, 'gateway_buckaroo_paypal_payment_method', ''),
(111, 'gateway_cardsave_enabled', '0'),
(112, 'gateway_cardsave_merchantId', ''),
(113, 'gateway_cardsave_currency', 'ARS'),
(114, 'gateway_cardsave_payment_method', ''),
(115, 'gateway_coinbase_enabled', '0'),
(116, 'gateway_coinbase_apiKey', ''),
(117, 'gateway_coinbase_accountId', ''),
(118, 'gateway_coinbase_currency', 'ARS'),
(119, 'gateway_coinbase_payment_method', ''),
(120, 'gateway_eway_rapid_enabled', '0'),
(121, 'gateway_eway_rapid_apiKey', ''),
(122, 'gateway_eway_rapid_testMode', '0'),
(123, 'gateway_eway_rapid_currency', 'ARS'),
(124, 'gateway_eway_rapid_payment_method', ''),
(125, 'gateway_firstdata_connect_enabled', '0'),
(126, 'gateway_firstdata_connect_storeId', ''),
(127, 'gateway_firstdata_connect_testMode', '0'),
(128, 'gateway_firstdata_connect_currency', 'ARS'),
(129, 'gateway_firstdata_connect_payment_method', ''),
(130, 'gateway_gocardless_enabled', '0'),
(131, 'gateway_gocardless_appId', ''),
(132, 'gateway_gocardless_merchantId', ''),
(133, 'gateway_gocardless_accessToken', ''),
(134, 'gateway_gocardless_testMode', '0'),
(135, 'gateway_gocardless_currency', 'ARS'),
(136, 'gateway_gocardless_payment_method', ''),
(137, 'gateway_migs_threeparty_enabled', '0'),
(138, 'gateway_migs_threeparty_merchantId', ''),
(139, 'gateway_migs_threeparty_merchantAccessCode', ''),
(140, 'gateway_migs_threeparty_secureHash', ''),
(141, 'gateway_migs_threeparty_currency', 'ARS'),
(142, 'gateway_migs_threeparty_payment_method', ''),
(143, 'gateway_migs_twoparty_enabled', '0'),
(144, 'gateway_migs_twoparty_merchantId', ''),
(145, 'gateway_migs_twoparty_merchantAccessCode', ''),
(146, 'gateway_migs_twoparty_secureHash', ''),
(147, 'gateway_migs_twoparty_currency', 'ARS'),
(148, 'gateway_migs_twoparty_payment_method', ''),
(149, 'gateway_mollie_enabled', '0'),
(150, 'gateway_mollie_apiKey', ''),
(151, 'gateway_mollie_currency', 'ARS'),
(152, 'gateway_mollie_payment_method', ''),
(153, 'gateway_multisafepay_enabled', '0'),
(154, 'gateway_multisafepay_accountId', ''),
(155, 'gateway_multisafepay_siteId', ''),
(156, 'gateway_multisafepay_siteCode', ''),
(157, 'gateway_multisafepay_testMode', '0'),
(158, 'gateway_multisafepay_currency', 'ARS'),
(159, 'gateway_multisafepay_payment_method', ''),
(160, 'gateway_netaxept_enabled', '0'),
(161, 'gateway_netaxept_merchantId', ''),
(162, 'gateway_netaxept_testMode', '0'),
(163, 'gateway_netaxept_currency', 'ARS'),
(164, 'gateway_netaxept_payment_method', ''),
(165, 'gateway_netbanx_enabled', '0'),
(166, 'gateway_netbanx_accountNumber', ''),
(167, 'gateway_netbanx_storeId', ''),
(168, 'gateway_netbanx_testMode', '0'),
(169, 'gateway_netbanx_currency', 'ARS'),
(170, 'gateway_netbanx_payment_method', ''),
(171, 'gateway_payfast_enabled', '0'),
(172, 'gateway_payfast_merchantId', ''),
(173, 'gateway_payfast_merchantKey', ''),
(174, 'gateway_payfast_pdtKey', ''),
(175, 'gateway_payfast_testMode', '0'),
(176, 'gateway_payfast_currency', 'ARS'),
(177, 'gateway_payfast_payment_method', ''),
(178, 'gateway_payflow_pro_enabled', '0'),
(179, 'gateway_payflow_pro_username', ''),
(180, 'gateway_payflow_pro_vendor', ''),
(181, 'gateway_payflow_pro_partner', ''),
(182, 'gateway_payflow_pro_testMode', '0'),
(183, 'gateway_payflow_pro_currency', 'ARS'),
(184, 'gateway_payflow_pro_payment_method', ''),
(185, 'gateway_paymentexpress_pxpay_enabled', '0'),
(186, 'gateway_paymentexpress_pxpay_username', ''),
(187, 'gateway_paymentexpress_pxpay_pxPostUsername', ''),
(188, 'gateway_paymentexpress_pxpay_testMode', '0'),
(189, 'gateway_paymentexpress_pxpay_currency', 'ARS'),
(190, 'gateway_paymentexpress_pxpay_payment_method', ''),
(191, 'gateway_paymentexpress_pxpost_enabled', '0'),
(192, 'gateway_paymentexpress_pxpost_username', ''),
(193, 'gateway_paymentexpress_pxpost_testMode', '0'),
(194, 'gateway_paymentexpress_pxpost_currency', 'ARS'),
(195, 'gateway_paymentexpress_pxpost_payment_method', ''),
(196, 'gateway_paypal_express_enabled', '0'),
(197, 'gateway_paypal_express_username', ''),
(198, 'gateway_paypal_express_testMode', '0'),
(199, 'gateway_paypal_express_currency', 'ARS'),
(200, 'gateway_paypal_express_payment_method', ''),
(201, 'gateway_paypal_pro_enabled', '0'),
(202, 'gateway_paypal_pro_username', ''),
(203, 'gateway_paypal_pro_signature', ''),
(204, 'gateway_paypal_pro_testMode', '0'),
(205, 'gateway_paypal_pro_currency', 'ARS'),
(206, 'gateway_paypal_pro_payment_method', ''),
(207, 'gateway_pin_enabled', '0'),
(208, 'gateway_pin_testMode', '0'),
(209, 'gateway_pin_currency', 'ARS'),
(210, 'gateway_pin_payment_method', ''),
(211, 'gateway_sagepay_direct_enabled', '0'),
(212, 'gateway_sagepay_direct_vendor', ''),
(213, 'gateway_sagepay_direct_testMode', '0'),
(214, 'gateway_sagepay_direct_referrerId', ''),
(215, 'gateway_sagepay_direct_currency', 'ARS'),
(216, 'gateway_sagepay_direct_payment_method', ''),
(217, 'gateway_sagepay_server_enabled', '0'),
(218, 'gateway_sagepay_server_vendor', ''),
(219, 'gateway_sagepay_server_testMode', '0'),
(220, 'gateway_sagepay_server_referrerId', ''),
(221, 'gateway_sagepay_server_currency', 'ARS'),
(222, 'gateway_sagepay_server_payment_method', ''),
(223, 'gateway_securepay_directpost_enabled', '0'),
(224, 'gateway_securepay_directpost_merchantId', ''),
(225, 'gateway_securepay_directpost_testMode', '0'),
(226, 'gateway_securepay_directpost_currency', 'ARS'),
(227, 'gateway_securepay_directpost_payment_method', ''),
(228, 'gateway_stripe_enabled', '0'),
(229, 'gateway_stripe_currency', 'ARS'),
(230, 'gateway_stripe_payment_method', ''),
(231, 'gateway_targetpay_directebanking_enabled', '0'),
(232, 'gateway_targetpay_directebanking_subAccountId', ''),
(233, 'gateway_targetpay_directebanking_currency', 'ARS'),
(234, 'gateway_targetpay_directebanking_payment_method', ''),
(235, 'gateway_targetpay_ideal_enabled', '0'),
(236, 'gateway_targetpay_ideal_subAccountId', ''),
(237, 'gateway_targetpay_ideal_currency', 'ARS'),
(238, 'gateway_targetpay_ideal_payment_method', ''),
(239, 'gateway_targetpay_mrcash_enabled', '0'),
(240, 'gateway_targetpay_mrcash_subAccountId', ''),
(241, 'gateway_targetpay_mrcash_currency', 'ARS'),
(242, 'gateway_targetpay_mrcash_payment_method', ''),
(243, 'gateway_twocheckout_enabled', '0'),
(244, 'gateway_twocheckout_accountNumber', ''),
(245, 'gateway_twocheckout_testMode', '0'),
(246, 'gateway_twocheckout_currency', 'ARS'),
(247, 'gateway_twocheckout_payment_method', ''),
(248, 'gateway_worldpay_enabled', '0'),
(249, 'gateway_worldpay_installationId', ''),
(250, 'gateway_worldpay_accountId', ''),
(251, 'gateway_worldpay_testMode', '0'),
(252, 'gateway_worldpay_currency', 'ARS'),
(253, 'gateway_worldpay_payment_method', ''),
(254, 'enable_permissive_search_clients', '1'),
(255, 'quotes_enabled', '0'),
(256, 'documentation_enabled', '0'),
(257, 'delivery_order_pre_password', ''),
(258, 'default_delivery_order_group', '6'),
(259, 'default_delivery_order_notes', ''),
(260, 'generate_delivery_order_number_for_draft', '1'),
(261, 'delivery_orders_expire_after', '15'),
(262, 'default_sales_order_group', '7'),
(263, 'generate_sales_order_number_for_draft', '1'),
(264, 'sales_order_enabled', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_tasks`
--

CREATE TABLE `ip_tasks` (
  `task_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `task_name` text,
  `task_description` longtext NOT NULL,
  `task_price` decimal(20,2) DEFAULT NULL,
  `task_finish_date` date NOT NULL,
  `task_status` tinyint(1) NOT NULL,
  `tax_rate_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_tax_rates`
--

CREATE TABLE `ip_tax_rates` (
  `tax_rate_id` int(11) NOT NULL,
  `tax_rate_name` text,
  `tax_rate_percent` decimal(5,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_tax_rates`
--

INSERT INTO `ip_tax_rates` (`tax_rate_id`, `tax_rate_name`, `tax_rate_percent`) VALUES
(1, 'PPN', '10.00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_units`
--

CREATE TABLE `ip_units` (
  `unit_id` int(11) NOT NULL,
  `unit_name` varchar(50) DEFAULT NULL,
  `unit_name_plrl` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_units`
--

INSERT INTO `ip_units` (`unit_id`, `unit_name`, `unit_name_plrl`) VALUES
(1, 'PCS', 'PCS'),
(2, 'UNIT', 'UNIT');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_uploads`
--

CREATE TABLE `ip_uploads` (
  `upload_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `url_key` char(32) NOT NULL,
  `file_name_original` longtext NOT NULL,
  `file_name_new` longtext NOT NULL,
  `uploaded_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_users`
--

CREATE TABLE `ip_users` (
  `user_id` int(11) NOT NULL,
  `user_type` int(1) NOT NULL DEFAULT '0',
  `user_active` tinyint(1) DEFAULT '1',
  `user_date_created` datetime NOT NULL,
  `user_date_modified` datetime NOT NULL,
  `user_language` varchar(255) DEFAULT 'system',
  `user_name` text,
  `user_company` text,
  `user_address_1` text,
  `user_address_2` text,
  `user_city` text,
  `user_state` text,
  `user_zip` text,
  `user_country` text,
  `user_phone` text,
  `user_fax` text,
  `user_mobile` text,
  `user_email` text,
  `user_password` varchar(60) NOT NULL,
  `user_web` text,
  `user_vat_id` text,
  `user_tax_code` text,
  `user_psalt` text,
  `user_passwordreset_token` varchar(100) DEFAULT '',
  `user_all_clients` int(1) NOT NULL DEFAULT '0',
  `user_subscribernumber` varchar(40) DEFAULT NULL,
  `user_iban` varchar(34) DEFAULT NULL,
  `user_gln` bigint(13) DEFAULT NULL,
  `user_rcc` varchar(7) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_users`
--

INSERT INTO `ip_users` (`user_id`, `user_type`, `user_active`, `user_date_created`, `user_date_modified`, `user_language`, `user_name`, `user_company`, `user_address_1`, `user_address_2`, `user_city`, `user_state`, `user_zip`, `user_country`, `user_phone`, `user_fax`, `user_mobile`, `user_email`, `user_password`, `user_web`, `user_vat_id`, `user_tax_code`, `user_psalt`, `user_passwordreset_token`, `user_all_clients`, `user_subscribernumber`, `user_iban`, `user_gln`, `user_rcc`) VALUES
(1, 1, 1, '2018-01-09 21:46:12', '2018-01-09 21:46:12', 'system', 'agusdiana', NULL, 'Gd. Graha Mandiri Lt. 31', 'Jl. Imam Bonjol No. 61', 'MENTENG', 'DKI Jakarta', '10310', 'ID', '81281046170', '81281046170', '81281046170', 'queryanalizer@gmail.com', '$2a$10$f7be0a00b7d02ba3b6b61uv.pjdeDf3ewlUion3j2g0mrMyJdK3tS', '', NULL, NULL, 'f7be0a00b7d02ba3b6b617', 'ba619801cf2a195807acf1b901c0c6b7', 0, NULL, NULL, NULL, NULL),
(2, 2, 1, '2018-01-20 11:40:10', '2018-01-20 11:40:10', 'system', 'saya', '', '', '', '', '', '', 'ID', '', '', '', 'saya@gmail.com', '$2a$10$0de8390046887b2741eecuZPec7M/cZkZ0zfZQqiLkzuEkWrp4apW', '', '', '', '0de8390046887b2741eec2', '', 0, '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_user_clients`
--

CREATE TABLE `ip_user_clients` (
  `user_client_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_user_clients`
--

INSERT INTO `ip_user_clients` (`user_client_id`, `user_id`, `client_id`) VALUES
(1, 2, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_user_custom`
--

CREATE TABLE `ip_user_custom` (
  `user_custom_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_custom_fieldid` int(11) NOT NULL,
  `user_custom_fieldvalue` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ip_versions`
--

CREATE TABLE `ip_versions` (
  `version_id` int(11) NOT NULL,
  `version_date_applied` varchar(14) NOT NULL,
  `version_file` varchar(45) NOT NULL,
  `version_sql_errors` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `ip_versions`
--

INSERT INTO `ip_versions` (`version_id`, `version_date_applied`, `version_file`, `version_sql_errors`) VALUES
(1, '1515530628', '000_1.0.0.sql', 0),
(2, '1515530645', '001_1.0.1.sql', 0),
(3, '1515530645', '002_1.0.2.sql', 0),
(4, '1515530646', '003_1.1.0.sql', 0),
(5, '1515530646', '004_1.1.1.sql', 0),
(6, '1515530646', '005_1.1.2.sql', 0),
(7, '1515530647', '006_1.2.0.sql', 0),
(8, '1515530647', '007_1.2.1.sql', 0),
(9, '1515530647', '008_1.3.0.sql', 0),
(10, '1515530647', '009_1.3.1.sql', 0),
(11, '1515530647', '010_1.3.2.sql', 0),
(12, '1515530647', '011_1.3.3.sql', 0),
(13, '1515530648', '012_1.4.0.sql', 0),
(14, '1515530648', '013_1.4.1.sql', 0),
(15, '1515530648', '014_1.4.2.sql', 0),
(16, '1515530648', '015_1.4.3.sql', 0),
(17, '1515530648', '016_1.4.4.sql', 0),
(18, '1515530648', '017_1.4.5.sql', 0),
(19, '1515530648', '018_1.4.6.sql', 0),
(20, '1515530655', '019_1.4.7.sql', 0),
(21, '1515530656', '020_1.4.8.sql', 0),
(22, '1515530656', '021_1.4.9.sql', 0),
(23, '1515530656', '022_1.4.10.sql', 0),
(24, '1515530658', '023_1.5.0.sql', 0),
(25, '1515530660', '024_1.5.1.sql', 0),
(26, '1515530660', '025_1.5.2.sql', 0),
(27, '1515530660', '026_1.5.3.sql', 0),
(28, '1515530660', '027_1.5.4.sql', 0),
(29, '1515530660', '028_1.5.5.sql', 0);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_sales_order_item_outstanding`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_sales_order_item_outstanding` (
`sales_order_id` int(11)
,`item_quantity_balance` decimal(20,2)
,`product_id` int(11)
,`family_id` int(11)
,`product_sku` text
,`product_name` text
,`product_description` longtext
,`product_price` decimal(20,2)
,`purchase_price` decimal(20,2)
,`provider_name` text
,`tax_rate_id` int(11)
,`unit_id` int(11)
,`product_tariff` int(11)
);

-- --------------------------------------------------------

--
-- Struktur untuk view `v_sales_order_item_outstanding`
--
DROP TABLE IF EXISTS `v_sales_order_item_outstanding`;

CREATE VIEW `v_sales_order_item_outstanding`  AS  select `a`.`sales_order_id` AS `sales_order_id`,`b`.`item_quantity_balance` AS `item_quantity_balance`,`c`.`product_id` AS `product_id`,`c`.`family_id` AS `family_id`,`c`.`product_sku` AS `product_sku`,`c`.`product_name` AS `product_name`,`c`.`product_description` AS `product_description`,`c`.`product_price` AS `product_price`,`c`.`purchase_price` AS `purchase_price`,`c`.`provider_name` AS `provider_name`,`c`.`tax_rate_id` AS `tax_rate_id`,`c`.`unit_id` AS `unit_id`,`c`.`product_tariff` AS `product_tariff` from ((`ip_sales_orders` `a` join `ip_sales_order_items` `b` on((`a`.`sales_order_id` = `b`.`sales_order_id`))) join `ip_products` `c` on((`b`.`item_product_id` = `c`.`product_id`))) where ((`a`.`sales_order_status_id` = 4) and (`b`.`item_quantity_balance` > 0)) ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `ip_clients`
--
ALTER TABLE `ip_clients`
  ADD PRIMARY KEY (`client_id`),
  ADD KEY `client_active` (`client_active`);

--
-- Indeks untuk tabel `ip_client_custom`
--
ALTER TABLE `ip_client_custom`
  ADD PRIMARY KEY (`client_custom_id`),
  ADD UNIQUE KEY `client_id` (`client_id`,`client_custom_fieldid`);

--
-- Indeks untuk tabel `ip_client_notes`
--
ALTER TABLE `ip_client_notes`
  ADD PRIMARY KEY (`client_note_id`),
  ADD KEY `client_id` (`client_id`,`client_note_date`);

--
-- Indeks untuk tabel `ip_custom_fields`
--
ALTER TABLE `ip_custom_fields`
  ADD PRIMARY KEY (`custom_field_id`),
  ADD UNIQUE KEY `custom_field_table_2` (`custom_field_table`,`custom_field_label`),
  ADD KEY `custom_field_table` (`custom_field_table`);

--
-- Indeks untuk tabel `ip_custom_values`
--
ALTER TABLE `ip_custom_values`
  ADD PRIMARY KEY (`custom_values_id`);

--
-- Indeks untuk tabel `ip_delivery_orders`
--
ALTER TABLE `ip_delivery_orders`
  ADD PRIMARY KEY (`delivery_order_id`),
  ADD KEY `user_id` (`user_id`,`client_id`,`invoice_group_id`,`delivery_order_date_created`,`delivery_order_date_expires`,`delivery_order_number`),
  ADD KEY `quote_status_id` (`delivery_order_status_id`);

--
-- Indeks untuk tabel `ip_delivery_order_amounts`
--
ALTER TABLE `ip_delivery_order_amounts`
  ADD PRIMARY KEY (`delivery_order_amount_id`),
  ADD KEY `quote_id` (`delivery_order_id`);

--
-- Indeks untuk tabel `ip_delivery_order_custom`
--
ALTER TABLE `ip_delivery_order_custom`
  ADD PRIMARY KEY (`delivery_order_custom_id`),
  ADD UNIQUE KEY `quote_id` (`delivery_order_id`,`delivery_order_custom_fieldid`);

--
-- Indeks untuk tabel `ip_delivery_order_items`
--
ALTER TABLE `ip_delivery_order_items`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `quote_id` (`delivery_order_id`,`item_date_added`,`item_order`),
  ADD KEY `item_tax_rate_id` (`item_tax_rate_id`);

--
-- Indeks untuk tabel `ip_delivery_order_item_amounts`
--
ALTER TABLE `ip_delivery_order_item_amounts`
  ADD PRIMARY KEY (`item_amount_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indeks untuk tabel `ip_delivery_order_tax_rates`
--
ALTER TABLE `ip_delivery_order_tax_rates`
  ADD PRIMARY KEY (`delivery_order_tax_rate_id`),
  ADD KEY `quote_id` (`delivery_order_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`);

--
-- Indeks untuk tabel `ip_email_templates`
--
ALTER TABLE `ip_email_templates`
  ADD PRIMARY KEY (`email_template_id`);

--
-- Indeks untuk tabel `ip_families`
--
ALTER TABLE `ip_families`
  ADD PRIMARY KEY (`family_id`);

--
-- Indeks untuk tabel `ip_imports`
--
ALTER TABLE `ip_imports`
  ADD PRIMARY KEY (`import_id`);

--
-- Indeks untuk tabel `ip_import_details`
--
ALTER TABLE `ip_import_details`
  ADD PRIMARY KEY (`import_detail_id`),
  ADD KEY `import_id` (`import_id`,`import_record_id`);

--
-- Indeks untuk tabel `ip_invoices`
--
ALTER TABLE `ip_invoices`
  ADD PRIMARY KEY (`invoice_id`),
  ADD UNIQUE KEY `invoice_url_key` (`invoice_url_key`),
  ADD KEY `user_id` (`user_id`,`client_id`,`invoice_group_id`,`invoice_date_created`,`invoice_date_due`,`invoice_number`),
  ADD KEY `invoice_status_id` (`invoice_status_id`);

--
-- Indeks untuk tabel `ip_invoices_recurring`
--
ALTER TABLE `ip_invoices_recurring`
  ADD PRIMARY KEY (`invoice_recurring_id`),
  ADD KEY `invoice_id` (`invoice_id`);

--
-- Indeks untuk tabel `ip_invoice_amounts`
--
ALTER TABLE `ip_invoice_amounts`
  ADD PRIMARY KEY (`invoice_amount_id`),
  ADD KEY `invoice_id` (`invoice_id`),
  ADD KEY `invoice_paid` (`invoice_paid`,`invoice_balance`);

--
-- Indeks untuk tabel `ip_invoice_custom`
--
ALTER TABLE `ip_invoice_custom`
  ADD PRIMARY KEY (`invoice_custom_id`),
  ADD UNIQUE KEY `invoice_id` (`invoice_id`,`invoice_custom_fieldid`);

--
-- Indeks untuk tabel `ip_invoice_groups`
--
ALTER TABLE `ip_invoice_groups`
  ADD PRIMARY KEY (`invoice_group_id`),
  ADD KEY `invoice_group_next_id` (`invoice_group_next_id`),
  ADD KEY `invoice_group_left_pad` (`invoice_group_left_pad`);

--
-- Indeks untuk tabel `ip_invoice_items`
--
ALTER TABLE `ip_invoice_items`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `invoice_id` (`invoice_id`,`item_tax_rate_id`,`item_date_added`,`item_order`);

--
-- Indeks untuk tabel `ip_invoice_item_amounts`
--
ALTER TABLE `ip_invoice_item_amounts`
  ADD PRIMARY KEY (`item_amount_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indeks untuk tabel `ip_invoice_sumex`
--
ALTER TABLE `ip_invoice_sumex`
  ADD PRIMARY KEY (`sumex_id`);

--
-- Indeks untuk tabel `ip_invoice_tax_rates`
--
ALTER TABLE `ip_invoice_tax_rates`
  ADD PRIMARY KEY (`invoice_tax_rate_id`),
  ADD KEY `invoice_id` (`invoice_id`,`tax_rate_id`);

--
-- Indeks untuk tabel `ip_item_lookups`
--
ALTER TABLE `ip_item_lookups`
  ADD PRIMARY KEY (`item_lookup_id`);

--
-- Indeks untuk tabel `ip_merchant_responses`
--
ALTER TABLE `ip_merchant_responses`
  ADD PRIMARY KEY (`merchant_response_id`),
  ADD KEY `merchant_response_date` (`merchant_response_date`),
  ADD KEY `invoice_id` (`invoice_id`);

--
-- Indeks untuk tabel `ip_payments`
--
ALTER TABLE `ip_payments`
  ADD PRIMARY KEY (`payment_id`),
  ADD KEY `invoice_id` (`invoice_id`),
  ADD KEY `payment_method_id` (`payment_method_id`),
  ADD KEY `payment_amount` (`payment_amount`);

--
-- Indeks untuk tabel `ip_payment_custom`
--
ALTER TABLE `ip_payment_custom`
  ADD PRIMARY KEY (`payment_custom_id`),
  ADD UNIQUE KEY `payment_id` (`payment_id`,`payment_custom_fieldid`);

--
-- Indeks untuk tabel `ip_payment_methods`
--
ALTER TABLE `ip_payment_methods`
  ADD PRIMARY KEY (`payment_method_id`);

--
-- Indeks untuk tabel `ip_products`
--
ALTER TABLE `ip_products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indeks untuk tabel `ip_projects`
--
ALTER TABLE `ip_projects`
  ADD PRIMARY KEY (`project_id`);

--
-- Indeks untuk tabel `ip_quotes`
--
ALTER TABLE `ip_quotes`
  ADD PRIMARY KEY (`quote_id`),
  ADD KEY `user_id` (`user_id`,`client_id`,`invoice_group_id`,`quote_date_created`,`quote_date_expires`,`quote_number`),
  ADD KEY `invoice_id` (`invoice_id`),
  ADD KEY `quote_status_id` (`quote_status_id`);

--
-- Indeks untuk tabel `ip_quote_amounts`
--
ALTER TABLE `ip_quote_amounts`
  ADD PRIMARY KEY (`quote_amount_id`),
  ADD KEY `quote_id` (`quote_id`);

--
-- Indeks untuk tabel `ip_quote_custom`
--
ALTER TABLE `ip_quote_custom`
  ADD PRIMARY KEY (`quote_custom_id`),
  ADD UNIQUE KEY `quote_id` (`quote_id`,`quote_custom_fieldid`);

--
-- Indeks untuk tabel `ip_quote_items`
--
ALTER TABLE `ip_quote_items`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `quote_id` (`quote_id`,`item_date_added`,`item_order`),
  ADD KEY `item_tax_rate_id` (`item_tax_rate_id`);

--
-- Indeks untuk tabel `ip_quote_item_amounts`
--
ALTER TABLE `ip_quote_item_amounts`
  ADD PRIMARY KEY (`item_amount_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indeks untuk tabel `ip_quote_tax_rates`
--
ALTER TABLE `ip_quote_tax_rates`
  ADD PRIMARY KEY (`quote_tax_rate_id`),
  ADD KEY `quote_id` (`quote_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`);

--
-- Indeks untuk tabel `ip_sales_orders`
--
ALTER TABLE `ip_sales_orders`
  ADD PRIMARY KEY (`sales_order_id`),
  ADD KEY `user_id` (`user_id`,`client_id`,`invoice_group_id`,`sales_order_date_created`,`sales_order_date_expires`,`sales_order_number`),
  ADD KEY `quote_status_id` (`sales_order_status_id`);

--
-- Indeks untuk tabel `ip_sales_order_amounts`
--
ALTER TABLE `ip_sales_order_amounts`
  ADD PRIMARY KEY (`sales_order_amount_id`),
  ADD KEY `quote_id` (`sales_order_id`);

--
-- Indeks untuk tabel `ip_sales_order_custom`
--
ALTER TABLE `ip_sales_order_custom`
  ADD PRIMARY KEY (`sales_order_custom_id`),
  ADD UNIQUE KEY `quote_id` (`sales_order_id`,`sales_order_custom_fieldid`);

--
-- Indeks untuk tabel `ip_sales_order_items`
--
ALTER TABLE `ip_sales_order_items`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `quote_id` (`sales_order_id`,`item_date_added`,`item_order`),
  ADD KEY `item_tax_rate_id` (`item_tax_rate_id`);

--
-- Indeks untuk tabel `ip_sales_order_item_amounts`
--
ALTER TABLE `ip_sales_order_item_amounts`
  ADD PRIMARY KEY (`item_amount_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indeks untuk tabel `ip_sales_order_tax_rates`
--
ALTER TABLE `ip_sales_order_tax_rates`
  ADD PRIMARY KEY (`sales_order_tax_rate_id`),
  ADD KEY `quote_id` (`sales_order_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`);

--
-- Indeks untuk tabel `ip_sessions`
--
ALTER TABLE `ip_sessions`
  ADD KEY `ip_sessions_timestamp` (`timestamp`);

--
-- Indeks untuk tabel `ip_settings`
--
ALTER TABLE `ip_settings`
  ADD PRIMARY KEY (`setting_id`),
  ADD KEY `setting_key` (`setting_key`);

--
-- Indeks untuk tabel `ip_tasks`
--
ALTER TABLE `ip_tasks`
  ADD PRIMARY KEY (`task_id`);

--
-- Indeks untuk tabel `ip_tax_rates`
--
ALTER TABLE `ip_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`);

--
-- Indeks untuk tabel `ip_units`
--
ALTER TABLE `ip_units`
  ADD PRIMARY KEY (`unit_id`);

--
-- Indeks untuk tabel `ip_uploads`
--
ALTER TABLE `ip_uploads`
  ADD PRIMARY KEY (`upload_id`);

--
-- Indeks untuk tabel `ip_users`
--
ALTER TABLE `ip_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indeks untuk tabel `ip_user_clients`
--
ALTER TABLE `ip_user_clients`
  ADD PRIMARY KEY (`user_client_id`),
  ADD KEY `user_id` (`user_id`,`client_id`);

--
-- Indeks untuk tabel `ip_user_custom`
--
ALTER TABLE `ip_user_custom`
  ADD PRIMARY KEY (`user_custom_id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`user_custom_fieldid`);

--
-- Indeks untuk tabel `ip_versions`
--
ALTER TABLE `ip_versions`
  ADD PRIMARY KEY (`version_id`),
  ADD KEY `version_date_applied` (`version_date_applied`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `ip_clients`
--
ALTER TABLE `ip_clients`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=805;

--
-- AUTO_INCREMENT untuk tabel `ip_client_custom`
--
ALTER TABLE `ip_client_custom`
  MODIFY `client_custom_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ip_client_notes`
--
ALTER TABLE `ip_client_notes`
  MODIFY `client_note_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `ip_custom_fields`
--
ALTER TABLE `ip_custom_fields`
  MODIFY `custom_field_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `ip_custom_values`
--
ALTER TABLE `ip_custom_values`
  MODIFY `custom_values_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ip_delivery_orders`
--
ALTER TABLE `ip_delivery_orders`
  MODIFY `delivery_order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT untuk tabel `ip_delivery_order_amounts`
--
ALTER TABLE `ip_delivery_order_amounts`
  MODIFY `delivery_order_amount_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT untuk tabel `ip_delivery_order_custom`
--
ALTER TABLE `ip_delivery_order_custom`
  MODIFY `delivery_order_custom_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT untuk tabel `ip_delivery_order_items`
--
ALTER TABLE `ip_delivery_order_items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=222;

--
-- AUTO_INCREMENT untuk tabel `ip_delivery_order_item_amounts`
--
ALTER TABLE `ip_delivery_order_item_amounts`
  MODIFY `item_amount_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=222;

--
-- AUTO_INCREMENT untuk tabel `ip_delivery_order_tax_rates`
--
ALTER TABLE `ip_delivery_order_tax_rates`
  MODIFY `delivery_order_tax_rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT untuk tabel `ip_email_templates`
--
ALTER TABLE `ip_email_templates`
  MODIFY `email_template_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ip_families`
--
ALTER TABLE `ip_families`
  MODIFY `family_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT untuk tabel `ip_imports`
--
ALTER TABLE `ip_imports`
  MODIFY `import_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `ip_import_details`
--
ALTER TABLE `ip_import_details`
  MODIFY `import_detail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ip_invoices`
--
ALTER TABLE `ip_invoices`
  MODIFY `invoice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT untuk tabel `ip_invoices_recurring`
--
ALTER TABLE `ip_invoices_recurring`
  MODIFY `invoice_recurring_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ip_invoice_amounts`
--
ALTER TABLE `ip_invoice_amounts`
  MODIFY `invoice_amount_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT untuk tabel `ip_invoice_custom`
--
ALTER TABLE `ip_invoice_custom`
  MODIFY `invoice_custom_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ip_invoice_groups`
--
ALTER TABLE `ip_invoice_groups`
  MODIFY `invoice_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `ip_invoice_items`
--
ALTER TABLE `ip_invoice_items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=181;

--
-- AUTO_INCREMENT untuk tabel `ip_invoice_item_amounts`
--
ALTER TABLE `ip_invoice_item_amounts`
  MODIFY `item_amount_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=181;

--
-- AUTO_INCREMENT untuk tabel `ip_invoice_sumex`
--
ALTER TABLE `ip_invoice_sumex`
  MODIFY `sumex_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ip_invoice_tax_rates`
--
ALTER TABLE `ip_invoice_tax_rates`
  MODIFY `invoice_tax_rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `ip_item_lookups`
--
ALTER TABLE `ip_item_lookups`
  MODIFY `item_lookup_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ip_merchant_responses`
--
ALTER TABLE `ip_merchant_responses`
  MODIFY `merchant_response_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ip_payments`
--
ALTER TABLE `ip_payments`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `ip_payment_custom`
--
ALTER TABLE `ip_payment_custom`
  MODIFY `payment_custom_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ip_payment_methods`
--
ALTER TABLE `ip_payment_methods`
  MODIFY `payment_method_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `ip_products`
--
ALTER TABLE `ip_products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3005;

--
-- AUTO_INCREMENT untuk tabel `ip_projects`
--
ALTER TABLE `ip_projects`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ip_quotes`
--
ALTER TABLE `ip_quotes`
  MODIFY `quote_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `ip_quote_amounts`
--
ALTER TABLE `ip_quote_amounts`
  MODIFY `quote_amount_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `ip_quote_custom`
--
ALTER TABLE `ip_quote_custom`
  MODIFY `quote_custom_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ip_quote_items`
--
ALTER TABLE `ip_quote_items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `ip_quote_item_amounts`
--
ALTER TABLE `ip_quote_item_amounts`
  MODIFY `item_amount_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `ip_quote_tax_rates`
--
ALTER TABLE `ip_quote_tax_rates`
  MODIFY `quote_tax_rate_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ip_sales_orders`
--
ALTER TABLE `ip_sales_orders`
  MODIFY `sales_order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT untuk tabel `ip_sales_order_amounts`
--
ALTER TABLE `ip_sales_order_amounts`
  MODIFY `sales_order_amount_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT untuk tabel `ip_sales_order_custom`
--
ALTER TABLE `ip_sales_order_custom`
  MODIFY `sales_order_custom_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT untuk tabel `ip_sales_order_items`
--
ALTER TABLE `ip_sales_order_items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT untuk tabel `ip_sales_order_item_amounts`
--
ALTER TABLE `ip_sales_order_item_amounts`
  MODIFY `item_amount_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT untuk tabel `ip_sales_order_tax_rates`
--
ALTER TABLE `ip_sales_order_tax_rates`
  MODIFY `sales_order_tax_rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `ip_settings`
--
ALTER TABLE `ip_settings`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=265;

--
-- AUTO_INCREMENT untuk tabel `ip_tasks`
--
ALTER TABLE `ip_tasks`
  MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ip_tax_rates`
--
ALTER TABLE `ip_tax_rates`
  MODIFY `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `ip_units`
--
ALTER TABLE `ip_units`
  MODIFY `unit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `ip_uploads`
--
ALTER TABLE `ip_uploads`
  MODIFY `upload_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ip_users`
--
ALTER TABLE `ip_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `ip_user_clients`
--
ALTER TABLE `ip_user_clients`
  MODIFY `user_client_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `ip_user_custom`
--
ALTER TABLE `ip_user_custom`
  MODIFY `user_custom_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ip_versions`
--
ALTER TABLE `ip_versions`
  MODIFY `version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
COMMIT;

CREATE TRIGGER `b4del_tbl_invoice_items` BEFORE DELETE ON `ip_invoice_items` FOR EACH ROW BEGIN
	DECLARE delta_qty INTEGER;
	
	SET delta_qty = -1 * OLD.item_quantity;

	UPDATE ip_delivery_order_items
			SET item_quantity_paid = item_quantity_paid + delta_qty
		WHERE delivery_order_id = OLD.delivery_order_id 
			AND item_product_id = OLD.item_product_id;
	
END;

CREATE TRIGGER `b4ins_tbl_invoice_items` BEFORE INSERT ON `ip_invoice_items` FOR EACH ROW BEGIN
	
	UPDATE ip_delivery_order_items
		SET item_quantity_paid = item_quantity_paid + NEW.item_quantity
		WHERE delivery_order_id = NEW.delivery_order_id AND item_product_id = NEW.item_product_id;
			
END;

CREATE TRIGGER `b4upd_tbl_invoice_items` BEFORE UPDATE ON `ip_invoice_items` FOR EACH ROW BEGIN
	DECLARE delta_qty INTEGER;
	
	SET delta_qty = NEW.item_quantity - OLD.item_quantity;
	
	UPDATE ip_delivery_order_items
		SET item_quantity_paid = item_quantity_paid + delta_qty
	WHERE delivery_order_id = new.delivery_order_id and item_product_id = new.item_product_id;
	
END;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
